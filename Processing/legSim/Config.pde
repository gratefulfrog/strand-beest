class Config{
  // constants!
  final int    nbPoints    = 14;
  final float  originX     = 0,
               originY     = 0,
               alphaX      = 38,
               alphaY      = -7.8,
               driveRadius = 15,
               D           = 61.9,
               C           = 50,
               B           = 39.4,
               A           = 39.3,
               mitA        = 40.1,
               mitB        = 41.5,
               mitC        = 55.8,
               matA        = 36.7,
               matB        = 49,
               matC        = 65.7,
               maxY        = 83.77;

  final color darkBlue   = #0000FF,
              lightGreen = #78C99D,
              red        = #FF0000,
              purple     = #9716F5,
              darkGreen  = #195D30,
              orange     = #EAA421,
              lightBlue  = #3291B2,
              black      = #000000,
              whiteish    = 150, //#FFFFFF
              lightGrey  = 30
              ;
  
  // user variables
  final float angularVelocity = radians(5),
              offsetEpsilon   = 15,  // to give a border around the leg
              scaleFactor     = 6, // 6 is optimal!
              mWidth          = round(scaleFactor*(matA+alphaX+driveRadius +D) + offsetEpsilon),
              nmWidth         = round(scaleFactor*(2*matA+2*alphaX+driveRadius +D) + offsetEpsilon),
              mHeight         = round(scaleFactor*(mitB + maxY)  + 2*offsetEpsilon),
              offsetX         = mWidth/2.0,
              offsetY         = mitB*scaleFactor + offsetEpsilon;
  
}

         
