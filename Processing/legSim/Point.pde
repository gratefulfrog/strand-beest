class Point{
  //physical points do not depend on others
  float x,y; 
  color c0 =-1;
  Point(float xx,float yy,color cc){
    x  = xx;
    y  = yy;
    c0 = cc;
  }
  Point(float xx,float yy){
    x = xx;
    y = yy;
  }
  
  Point(Point p){
    if (p!=null){
      x = p.x;
      y = p.y;
      c0= p.c0;
    }
    else{
      x=y=0;
    }
  }
  void display(){
    pushStyle();
    if (c0>0){
      stroke(c0);
      fill(c0);
    }
    ellipse(x,y,1,1);
    popStyle();
  }
  void lineTo(Point target){
    // draw a line from point to target
    if (target !=null){
      line(x,y,target.x,target.y);
    }
  }
  void update(){}
}
abstract class MovingPoint extends Point{
  Point p0;
  color c0;
  MovingPoint(Point pp, color cc){
    super(null);
    p0 = pp;
    c0 = cc;
  }
  void display(){
    pushStyle();
    stroke(c0);
    fill(c0);
    super.display();
    lineTo(p0);
    popStyle();
  }
  //abstract void update();
}

class DrivePoint extends MovingPoint{
  // drive point depends only on axis center
  float sign;
  DrivePoint(Point pp,color cc, float ssign){
    super(pp,cc);
    sign=ssign;
  }
  void update(){
    float theta = conf.angularVelocity*frameCount;
    x = p0.x + sign* conf.driveRadius*cos(theta);
    y = p0.y - sign* conf.driveRadius*sin(theta);
  }
}

class LegPoint extends MovingPoint{
 // moving leg points depend on 2 points for circle intersection
  Point p1; 
  color c1;
  float r0 = 0,
        r1 = 0,
        sign = 1;

  LegPoint(Point p0, Point pp1, float rr0, float rr1,color c0,color cc1,float ssign){
    super(p0,c0);
    c1 = cc1;
    p1 = pp1;
    sign = ssign;
    r0 = rr0;
    r1 = rr1;
    this.update();
  }
  void update(){
    //println("r0: ",r0,"r1: ",r1);
    float dx = p1.x-p0.x,
          dy = p1.y-p0.y;
          //println("dx: ",dx,"dy: ",dy);
    float d = sqrt(pow(dx,2)+pow(dy,2));
          //println("d: ",d);
    float a = (pow(r0,2)-pow(r1,2) + pow(d,2))/(2*d);
          //println("a: ",a);
    float h = sqrt(pow(r0,2)-pow(a,2));
          //println("h: ",h);
    float x2 = p0.x + a*(p1.x-p0.x)/d,
          y2 = p0.y + a*(p1.y-p0.y)/d;
    x = x2 - sign*h*(-p1.y+p0.y)/d;
    y = y2 - sign*h*(p1.x-p0.x)/d;
    //println("x: ",this.x, "y: ", this.y);
  }  
  void display(){
    super.display();
    pushStyle();
    stroke(c1);
    fill(c1);
    lineTo(p1);
    popStyle();
  }
}
