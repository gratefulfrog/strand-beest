// Stradbeest Leg Simulator

Config conf;

// used to pause the display
boolean pauseDraw = false;

Point      origin1,
           origin2,
           alpha;
           //beta;
DrivePoint p0;
LegPoint   p11,
           p12,
           p13,
           p14,
           p15,
           p21,
           p22,
           p23,
           p24,
           p25;
           
DrivePoint p00;
LegPoint   p011,
           p012,
           p013,
           p014,
           p015,
           p021,
           p022,
           p023,
           p024,
           p025;
           
// for vector-wise point processing 
Point[] pointVec;

void setup() {
  /*
  scaleFactor: 1.0  width:  177 height:  155
  scaleFactor: 2.0  width:  338 height:  281
  scaleFactor: 3.0  width:  500 height:  406
  scaleFactor: 4.0  width:  661 height:  531
  scaleFactor: 5.0  width:  823 height:  656
  scaleFactor: 6.0  width:  985 height:  782
  scaleFactor: 7.0  width:  1146 height:  907  *** optimal ***
  scaleFactor: 8.0  width:  1308 height:  1032
  */
  //size(985,782); // need to compute these since js version cannont use settings!
  size(1373,782); // need to compute these since js version cannont use settings!
  colorMode(RGB,256);
  conf = new Config();
  println("mWidth",conf.mWidth);
  println("mmWidth",conf.nmWidth);
  background(conf.whiteish); // bg color is dark!
  
  origin1 = new Point(conf.originX,conf.originY,conf.whiteish);
  origin2 = new Point(conf.originX+2*conf.alphaX,conf.originY,conf.whiteish);
  alpha   = new Point(conf.alphaX,conf.alphaY,conf.whiteish);
  p0      = new DrivePoint(alpha,conf.darkBlue,1);
  p11     = new LegPoint(origin1, p0, conf.mitB,conf.C,conf.red,conf.darkGreen,1);
  p21     = new LegPoint(origin2, p0, conf.mitB,conf.C,conf.red,conf.darkGreen,-1);
  p12     = new LegPoint(origin1,p11,conf. mitA,conf.mitC,conf.red,conf.red,1);
  p22     = new LegPoint(origin2,p21,conf. mitA,conf.mitC,conf.red,conf.red,-1);
  p13     = new LegPoint(origin1,p0,conf.A,conf.D,conf.lightGreen,conf.orange,-1);
  p23     = new LegPoint(origin2,p0,conf.A,conf.D,conf.lightGreen,conf.orange,1);
  p14     = new LegPoint(p13,p12,conf.matA,conf.B,conf.lightBlue,conf.purple,1); 
  p24     = new LegPoint(p23,p22,conf.matA,conf.B,conf.lightBlue,conf.purple,-1); 
  p15     = new LegPoint(p13,p14,conf.matB,conf.matC,conf.lightBlue,conf.lightBlue,1);
  p25     = new LegPoint(p23,p24,conf.matB,conf.matC,conf.lightBlue,conf.lightBlue,-1);

  p00      = new DrivePoint(alpha,conf.darkBlue,-1);
  p011     = new LegPoint(origin1, p00, conf.mitB,conf.C,conf.red,conf.darkGreen,1);
  p021     = new LegPoint(origin2, p00, conf.mitB,conf.C,conf.red,conf.darkGreen,-1);
  p012     = new LegPoint(origin1,p011,conf. mitA,conf.mitC,conf.red,conf.red,1);
  p022     = new LegPoint(origin2,p021,conf. mitA,conf.mitC,conf.red,conf.red,-1);
  p013     = new LegPoint(origin1,p00,conf.A,conf.D,conf.lightGreen,conf.orange,-1);
  p023     = new LegPoint(origin2,p00,conf.A,conf.D,conf.lightGreen,conf.orange,1);
  p014     = new LegPoint(p013,p012,conf.matA,conf.B,conf.lightBlue,conf.purple,1); 
  p024     = new LegPoint(p023,p022,conf.matA,conf.B,conf.lightBlue,conf.purple,-1); 
  p015     = new LegPoint(p013,p014,conf.matB,conf.matC,conf.lightBlue,conf.lightBlue,1);
  p025     = new LegPoint(p023,p024,conf.matB,conf.matC,conf.lightBlue,conf.lightBlue,-1);


  Point[] ppointVec = new Point[] { p0,
                                    p11,
                                    p12,
                                    p13,
                                    p14,
                                    p15,
                                    p21,
                                    p22,
                                    p23,
                                    p24,
                                    p25,
                                    p00,
                                    p011,
                                    p012,
                                    p013,
                                    p014,
                                    p015,
                                    p021,
                                    p022,
                                    p023,
                                    p024,
                                    p025,
                                    alpha,
                                    origin1,
                                    origin2,
                                  };
  pointVec = ppointVec;        
}
/*
void printoutParams(){
  println("scaleFactor:",conf.scaleFactor," width: ", conf.mWidth,"height: ",conf.mHeight);
}
*/

void update(){
  for (int i=0;i<tempNbPoints/*conf.nbPoints*/;i++){
    pointVec[i].update();
  }
}

int tempNbPoints = 25;

void drawBGLines(){
  pushStyle();
  stroke(conf.lightGrey);
  //fill(120);
  line(origin1.x,origin1.y,origin2.x,origin2.y);
  line(alpha.x,origin1.y,alpha.x,alpha.y);
  popStyle();
}

void draw() {  
  background(conf.black);
  pushMatrix();
  translate(conf.offsetX,conf.offsetY);
  scale(conf.scaleFactor);
  drawBGLines();
  for (int i=0;i<tempNbPoints/*conf.nbPoints*/;i++){
    pointVec[i].display();
  }
  displayBaseline();
  popMatrix();
  update();
}

void displayBaseline(){
  // from previously translated matrix
  translate(-conf.offsetX,0);
  pushStyle();
  stroke(conf.whiteish);
  fill(conf.whiteish);
  line(0,conf.maxY,width,conf.maxY);
  popStyle();
}

void mouseClicked(){
  pauseDraw =!pauseDraw;
  if (pauseDraw){
    noLoop();
  }
  else{
    loop();
  }
}

void keyPressed(){
  mouseClicked();
}
