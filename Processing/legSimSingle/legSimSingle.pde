// Stradbeest Leg Simulator

Config conf;

// used to pause the display
boolean pauseDraw = false;

Point      origin,
           alpha;
DrivePoint p0;
LegPoint   p1,
           p2,
           p3,
           p4,
           p5;
           
// for vector-wise point processing 
Point[] pointVec;

void setup() {
  /*
  scaleFactor: 1.0  width:  177 height:  155
  scaleFactor: 2.0  width:  338 height:  281
  scaleFactor: 3.0  width:  500 height:  406
  scaleFactor: 4.0  width:  661 height:  531
  scaleFactor: 5.0  width:  823 height:  656
  scaleFactor: 6.0  width:  985 height:  782
  scaleFactor: 7.0  width:  1146 height:  907  *** optimal ***
  scaleFactor: 8.0  width:  1308 height:  1032
  */
  size(985,782); // need to compute these since js version cannont use settings!
  conf = new Config();
  background(conf.black); // bg color is dark!
  
  origin = new Point(conf.originX,conf.originY,conf.white);
  alpha  = new Point(conf.alphaX,conf.alphaY,conf.white);
  p0     = new DrivePoint(alpha,conf.darkBlue);
  p1     = new LegPoint(origin, p0, conf.mitB,conf.C,conf.red,conf.darkGreen,1);
  p2     = new LegPoint(origin,p1,conf. mitA,conf.mitC,conf.red,conf.red,1);
  p3     = new LegPoint(origin,p0,conf.A,conf.D,conf.lightGreen,conf.orange,-1);
  p4     = new LegPoint(p3,p2,conf.matA,conf.B,conf.lightBlue,conf.purple,1); 
  p5     = new LegPoint(p3,p4,conf.matB,conf.matC,conf.lightBlue,conf.lightBlue,1);

  Point[] ppointVec = new Point[] { origin,
                                    alpha,
                                    p0,
                                    p1,
                                    p2,
                                    p3,
                                    p4,
                                    p5
                                  };
  pointVec = ppointVec;        
}
/*
void printoutParams(){
  println("scaleFactor:",conf.scaleFactor," width: ", conf.mWidth,"height: ",conf.mHeight);
}
*/
void update(){
  for (int i=0;i<conf.nbPoints;i++){
    pointVec[i].update();
  }
}

void draw() {  
  background(conf.black);
  pushMatrix();
  translate(conf.offsetX,conf.offsetY);
  scale(conf.scaleFactor);
  for (int i=0;i<conf.nbPoints;i++){
    pointVec[i].display();
  }
  displayBaseline();
  popMatrix();
  update();
}

void displayBaseline(){
  // from previously translated matrix
  translate(-conf.offsetX,0);
  pushStyle();
  stroke(conf.white);
  line(0,conf.maxY,width,conf.maxY);
  popStyle();
}

void mouseClicked(){
  pauseDraw =!pauseDraw;
  if (pauseDraw){
    noLoop();
  }
  else{
    loop();
  }
}

void keyPressed(){
  mouseClicked();
}
