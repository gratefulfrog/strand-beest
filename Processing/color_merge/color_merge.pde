
int r = 255,
    g = 0,
    b = 100,
    i = 0;

void setup(){
  size(200,200);
}

void draw(){
  fill(color(255-g,g,b));
  circle(width/2.,height/2.,50);
  g = (g+1)%255;
  delay(100);
}
