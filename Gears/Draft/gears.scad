$fn=100;
thickness  = 4.0;
sThickness = 3.0;


//linear_extrude(height = 3, center = true, convexity = 10)
  //( import (file = "4gears.dxf", layer = "Gear1a");
/*
module g1(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = "4gears.dxf", layer = "Gear1a");
}
module g2(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = "4gears.dxf", layer = "Gear2a");
}
*/
module g1(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = "10-26-27-30.dxf", layer = "G10");
}
module g2(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = "10-70-20-60.dxf", layer = "Gear2");
}
module g3(){
  translate([0,0,thickness]){
    linear_extrude(height = thickness, center = true, convexity = 10)
      import (file = "4gears.dxf", layer = "Gear3a");
  } 
}
module g4(){
  translate([0,0,thickness]){
    linear_extrude(height = thickness, center = true, convexity = 10)
      import (file = "4gears.dxf", layer = "Gear4a");
  }
}

module getGear(nbTeeth){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = "../DXF/10-28x cp9 pa30.dxf", layer = str("g",nbTeeth));
}

module support(){
  translate([0,0,sThickness]){
    linear_extrude(height = sThickness, center = true, convexity = 10)
      import (file = "gear test support.dxf", layer = "lifters");
  }
    linear_extrude(height = sThickness, center = true, convexity = 10)
      import (file = "gear test support.dxf", layer = "0");
}
  
//support();
gears = [10,26,27,30];
/*
for(i=[0:3]){
  translate([100*i,0,0])
  getGear(gears[i]);
}
*/
translate([0,0,thickness])
getGear(10);
getGear(28);