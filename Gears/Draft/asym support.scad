$fn=100;
thickness  = 3.0;
wThickness = 1.5;
postHeight = 3.0;
epsilon    = 0.1;

fileName = "asym gear test support.dxf";

module rawDisk(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = fileName, layer = "disk");
}

module wedgie(){
  linear_extrude(height = thickness, center = true, convexity = 10)
    import (file = fileName, layer = "wedgie");
}
module post(){
  linear_extrude(height =postHeight, center = true, convexity = 10)
    import (file = fileName, layer = "post");
}  

module postHole(){
  linear_extrude(height =thickness*100, center = true, convexity = 10)
    import (file = fileName, layer = "post-hole");
}  
  
module rawBar(){
  linear_extrude(height =postHeight, center = true, convexity = 10)
    import (file = fileName, layer = "bar");
} 
module disk(){
  difference(){
    rawDisk();
    translate([0,0,wThickness])
      wedgie();
  }
    translate([0,0,wThickness-epsilon])
      post();
}
module bar(){
  difference(){
    union(){
      rawBar();
      translate([0,0,wThickness])
        wedgie();
    }
  
  postHole();
  }
}

module view(){
  mirror([0,0,1])disk();
  #translate([0,0,-3])bar();
}
//disk();
bar();

