
#include <AccelStepper.h>

#include <esp_now.h>
#include <WiFi.h>

#include "outils.h"

//#define IS_ROBOT
#define USE_DRIVER  // rest current 0.7 !!!

////////////////// ESP NOW Comms Config ///////////////////////////////

uint8_t //ESP32_A_Address[] = {0x24, 0x62, 0xAB, 0xF6, 0x1E, 0x64},  // this MCU is dead, it's a dead MCU...
        ESP32_B_Address[] = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},    // current config robot
        ESP32_C_Address[] = {0x24, 0x0A, 0xC4, 0x59, 0xBA, 0x34},    // spare MCU 
        ESP32_D_Address[] = {0xAC, 0x67, 0xB2, 0x3D, 0x5F, 0x50},    // current config controller
        ESP32_E_Address[] = {0xAC, 0x67, 0xB2, 0x3D, 0x52, 0x30},    // spare MCU 
        myMac[6],
        receiverAddress[6];

// cannot go out of scope, for some reason??
esp_now_peer_info_t peerInfo;

#ifdef IS_ROBOT
#include "stripper.h"
#define STRIP_PINL (26)
#define STRIP_PINR (25)
#define RECEIVER_ADDRESS  (ESP32_D_Address)

Stripper *l,*r,
         *stripperVec[2];// = {r,l};
////////////////// Motor Config ///////////////////////////////

/*
// headlight
const int nbLeds = 4,
          flLed  = 2,
          frLed  = 22,
          rlLed  = 15,
          rrLed  = 23,
          ledPinVec[] = {flLed,frLed,rlLed,rrLed},
          ledPinArray[2][2] = {{frLed,rrLed},// right 
                               {flLed,rlLed}}, // left 
          ledOnOffVec[3][2] = {{0,0},{1,0},{0,1}}; // {FrontLedState, RearLedState}
*/
           
volatile byte incoming = 0;

#ifdef USE_DRIVER
const int speedValue = 600,
          lStep      = 27,
          lDir       = 14,
          rStep      = 12,
          rDir       = 13;
//AccelStepper stepper = AccelStepper(AccelStepper::DRIVER, stepPin, dirPin);
AccelStepper leftStepper(AccelStepper::DRIVER, lStep,lDir), // stepPin, dirPin)
             rightStepper(AccelStepper::DRIVER,rStep,rDir); // stepPin, dirPin)

#else
const int speedValue   = 500;
// stepper motor pins, 4 per motor, input to the ULN2803 darlington driver 
const int lm1 = 27,
          lm2 = 14,
          lm3 = 12,
          lm4 = 13,
          rm1 = 26,
          rm2 = 25,
          rm3 = 33,
          rm4 = 32;

// maybe try with AccelStepper::HALF4WIRE ??
AccelStepper leftStepper(AccelStepper::FULL4WIRE, lm1,lm3,lm2,lm4),      // note motor pins: in1, in3, in2, in4 
             rightStepper(AccelStepper::FULL4WIRE,rm1,rm3,rm2,rm4);  // note motor pins: in1, in3, in2, in4
#endif
             
struct mvStruct{
  AccelStepper motor;
  int          lastSpeed;
};

mvStruct motorVec[]= {{rightStepper, 0},    // motor, lastSpeed}
                      {leftStepper,0}};

const int speedFacotorVec[] = {0,1,-1},
          nbMotors          = 2;

#else    /// CONTROLLER DATA

#define RECEIVER_ADDRESS  (ESP32_C_Address)

byte outgoing             = 0;
const int nbLedPins       = 6,
          nbButtonPins    = 4,
          rightForwardLedPin    = 32,
          rightReverseLedPin    = 33,
          rightStoppedLedPin    = 23,
          leftForwardLedPin     = 27,
          leftReverseLedPin     = 14,
          leftStoppedLedPin     = 22,
          leftForwardButtonPin  = 13,
          leftReverseButtonPin  = 12,
          rightForwardButtonPin = 26,
          rightReverseButtonPin = 25,
          *buttonPinVec[] = {&rightForwardButtonPin,&rightReverseButtonPin,&leftForwardButtonPin,&leftReverseButtonPin},
          *ledPinVec[]    = {&rightForwardLedPin,
                             &rightReverseLedPin, 
                             &leftForwardLedPin,
                             &leftReverseLedPin,
                             &rightStoppedLedPin,
                             &leftStoppedLedPin};
#endif

////////////////////// EPS NOW Functions ///////////////
// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {  
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void setupWifi(){
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  
  WiFi.macAddress(myMac);
  
  Serial.print("My MAC: ");
  printMac(myMac);
  /*
  if (macEquals(myMac,ESP32_D_Address)){
    memcpy(receiverAddress, ESP32_B_Address, 6);
  }
  else{
    memcpy(receiverAddress, ESP32_D_Address, 6);
  }
  Serial.print("Receiver MAC: ");
  printMac(receiverAddress);
  */

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    while(1);
  } 
}

void registerAndAddPeer(){  
  memcpy(peerInfo.peer_addr, RECEIVER_ADDRESS,6);
  peerInfo.channel = 0;   
  peerInfo.encrypt = false;

  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    while(1);
  }
}

void setup(){  
  Serial.begin(115200);
  
  #ifdef IS_ROBOT 
     setupRobot();
  #else
     setupController();
  #endif
  
  setupWifi();

  // register data Send CB to
  esp_now_register_send_cb(OnDataSent);
  
  registerAndAddPeer();

  #ifdef IS_ROBOT 
    // Register data rcev callback
    esp_now_register_recv_cb(OnDataRecv);
  #endif
   
  Serial.println("Starting up");
}

#ifdef IS_ROBOT
void setupRobot(){
  for (int i =0;i<2;i++){
    motorVec[i].motor.setMaxSpeed(speedValue);
    motorVec[i].motor.setSpeed(0);
    motorVec[i].motor.disableOutputs();
  } 
  memcpy(receiverAddress, RECEIVER_ADDRESS, 6);
  Serial.print("Receiver MAC: ");
  printMac(receiverAddress);
  // setup neopixel strips
  l = new Stripper(STRIP_PINL);
  l->setSpeed(0);
  r = new Stripper(STRIP_PINR);
  r->setSpeed(0);
  stripperVec[0] = r;
  stripperVec[1] = l;
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  incoming = *incomingData;
  //Serial.print("Bytes received: ");
  //Serial.println(len);
  //Serial.print("incoming data: ");
  //Serial.println(incoming,BIN);
}
/*
void setLeds(int indexLR, const int onOff[]){
  for (int i=0;i<nbMotors;i++){
    digitalWrite(ledPinArray[indexLR][i],onOff[i]);      
  }
}
*/
void setSpeed(int index,int speed){
  if (motorVec[index].motor.speed() != speed){
    motorVec[index].lastSpeed = round(motorVec[index].motor.speed());
    motorVec[index].motor.setSpeed(speed);
    Serial.println("set motor "    + 
                   String(index) + 
                   " speed "       + 
                   String(speed));
  }
}

void processIncoming(){
  static byte lastIncoming = 0xFF;
  if (incoming != lastIncoming){
    lastIncoming = incoming;
    for (int i=0;i<nbMotors;i++){
      byte b = incoming>>(2*i)&3;
      setSpeed(i,speedValue*speedFacotorVec[b]);
      //stripperVec[i]->update(); setSpeed(0);
      stripperVec[i]->setSpeed(speedFacotorVec[b]);
      //setLeds(i,ledOnOffVec[b]);
    }
  }
}

void runSpeed(int i){
  if (motorVec[i].motor.speed()){
    motorVec[i].motor.runSpeed();
  }
  else{
    motorVec[i].motor.disableOutputs();
  }
}

void runSpeeds(){
  for (int i=0;i<nbMotors;i++){
    if (motorVec[i].motor.speed() != motorVec[i].lastSpeed){
      runSpeed(i);
    }
  }
}

void loop() {
  processIncoming();
  runSpeeds();
  l->update();
  r->update();
}

#else ////// Robot Controller

void setupController(){
  memcpy(receiverAddress,RECEIVER_ADDRESS, 6);
  Serial.print("Receiver MAC: ");
  printMac(receiverAddress);
  for (int i=0;i<nbButtonPins;i++){
    pinMode(*buttonPinVec[i],INPUT_PULLUP);
  }
  for (int i=0;i<nbLedPins;i++){
    pinMode(*ledPinVec[i],OUTPUT);
  }
}

void updateOutgoing(){
  outgoing = 0;
  for (int i=0;i<nbButtonPins;i++){
    byte val = !digitalRead(*buttonPinVec[i]);
    outgoing |= (val )<<i;    
    digitalWrite(*ledPinVec[i],val);
  }
  for (int i =0;i<2;i++){
    digitalWrite(*ledPinVec[i+4],
                 !(digitalRead(*ledPinVec[0])||digitalRead(*ledPinVec[i+1])));
  }
}

void send(){
  Serial.print("outgoing data: ");
  Serial.println(outgoing,BIN);
  
  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(receiverAddress, (uint8_t *) &outgoing, sizeof(outgoing));
  Serial.println((result == ESP_OK) ? "Sent with success" : "Error sending the data"); 
}

void loop(){
  static byte lastOutgoing = 0;
  updateOutgoing();
  if (outgoing != lastOutgoing){
    lastOutgoing = outgoing;
    send();
  }
}

#endif
