#include "app.h"

const int DCRobot::_speedFacotorVec[] = {0,1,-1};

DCRobot::StepperRobot(const uint8_t *peer):Robot(){
  _leftStepper  = new AccelStepper(AccelStepper::DRIVER, _lStep,_lDir);
  _rightStepper = new AccelStepper(AccelStepper::DRIVER, _rStep,_rDir);

  _motorVec = new mvStruct[2];
  _motorVec[0] = {_rightStepper, 0};
  _motorVec[1] = {_leftStepper,  0};

  for (int i =0;i<_nbMotors;i++){
    _motorVec[i].motor->setMaxSpeed(_speedValue);
    _motorVec[i].motor->setSpeed(0);
    _motorVec[i].motor->disableOutputs();
  } 
  _setupNeoPixels();
}
void DCRobot::_setSpeed(int index,int speed){
  if (_motorVec[index].motor->speed() != speed){
    _motorVec[index].lastSpeed = round(_motorVec[index].motor->speed());
    _motorVec[index].motor->setSpeed(speed);
    Serial.println("set motor "    + 
                   String(index) + 
                   " speed "       + 
                   String(speed));
  }
}

void DCRobot::_toggleSpeedRange(){
  _speedValue = ((_speedValue == _fastSpeedValue) ? _slowSpeedValue 
                                                  : _fastSpeedValue);
  Serial.println("Set speeValue to : "  + String(_speedValue));
  for (int i=0;i<_nbMotors;i++){
      _stripperVec[i]->toggleCadence();
  }
}

void DCRobot::_processIncoming(){
  static byte lastIncoming = 0xFF;
  const byte toggleSpeedMask = 0B10000;
  
  if (_incoming != lastIncoming){
    if (_incoming & toggleSpeedMask){
    _toggleSpeedRange();
    }
    lastIncoming = _incoming;
    for (int i=0;i<_nbMotors;i++){
      byte b = _incoming>>(2*i)&3;
      _setSpeed(i,_speedValue*_speedFacotorVec[b]);
      _stripperVec[i]->setSpeed(_speedFacotorVec[b]);
    }
  }
}

void DCRobot::_runSpeed(int i){
  if (_motorVec[i].motor->speed()){
    _motorVec[i].motor->runSpeed();
  }
  else{
    _motorVec[i].motor->disableOutputs();
  }
}

void DCRobot::_runSpeeds(){
  for (int i=0;i<_nbMotors;i++){
    if (_motorVec[i].motor->speed() != _motorVec[i].lastSpeed){
      _runSpeed(i);
    }
  }
}

void DCRobot::loop() {
  _processIncoming();
  _runSpeeds();
  _updateStrippers();
}



/*StepperRobot : public App{
  protected:
    Stripper *_lStripper,*_rStripper,
             *stripperVec[2];// = {r,l};
    volatile byte _incoming = 0;

    const int _speedValue = 600,
              _lStep      = LSTEP,
              _lDir       = LDIR,
              _rStep      = RSTEP,
              _rDir       = RDIR;
    AccelStepper *_leftStepper, //(AccelStepper::DRIVER, _lStep,_lDir), // stepPin, dirPin)
                 *_rightStepper; //(AccelStepper::DRIVER, _rStep,_rDir); // stepPin, dirPin)
    struct mvStruct{
      AccelStepper *motor;
      int          lastSpeed;
    };

    mvStruct _motorVec[]; //= {{_rightStepper, 0},    // motor, lastSpeed}
                           //{_leftStepper,0}};

    const int _speedFacotorVec[],// = {0,1,-1},
              _nbMotors          = 2;

  public:
    StepperRobot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    void updateIncoming(const uint8_t *incomingData);
    virtual void loop();
*/
