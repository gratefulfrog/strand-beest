#include "app.h"

//#define STEPPER_ROBOT
#define DC_ROBOT

App * app;

void setup(){
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up...");
  #ifdef STEPPER_ROBOT
  app = new StepperRobot();
  #else  
    #ifdef DC_ROBOT
    app = new DCRobot();
    #else
    app = new Controller();
    #endif
  #endif
}
void loop(){
  app->loop();
}
