 #include "dcmotor.h"
 
 // abstract motor class
 void DCMotor2Speed::setDirection(int dir){
  _setDirection(dir);
 }

void DCMotor2Speed::runSpeed() const{
  _runSpeed();
}
void DCMotor2Speed::toggleSpeedRange(){
  _toggleSpeedRange();
}


// DC Motor concrete class


int DCMotorL298N::_nextIndex = 0;
int DCMotorL298N::_getIndex(){
  return DCMotorL298N::_nextIndex++;
}
                 
void DCMotorL298N::_toggleSpeedRange(){
  _speedValue = ((_speedValue == _fastSpeedValue) ? _slowSpeedValue 
                                                  : _fastSpeedValue);
  Serial.println("Motor : "  + String(_pwmIndex) + ": Set speedValue to : "  + String(_speedValue));
  /*for (int i=0;i<_nbMotors;i++){
      _stripperVec[i]->toggleCadence();
  }
  */
}

void DCMotorL298N::_setDirection(int dir){
  _direction = ((dir > 0) ? 1 : ((dir < 0) ? -1 : 0));
}

void DCMotorL298N::_runSpeed() const{
  switch (_direction){
    case 0:
      digitalWrite(_in1,LOW);
      digitalWrite(_in2,LOW);
      ledcWrite(_pwmIndex, 0);
      break;
    case -1:
      digitalWrite(_in2, HIGH); 
      digitalWrite(_in1, LOW);
      ledcWrite(_pwmIndex, _speedValue); 
      break;
    case 1:
      digitalWrite(_in1, HIGH); 
      digitalWrite(_in2, LOW); 
      ledcWrite(_pwmIndex, _speedValue);
      break;        
  }
}

DCMotorL298N::DCMotorL298N(int ePin, int i1Pin, int i2Pin):
  _en(ePin),
  _in1(i1Pin),
  _in2(i2Pin),
  _pwmIndex(DCMotorL298N::_getIndex()){
    _speedValue = _slowSpeedValue;
    pinMode(_en, OUTPUT);
    pinMode(_in1, OUTPUT);
    pinMode(_in2, OUTPUT);
    _setDirection(0);
    
    Serial.print(String("Motor: ") + String(_pwmIndex) + String(" : "));
    Serial.println(ledcSetup(_pwmIndex, _pwmFrequency, _pwmResolution));
    ledcAttachPin(_en, _pwmIndex);
  }
