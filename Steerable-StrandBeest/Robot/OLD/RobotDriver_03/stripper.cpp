#include "stripper.h"

const uint32_t Stripper::colorVec[] ={Stripper::blue,
                                      Stripper::red,
                                      Stripper::green};
  
void Stripper::cycle(){
  int count = 0,
      i     = startPix,
      dir = direction-1;
  strip->clear(); 
  while (count++<nbPixels){
   if (i == startPix){
      for (int j=0;j<cycleNbOn;j++){
        strip->setPixelColor(i, colorVec[direction]);
        i = dir ? (i+1)%nbPixels : (i == 0 ? 7 : i-1); //
        count++;
      }
    }
    strip->setPixelColor(i,0);
    i = dir ? (i+1)%nbPixels : (i == 0 ? 7 : i-1);
  }
  strip->show();
  startPix = i = dir ? (i+1)%nbPixels : (i == 0 ? 7 : i-1);
}
  
void Stripper::flash(){
//void theaterChaseLoop(int nbOn, uint32_t color,Adafruit_NeoPixel &strip,int &b,int bright=10) {
  strip->clear();     
  // 'c' counts up from 'b' to end of strip in steps of nbOn...
  for(int c=startPix; c<nbPixels; c += flashNbOn) {
    strip->setPixelColor(c, blue); 
  }
  strip->show(); // Update strip with new contents
  startPix=(startPix+1)%flashNbOn; 
}

    
Stripper::Stripper(int pin, int nbP, unsigned long updateDel, int bright): 
      nbPixels(nbP),
      updateDelay(updateDel),
      lastUpdateTime(millis()){
  strip = new Adafruit_NeoPixel(nbPixels, pin, NEO_GRB + NEO_KHZ800);
  strip->begin();
  strip->setBrightness(brightness);
  strip->clear();
  strip->show();
  setSpeed(0);
}

void Stripper::setSpeed(int speed){
  direction = speed < 0  ? 1 : (speed > 0 ? 2 : 0 );
}

void Stripper::toggleCadence(){
  updateDelay = ((updateDelay == defaultUpdateDelay) ? defaultUpdateDelay/speedRangeFactor 
                                                    : defaultUpdateDelay);
}

void Stripper::update(){
  #ifdef DEBUG
    static int count = 0;
  #endif
  long unsigned now = millis();
  if (now-lastUpdateTime > updateDelay){
    lastUpdateTime = now;
    if (direction){
      cycle();
      #ifdef DEBUG
        if (!(count++%100)){
          setSpeed(direction == 2 ? -1 : 1);
        }
      #endif
    }
    else{
      flash();
    }
  }
}
