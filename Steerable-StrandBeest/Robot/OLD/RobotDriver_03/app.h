#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include <AccelStepper.h>
#include "stripper.h"
#include "espMgr.h"

// Robot Pin defs
#define STRIP_PINL (26)
#define STRIP_PINR (25)
#define LSTEP      (27)
#define LDIR       (14)
#define RSTEP      (12)
#define RDIR       (13)

// Controller Pin defs
#define RFLEDPIN (32)
#define RRLEDPIN (33)
#define RSLEDPIN (23)

#define LFLEDPIN (27)
#define LRLEDPIN (14)
#define LSLEDPIN (22)

#define FSTLEDPIN (19)
#define SLWLEDPIN (18)

#define LFBUTPIN (13)
#define LRBUTPIN (12)

#define RFBUTPIN (26)
#define RRBUTPIN (25)

#define STBUTPIN (21)

//// end of defines //////

class ESPMgr;
class Robot;

class App{
  protected:
    ESPMgr *espMgr;
  public:
    App(const uint8_t *peer, Robot *robotPtr = NULL);
    virtual void loop() = 0;
};

class Robot: public App{
  protected:
    static const int  _nbMotors;   //= 2;     
    volatile byte     _incoming = 0;

    Stripper *_lStripper,*_rStripper,
             *_stripperVec[2];// = {r,l};

    void _updateStrippers();
    void _setupNeoPixels();
  
  public:
    Robot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    void updateIncoming(const uint8_t *incomingData);
};

class StepperRobot : public Robot{
  protected:
    static const int _speedFacotorVec[]; // = {0,1,-1},         

    const int _fastSpeedValue = 600,
              _slowSpeedValue = 400,
              _lStep      = LSTEP,
              _lDir       = LDIR,
              _rStep      = RSTEP,
              _rDir       = RDIR;
    int _speedValue = _slowSpeedValue;
    
    AccelStepper *_leftStepper, //(AccelStepper::DRIVER, _lStep,_lDir), // stepPin, dirPin)
                 *_rightStepper; //(AccelStepper::DRIVER, _rStep,_rDir); // stepPin, dirPin)
    struct mvStruct{
      AccelStepper *motor;
      int          lastSpeed;
    };

    mvStruct *_motorVec; //= {{_rightStepper, 0},    // motor, lastSpeed}
                         //{_leftStepper,0}};
    
    void _toggleSpeedRange();
    void _setSpeed(int index,int speed);
    void _processIncoming();
    void _runSpeed(int i);
    void _runSpeeds();
  
  public:
    StepperRobot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
};

class Controller : public App{
  protected:
    static const int _nbLedPins       = 8,
                     _nbButtonPins    = 5;
    const int _rightForwardLedPin    = RFLEDPIN,
              _rightReverseLedPin    = RRLEDPIN,
              _rightStoppedLedPin    = RSLEDPIN,
              _leftForwardLedPin     = LFLEDPIN,
              _leftReverseLedPin     = LRLEDPIN,
              _leftStoppedLedPin     = LSLEDPIN,
              _fastLedPin            = FSTLEDPIN,
              _slowLedPin            = SLWLEDPIN,
              _leftForwardButtonPin  = LFBUTPIN,
              _leftReverseButtonPin  = LRBUTPIN,
              _rightForwardButtonPin = RFBUTPIN,
              _rightReverseButtonPin = RRBUTPIN,
              _speedToggleButtonPin  = STBUTPIN,
              *_buttonPinVec[_nbButtonPins] = { &_rightForwardButtonPin,
                                                &_rightReverseButtonPin,
                                                &_leftForwardButtonPin,
                                                &_leftReverseButtonPin,
                                                &_speedToggleButtonPin},
              *_ledPinVec[_nbLedPins]    = {&_rightForwardLedPin,
                                            &_rightReverseLedPin, 
                                            &_leftForwardLedPin,
                                            &_leftReverseLedPin,
                                            &_rightStoppedLedPin,
                                            &_leftStoppedLedPin,
                                            &_fastLedPin,
                                            &_slowLedPin}; 
    byte _outgoing = 0;
    
    
    void _send();
    void _updateOutgoing();
    void _doSpeedChange();
    void _updateSpeedLED();
    
  public:
    Controller(const uint8_t *peer = ESPMgr::ESP32_C_Address);
    virtual void loop();
};

#endif
