#include "app.h"

const int StepperRobot::_speedFacotorVec[] = {0,1,-1};

StepperRobot::StepperRobot(const uint8_t *peer):Robot(){
  _leftStepper  = new AccelStepper(AccelStepper::DRIVER, _lStep,_lDir);
  _rightStepper = new AccelStepper(AccelStepper::DRIVER, _rStep,_rDir);

  _motorVec = new mvStruct[2];
  _motorVec[0] = {_rightStepper, 0};
  _motorVec[1] = {_leftStepper,  0};

  for (int i =0;i<_nbMotors;i++){
    _motorVec[i].motor->setMaxSpeed(_speedValue);
    _motorVec[i].motor->setSpeed(0);
    _motorVec[i].motor->disableOutputs();
  } 
  _setupNeoPixels();
}
void StepperRobot::_setSpeed(int index,int speed){
  if (_motorVec[index].motor->speed() != speed){
    _motorVec[index].lastSpeed = round(_motorVec[index].motor->speed());
    _motorVec[index].motor->setSpeed(speed);
    Serial.println("set motor "    + 
                   String(index) + 
                   " speed "       + 
                   String(speed));
  }
}

void StepperRobot::_toggleSpeedRange(){
  _speedValue = ((_speedValue == _fastSpeedValue) ? _slowSpeedValue 
                                                  : _fastSpeedValue);
  Serial.println("Set speeValue to : "  + String(_speedValue));
  for (int i=0;i<_nbMotors;i++){
      _stripperVec[i]->toggleCadence();
  }
}

void StepperRobot::_processIncoming(){
  static byte lastIncoming = 0xFF;
  const byte toggleSpeedMask = 0B10000;
  
  if (_incoming != lastIncoming){
    if (_incoming & toggleSpeedMask){
    _toggleSpeedRange();
    }
    lastIncoming = _incoming;
    for (int i=0;i<_nbMotors;i++){
      byte b = _incoming>>(2*i)&3;
      _setSpeed(i,_speedValue*_speedFacotorVec[b]);
      _stripperVec[i]->setSpeed(_speedFacotorVec[b]);
    }
  }
}

void StepperRobot::_runSpeed(int i){
  if (_motorVec[i].motor->speed()){
    _motorVec[i].motor->runSpeed();
  }
  else{
    _motorVec[i].motor->disableOutputs();
  }
}

void StepperRobot::_runSpeeds(){
  for (int i=0;i<_nbMotors;i++){
    if (_motorVec[i].motor->speed() != _motorVec[i].lastSpeed){
      _runSpeed(i);
    }
  }
}

void StepperRobot::loop() {
  _processIncoming();
  _runSpeeds();
  _updateStrippers();
}
