#ifndef STRIPPER_H
#define STRIPPER_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

//#define DEBUG

class Stripper{
  protected:
    const static uint32_t red   = 0xFF0000,
                          green = 0x00FF00,
                          blue  = 0x0000FF,
                          colorVec[]; //={blue,red,green};
    const static int defaultBrightness = 30,
                     cycleNbOn         =  3,
                     flashNbOn         =  2,
                     defaultNbPixels   =  8;
    const static unsigned long defaultUpdateDelay = 50;
  
    void cycle();
    void flash();

    const unsigned long updateDelay;
    const int           nbPixels;

    unsigned long       lastUpdateTime;

    int direction  = 2,
        brightness = defaultBrightness,
        startPix   = 0;

    Adafruit_NeoPixel *strip;
    
  public:
    Stripper(int pin, 
             int nbPix               = defaultNbPixels,
             unsigned long updateDel = defaultUpdateDelay,
             int           bright    = defaultBrightness);
    void setSpeed(int speed);
    void update();  
};


#endif
