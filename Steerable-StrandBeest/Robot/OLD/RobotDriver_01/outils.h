#ifndef OUTILS_H
#define OUTILS_H

#include <Arduino.h>

extern bool macEquals(uint8_t macL[],uint8_t macR[]);
extern void printMac(uint8_t mac[]);
extern void showPins();

#endif
