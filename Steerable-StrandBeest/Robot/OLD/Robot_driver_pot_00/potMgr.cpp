#include "potMgr.h"

PotMgr::PotMgr(int pPin):_potPin(pPin){
  pinMode(_potPin,INPUT);
}

byte PotMgr::getNormedValue() const{
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  // reduce the resolution to steps of 10 on [0,255] i.e. one byte
  byte res = min(_outputMaxVal,
                 (int)(_outputResolutionDecimal*round(map(potValue,0,_adcMaxVal,0,_outputMaxVal)/(float)_outputResolutionDecimal)));
  return res;
}
