#include "robot.h"


DCRobot::DCRobot(){
  _setupNeoPixels();
}
void DCRobot::_setupNeoPixels(){
  _lStripper = new Stripper(STRIP_PINL);
  _lStripper->setDirection(0);
  _rStripper = new Stripper(STRIP_PINR);
  _rStripper->setDirection(0);
  _stripperVec[0] = _rStripper;
  _stripperVec[1] = _lStripper;
}

void DCRobot::setDirection(int dir){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->setDirection(dir);
    _stripperVec[i]->setDirection(dir);
  }
}
void DCRobot::toggleSpeedRange(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->toggleSpeedRange();
  }
}
void DCRobot::setSpeed(int index, byte newSpeed){
  _motorVec[index]->setSpeed(newSpeed);
  _stripperVec[index]->updateCadence(newSpeed);
}

void DCRobot::runSpeed(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->runSpeed();
    _stripperVec[i]->update();
  }
}

void DCRobot::_updateStrippers(){
  for (int i = 0; i< _nbMotors;i++){
    _stripperVec[i]->update();
  }
}

DCRobot3Pin::DCRobot3Pin(const int *pinVec):DCRobot(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor3Pin(pinVec[0+i*DCMotor3Pin::nbPins],
                                     pinVec[1+i*DCMotor3Pin::nbPins],
                                     pinVec[2+i*DCMotor3Pin::nbPins]);
  }
}

DCRobot2Pin::DCRobot2Pin(const int *pinVec):DCRobot(){
 for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor2Pin(pinVec[0+i*DCMotor2Pin::nbPins],
                                     pinVec[1+i*DCMotor2Pin::nbPins]);
  }
}
