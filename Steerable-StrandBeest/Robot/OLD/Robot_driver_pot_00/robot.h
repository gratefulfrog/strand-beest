#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "motor.h"
#include "stripper.h"

#define STRIP_PINL (5)
#define STRIP_PINR (17)

class DCRobot{
  protected:
    static const int _nbMotors = 2;
    DCMotor2Speed *_motorVec[_nbMotors];
    virtual void _unused() = 0;
    Stripper *_lStripper,*_rStripper,
             *_stripperVec[2];// = {r,l};

    void _updateStrippers();
    void _setupNeoPixels();
  public:
    DCRobot();
    void setDirection(int dir);
    void toggleSpeedRange();
    void setSpeed(int index, byte newSpeed);
    void runSpeed();
};

class DCRobot3Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot3Pin(const int *pinVec);
};

class DCRobot2Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot2Pin(const int *pinVec);
};

#endif
