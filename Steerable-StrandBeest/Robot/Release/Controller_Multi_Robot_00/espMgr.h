#ifndef ESPMGR_H
#define ESPMGR_H

#include <Arduino.h>
#include <esp_now.h>
#include <WiFi.h>

#include "outils.h"

typedef uint8_t (mac_address)[6];

typedef struct AddressName {
  String name;
  const uint8_t *address;
} AddressName;

class ESPMgr{
  public:
    static const mac_address ESP32_B_Address, // = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},    // current config robot
                             ESP32_C_Address, // = {0x24, 0x0A, 0xC4, 0x59, 0xBA, 0x34},    // spare MCU 
                             ESP32_D_Address, // = {0xAC, 0x67, 0xB2, 0x3D, 0x5F, 0x50},     // current config controller
                             ESP32_E_Address, // = {0xAC, 0x67, 0xB2, 0x3D, 0x52, 0x30},    // spare MCU   
                             ESP32_F_Address, // = {0xAC, 0x67, 0xB2, 0x50, 0xB3, 0xF8},    // spare MCU   
                             ESP32_G_Address, // = {0xAC, 0x67, 0xB2, 0x50, 0x7C, 0xEC},    // spare MCU   
                             ESP32_H_Address,
                             ESP32_I_Address,  
                             ESP32_J_Address;
      static AddressName anVec[6];
                         
  protected:    
    static ESPMgr *_thisPtr;  // used for reception of data by static method
    
    static void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status);
    static void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len);
 
    void setupWifi();
    void registerAndAddPeers(const uint8_t  **peerLis, uint8_t nbPeers);
    
    uint8_t *_incomingDataHolder;
    esp_now_peer_info_t *_peerInfo;   // cannot go out of scope, for some reason??    
    
  public:
    String name; // for debugging    
    ESPMgr(uint8_t incomingDataHolder[], const uint8_t **peerAddressLis = NULL, uint8_t nbPeers =0);
    void sendByteVec(const byte outgoing[],uint8_t msgLen) const;
};

#endif
