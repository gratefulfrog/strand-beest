#include "potMgr.h"

PotMgr::PotMgr(uint8_t pin,uint8_t outputResolution):
  _potPin(pin),
  maxOutputVal(pow(2,outputResolution)-1){
    pinMode(pin,INPUT);
}
    
uint16_t PotMgr::getReading() const{
  return map(analogRead(_potPin),0,maxInputVal,0,maxOutputVal);
}
