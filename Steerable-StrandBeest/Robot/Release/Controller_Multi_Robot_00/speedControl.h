#ifndef SPEED_CONTROL_H
#define SPEED_CONTROL_H

#include <Arduino.h>
#include "pixelMgr.h"
#include "potMgr.h"

class SpeedControl{
  protected:
    PotMgr &_potMgr;
    UpdateableBlinkerPixelSegment &_ubps;
  public:
    SpeedControl(PotMgr &potMgr, UpdateableBlinkerPixelSegment &ubps);
    void update();  // does not call show()!
    uint8_t getSpeed() const;
};

#endif
