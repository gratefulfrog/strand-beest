#ifndef RF_BUTTON_MGR_H
#define RF_BUTTON_MGR_H

#include <Arduino.h>

class RFButtonMgr{
  public:
    static int toInt(uint8_t rfVal);
  
  protected:
    static const int _byteIntVec[]; // maps byte values to int 0->0, 1->-1,2->1
    static const uint8_t _nbPins;
    const uint8_t  _pinVec[2]; //reverse button, forard button
    void _setupPins() const;

  public:
    RFButtonMgr(uint8_t rPin, uint8_t fPin);
    RFButtonMgr(uint8_t *pinVec);
    uint8_t getRFByte() const;  // returns 0bfr where f and r are 1 if button is down
};

class ButtonMgr{
  public:
    static int toInt(uint8_t valByte, bool getLeft);
    
  protected:
    RFButtonMgr *_bmVec[2];
    static const uint8_t _nbButtonMgrs = 2;

    void _setupMgrs(uint8_t pinVec[][2]);

  public:
    ButtonMgr(uint8_t rrPin,
              uint8_t rfPin,
              uint8_t lrPin,
              uint8_t  lfPin);
    ButtonMgr(const uint8_t *pinVec);
    uint8_t getByteValue() const;  // returns 0bKJ, where K and J are 0b0, 0b01 (reverse button pushed),0b10  (forward button pushed)
};

#endif
