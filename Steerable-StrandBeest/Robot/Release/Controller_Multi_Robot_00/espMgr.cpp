#include "espMgr.h"

const mac_address ESPMgr::ESP32_B_Address = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},    // current config robot
                  ESPMgr::ESP32_C_Address = {0x24, 0x0A, 0xC4, 0x59, 0xBA, 0x34},    // spare MCU 
                  ESPMgr::ESP32_D_Address = {0xAC, 0x67, 0xB2, 0x3D, 0x5F, 0x50},    // current config controller
                  ESPMgr::ESP32_E_Address = {0xAC, 0x67, 0xB2, 0x3D, 0x52, 0x30},    // spare MC
                  ESPMgr::ESP32_F_Address = {0xAC, 0x67, 0xB2, 0x50, 0xB3, 0xF8},    // spare MCU   
                  ESPMgr::ESP32_G_Address = {0xAC, 0x67, 0xB2, 0x50, 0x7C, 0xEC},    // spare MCU   
                  ESPMgr::ESP32_H_Address = {0x94, 0xB9, 0x7E, 0xC2, 0xEF, 0x98},
                  ESPMgr::ESP32_I_Address = {0x94, 0xB9, 0x7E, 0xC0, 0xC8, 0x38},
                  ESPMgr::ESP32_J_Address = {0x94, 0xB9, 0x7E, 0xD2, 0xF3, 0x8C};

AddressName ESPMgr::anVec[6] = {{String("ESP32_B"), ESPMgr::ESP32_B_Address},
                                {String("ESP32_C"), ESPMgr::ESP32_C_Address},
                                {String("ESP32_D"), ESPMgr::ESP32_D_Address},
                                {String("ESP32_E"), ESPMgr::ESP32_E_Address},
                                {String("ESP32_F"), ESPMgr::ESP32_F_Address},   
                                {String("ESP32_G"), ESPMgr::ESP32_G_Address}};

ESPMgr *ESPMgr::_thisPtr;

void ESPMgr::OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {  
  if (status != ESP_NOW_SEND_SUCCESS){
    Serial.print("Delivery Fail: ");
    printMac(mac_addr);
  }
}

void ESPMgr::OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(_thisPtr->_incomingDataHolder, incomingData,len); 
}

void  ESPMgr::setupWifi(){
  WiFi.mode(WIFI_STA);

  mac_address myMac;
  WiFi.macAddress(myMac);

  for (uint8_t i = 0;i< 6;i++){
    if(everyEqual(anVec[i].address,myMac,6)){
      name = anVec[i].name;
      break;
    }
  }
  
  Serial.print("My MAC: ");
  printMac(myMac);
 
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    while(1);
  } 
}

void ESPMgr::registerAndAddPeers(const uint8_t **peerAddressLis,uint8_t nbPeers){  
  _peerInfo = new esp_now_peer_info_t();
  _peerInfo->channel = 0;   
  _peerInfo->encrypt = false;

  for (uint8_t i=0;i<nbPeers;i++){
    memcpy(_peerInfo->peer_addr, (uint8_t*)peerAddressLis[i],6);
    if (esp_now_add_peer(_peerInfo) != ESP_OK){
      Serial.print("Failed to add peer: ");
      printMac(_peerInfo->peer_addr);
      while(1);
    }
    else{
      Serial.print("Peer added: ");
      printMac(peerAddressLis[i]);
    }
  }
}

ESPMgr::ESPMgr(uint8_t *incomingDataHolder, const uint8_t **peerAddressLis, uint8_t nbPeers):
  _incomingDataHolder(incomingDataHolder){

  ESPMgr::_thisPtr = this;
  
  setupWifi(); 

  if (nbPeers && peerAddressLis){   // it's a sender
    esp_now_register_send_cb(OnDataSent);
    registerAndAddPeers(peerAddressLis,nbPeers);      
  }
  else{  // it's a receiver
    esp_now_register_recv_cb(OnDataRecv);
  }
}

void ESPMgr::sendByteVec(const uint8_t *outgoing,uint8_t msgLength) const{
  esp_err_t result = esp_now_send(0,  outgoing, msgLength);
  if (result != ESP_OK){
    Serial.println("Send Failed!"); 
  }
}
