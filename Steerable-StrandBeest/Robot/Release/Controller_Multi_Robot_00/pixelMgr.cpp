#include "pixelMgr.h"

/////////////////////////////////////////////////////////////////////
///////////////////     PIXEL MGR        ////////////////////////////
/////////////////////////////////////////////////////////////////////
PixelMgr::PixelMgr(uint8_t pin, 
                   uint8_t nbPixels, 
                   uint8_t brightness):
  _neoPixels(new Adafruit_NeoPixel(nbPixels, pin, NEO_GRB + NEO_KHZ800)){
    _neoPixels->begin();
    _neoPixels->setBrightness(brightness);
    _neoPixels->clear();  // start with all off
    _neoPixels->show();
}
void PixelMgr::show(){
  _neoPixels->show();
}

/////////////////////////////////////////////////////////////////////
///////////////////     PIXEL SEGMENT    ////////////////////////////
/////////////////////////////////////////////////////////////////////
PixelSegment::PixelSegment(PixelMgr &pm,
                           uint8_t startPixel,
                           uint8_t nbPixels): 
  _pixelMgr(pm),
  _startIndex(startPixel),
  _nbPixels(min((int)nbPixels,(int)pm._neoPixels->numPixels())),
  _endIndexPlusOne(_startIndex+nbPixels){
  // check that nbPixl is <= total pixels managed
}
void PixelSegment::setBrightness(uint8_t brightness){
  _pixelMgr._neoPixels->setBrightness(brightness); 
}
void PixelSegment::clear(){
  for (uint8_t i=0;i<_nbPixels;i++){
    clear(i);
  }
}
void PixelSegment::clear (uint8_t index){
  setColor(index+_startIndex,PixelMgr::black);
}
void PixelSegment::setColor(uint32_t color){
  for (uint8_t i=0;i<_nbPixels;i++){
    setColor(i,color);
  }
}
void PixelSegment::setColor(uint8_t index,uint32_t color){
  _pixelMgr._neoPixels->setPixelColor(index+_startIndex,color);
}
void PixelSegment::setColor(uint8_t r,uint8_t g,uint8_t b){
  for (uint8_t i=0;i<_nbPixels;i++){
    setColor(i,r,g,b);
  }
}
void PixelSegment::setColor(uint8_t index,uint8_t r,uint8_t g,uint8_t b){
  _pixelMgr._neoPixels->setPixelColor(index+_startIndex,r,g,b);
}
uint8_t PixelSegment::getNbPixels() const{
  return _nbPixels;
}
void PixelSegment::activate(bool newVal){
  _active = newVal;
}
/////////////////////////////////////////////////////////////////////
/////////////////// UPDATEABLE PIXEL SEGMENT ////////////////////////
/////////////////////////////////////////////////////////////////////
UpdateablePixelSegment::UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels):
  Updateable(maxUpdatePeriod),
  PixelSegment(pm,startPixel,nbPixels){
}
UpdateablePixelSegment::UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels,long unsigned udPeriod):
  Updateable(),
  PixelSegment(pm,startPixel,nbPixels){
    setPeriod(udPeriod);
}
UpdateablePixelSegment::UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels,
                                               long unsigned udPeriod, float dutyCycle):
  Updateable(udPeriod,dutyCycle),
  PixelSegment(pm,startPixel,nbPixels){
    setPeriod(udPeriod);
}
void UpdateablePixelSegment::setPeriod(unsigned long newPeriod){
  Updateable::setPeriod(max(min(newPeriod,maxUpdatePeriod),minUpdatePeriod));
}

/////////////////////////////////////////////////////////////////////
/////////////////// UPDATEABLE BLINKER PIXEL SEGMENT /////////////////
/////////////////////////////////////////////////////////////////////
void UpdateableBlinkerPixelSegment::_onFunc(long unsigned now){
  if (!_active) {
    return;
  }
  uint8_t startIndex = _direction > 0 ? 0 :  _nbPixels -1;
  for (uint8_t i = 0;i<_nbOn;i++){
    setColor(startIndex+i*_direction,_onColor);
  }
}

void UpdateableBlinkerPixelSegment::_offFunc(long unsigned now){
  if (!_active) {
    return;
  }
  clear();
}

UpdateableBlinkerPixelSegment::UpdateableBlinkerPixelSegment(PixelMgr &pm, 
                                                             uint8_t startPixel,
                                                             uint8_t nbPixels,
                                                             long unsigned udPeriod,
                                                             uint32_t onColor,
                                                             uint8_t  nbOn,
                                                             int direction, // 1 is left to right ; -1 is right to left
                                                             uint32_t offColor):
  UpdateablePixelSegment(pm,startPixel,nbPixels,udPeriod),
  _onColor(onColor),
  _nbOn(nbOn),
  _direction(direction),
  _offColor(offColor){
}

void UpdateableBlinkerPixelSegment::setOnColor(uint32_t col){
  _onColor = col;
}
void UpdateableBlinkerPixelSegment::setNbOn(uint8_t nb){
  _nbOn = nb;
}

void UpdateableBlinkerPixelSegment::setOffColor(uint32_t col){
  _offColor = col;
}
void UpdateableBlinkerPixelSegment::clear(){
  setColor(_offColor);
}
void UpdateableBlinkerPixelSegment::clear (uint8_t index){
  setColor(index, _offColor);
}

/////////////////////////////////////////////////////////////////////
////////// UPDATEABLE THEATER FLASHER PIXEL SEGMENT /////////////////
/////////////////////////////////////////////////////////////////////
UpdateableTheaterFlasherPixelSegment::UpdateableTheaterFlasherPixelSegment( PixelMgr &pm,
                                                                            uint8_t startPixel,
                                                                            uint8_t nbPixels,
                                                                            long unsigned udPeriod,
                                                                            uint8_t nbOn,
                                                                            uint8_t nbOff,
                                                                            uint32_t onColor,
                                                                            uint32_t offColor):
  UpdateableBlinkerPixelSegment(pm, 
                                startPixel,
                                nbPixels,
                                udPeriod,
                                onColor,
                                nbOn,
                                offColor),
  _nbOff(nbOff){
}

void UpdateableTheaterFlasherPixelSegment::setNbOff(uint8_t nb){
  _nbOff = nb;
}

void UpdateableTheaterFlasherPixelSegment::_onFunc(long unsigned now){
  if (!_active) {
    return;
  }
  for(uint8_t i=0;i<_nbPixels;){
    for (uint8_t j=i;j<i+_nbOn && j<_nbPixels;j++){
      setColor(j,_onColor);
    } 
    for (uint8_t j=i+_nbOn;j<i+_nbOn+_nbOff && j<_nbPixels;j++){
      setColor(j,_offColor);
    }
    i+=(_nbOn +_nbOff);
  }
}

void UpdateableTheaterFlasherPixelSegment::_offFunc(long unsigned now){
  if (!_active) {
    return;
  }
  for(uint8_t i=0;i<_nbPixels;){
    for (uint8_t j=i;j<i+_nbOff && j<_nbPixels;j++){
      setColor(j,_offColor);
    }  
    for (uint8_t j=i+_nbOff;j<i+_nbOn+_nbOff && j<_nbPixels;j++){
      setColor(j,_onColor);
    }
    i+=(_nbOn +_nbOff);
  }
}

/////////////////////////////////////////////////////////////////////
////////// UPDATEABLE THEATER RUNNER PIXEL SEGMENT /////////////////
/////////////////////////////////////////////////////////////////////

void incWithDirection(uint8_t &index,int val,uint8_t nb){
  if (val <0){
    index = index? index + val
                 : nb    + val;
  }
  else{ 
    index =  index+val == nb ? 0
                             : index+val;
  }
}

void UpdateableTheaterRunnerPixelSegment::_onFunc(long unsigned now){
  if (!_active) {
    return;
  }
  clear();
  uint8_t count = 0,
          index = _firstOn;
  while(count++<_nbOn){
    setColor(index,_onColor);
    incWithDirection(index,_direction,_nbPixels);
  }
  incWithDirection(_firstOn,_direction,_nbPixels);
}

void UpdateableTheaterRunnerPixelSegment::_offFunc(long unsigned now){
  if (!_active) {
    return;
  }
  clear();
}

UpdateableTheaterRunnerPixelSegment::UpdateableTheaterRunnerPixelSegment( PixelMgr &pm,
                                                                          uint8_t startPixel,
                                                                          uint8_t nbPixels,
                                                                          long unsigned udPeriod,
                                                                          uint8_t nbOn,
                                                                          uint32_t onColor,
                                                                          int      direction,
                                                                          uint32_t offColor):
  UpdateableTheaterFlasherPixelSegment(pm,
                                       startPixel,
                                       nbPixels,
                                       udPeriod,
                                       nbOn,
                                       0,
                                       onColor,
                                       offColor){
    _firstOn = direction<0 ? nbPixels-1 : 0;
    setDirection(direction);
}

void UpdateableTheaterRunnerPixelSegment::setDirection (int dir){
  _direction = dir>=0 ? 1 : -1;
}
                                       
