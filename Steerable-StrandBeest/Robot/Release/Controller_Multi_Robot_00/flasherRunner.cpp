#include "flasherRunner.h"


FlasherRunner::FlasherRunner(UpdateableTheaterFlasherPixelSegment &fps,
                             UpdateableTheaterRunnerPixelSegment &rps):
  _utf(fps),
  _utr(rps),
  _colorVec({stopColor, reverseColor, forwardColor}){
    _utf.activate(false);
}
void FlasherRunner::setSpeed(uint8_t speed){
  unsigned long newPeriod = map(speed,255,0,_utf.minUpdatePeriod,_utf.maxUpdatePeriod);
  _utf.setPeriod(newPeriod);
  _utr.setPeriod(newPeriod);
}

void FlasherRunner::setDrection(uint8_t val,uint8_t index) { // 0 stopped, 1 revers, 2 forward, index 0 means right, dir is +1, 1 -> dir -1
  _utr.activate(val);
  _utf.activate(!val);
  _utr.setOnColor(val & 0b0001 ? PixelMgr::red : PixelMgr::green);
  _utr.setDirection(val & 0b0001 ? (!index ? -1 : 1)
                                 : (!index ? 1 : -1));
}

void FlasherRunner::update(uint8_t rawDirectionValue, // where it is 0 for stopped, 1 for reverse, 2 for forward;
                           uint8_t speedByte){       
                           }

FRMgr::FRMgr(FlasherRunner *frVec[nbFRs]){
  for (uint8_t i=0;i<FRMgr::nbFRs;i++){
    _frVec[i] = frVec[i];
  }
}
void FRMgr::update(uint8_t *dirSpeedVec){
  for (uint8_t i=0;i<FRMgr::nbFRs;i++){
    _frVec[i]->setSpeed(dirSpeedVec[1]);
    _frVec[i]->setDrection(dirSpeedVec[0]>>(2*i)&0b11,i); // i indicates r,l ie. 0,1
  }
}//0bJJKK, where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
