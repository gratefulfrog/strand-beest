#ifndef APP_H
#define APP_H

#include "espMgr.h"
#include "config.h"
#include "directionIndicator.h"
#include "flasherRunner.h"
#include "headLight.h"
#include "motor.h"
#include "outils.h"
#include "pixelMgr.h"
#include "potMgr.h"
#include "rfButtonMgr.h"
#include "speedControl.h"

typedef struct robotDefStruct{
  uint8_t pixelPin;
  uint8_t nbPixels;
  uint8_t motorPinVec[ROBOT_NB_MOTOR_PINS];
}robotDefStruct;

extern robotDefStruct robotDefs;

typedef struct controllerDefStruct{
  uint8_t        pixelPin;
  uint8_t        nbPixels;
  uint8_t        potPin;
  uint8_t        buttonPinVec[CONTROLLER_NB_BUTTON_PINS];
  const uint8_t *robotAddressVec[CONTROLLER_NB_ROBOTS];
  uint8_t        nbRobots;
}controllerDefStruct;

extern controllerDefStruct controllerDefs;

class App{
  protected:
    static const uint8_t _msgLength = 2;
    uint8_t              _lastMsg[_msgLength]    = {0,0},
                         _currentMsg[_msgLength] = {0,0};
    virtual bool _updateCurrentMsg();  // return true if something new
    virtual bool _updateOutgoing() = 0;  // return true if something new
    ESPMgr *_espMgr;
    PixelMgr *_pm;
  public:
    App(ESPMgr *espMgr, uint8_t pixelPin, uint8_t nbPixels);
    virtual void loop() = 0;
};

class ControllerApp: public App{
  protected:
    virtual bool _updateOutgoing();
    DirectionIndicatorMgr         *_dim;
    PixelSegment                  *_disVec[DirectionIndicatorMgr::nbDirectionIndicators];
    ButtonMgr                     *_bm;
    PotMgr                        *_potMgr;
    UpdateableBlinkerPixelSegment *_speedBPS;
    SpeedControl                  *_sc;

    void _sendCurrentMsg() const;
  public:
    ControllerApp(controllerDefStruct &defs);
    void loop();
};

class RobotApp: public App{
  public:
    static const uint8_t  nbMotors = 2;
  protected:
    virtual bool _updateOutgoing(){return false;}
    MotorMgr              *_mm;
    HeadLightMgr          *_hlm;
    FRMgr                 *_frm;  

    PixelSegment          *_hlsVec[2*nbMotors];
    FlasherRunner         *_frVec[nbMotors];

    void _updateManagers();

  public:
    RobotApp(robotDefStruct &defs);
    void loop();
};

#endif;
