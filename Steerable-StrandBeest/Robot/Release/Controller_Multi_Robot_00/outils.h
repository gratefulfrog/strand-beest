#ifndef OUTILS_H
#define OUTILS_H

#include <Arduino.h>

typedef bool (*byte2byte)(uint8_t,uint8_t);

extern bool macEquals(const uint8_t macL[], const uint8_t macR[]);
extern void printMac(const uint8_t mac[]);
extern String byte2BinString(uint8_t b);
extern void showByteVec(const uint8_t *vec, uint8_t len,bool carReturn = true);

extern bool any(const uint8_t *v0, const uint8_t *v1, uint8_t nb, byte2byte func);
bool every(const uint8_t *v0, const uint8_t *v1, uint8_t nb, byte2byte func);
extern bool anyEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb);
extern bool anyNotEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb);
extern bool everyEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb);
extern bool everyNotEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb);

#endif
