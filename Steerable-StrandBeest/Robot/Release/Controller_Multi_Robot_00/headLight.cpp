#include "headLight.h"

HeadLight::HeadLight(PixelSegment &ps,bool isFront):
  _ps(ps),
  _colorVec({stopColor, 
             (isFront ? reverseColor : forwardColor),
             (isFront ? forwardColor : reverseColor)}){
}
                           
void HeadLight::update(uint8_t rawDirectionValue){ // where it is 0 for stopped, 1 for reverse, 2 for forward;
  _ps.setColor(_colorVec[rawDirectionValue]);
}

HeadLightMgr::HeadLightMgr(PixelSegment *psVec[4]){
  bool isFrontVec[] = {false,true,true,false}; // rightRear, rightFront, LeftFront,LeftRear
  for (uint8_t i = 0; i< nbHeadLights;i++){
    _hlVec[i] = new HeadLight(*psVec[i],isFrontVec[i]);
  }
}

void HeadLightMgr::update(uint8_t rawDirectionByte){
  for (uint8_t i = 0; i< nbHeadLights;i++){
    _hlVec[i]->update((rawDirectionByte>>(i/2)*2)&0b11);
  }
}
