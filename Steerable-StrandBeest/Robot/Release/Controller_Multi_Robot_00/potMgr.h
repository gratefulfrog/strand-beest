#ifndef POTMGR_H
#define POTMGR_H

#include <Arduino.h>

class PotMgr{
  public:
    static const uint8_t inputResolution         = 12,  //bits
                         defaultOutputResoltuion = 8;   //bits

    const uint16_t maxInputVal   = pow(2,inputResolution)-1,
                   maxOutputVal;
  protected:
    const uint8_t  _potPin;
    
  public:
    PotMgr(uint8_t pin,uint8_t outputResolution = defaultOutputResoltuion);
    uint16_t getReading() const;
};

#endif
