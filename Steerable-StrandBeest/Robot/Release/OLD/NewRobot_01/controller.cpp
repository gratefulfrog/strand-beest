

#include "app.h"

void Controller::_send(){
  espMgr->sendByteVec(_outgoingVec,_msgLength);
}

void Controller::_updateOutgoing(){
  for (int i=0; i< _msgLength;i++){
    _outgoingVec[i] = 0;
  }
  for (int i=0;i<_msgLength;i++){
    _outgoingVec[0] |= (_blcVec[i]->getAndUpdate())<<(i*2);
  }
  _outgoingVec[1] = _psc->getAndUpdate();
}

Controller::Controller(const uint8_t *peer): App(peer){
  _psc = new PotSpeedControl(_potPin,_potLedPin);
  
  for (int i=0;i<_nbButtonLedCombos;i++){
    _blcVec[i] = new ButtonLedCombo(_butPinVecVec[i],_ledPinVecVec[i]); 
  }
}

bool Controller::_updateNeeded(){
  
  bool res = _lastOutgoingVec[0] != _outgoingVec[0];
  _lastOutgoingVec[0] = _outgoingVec[0];  
  return res;
}

void Controller::showOutgoing() const{
  Serial.print(_outgoingVec[0],BIN);
  Serial.print(" : ");
  Serial.println(_outgoingVec[1],DEC);
}
void Controller::loop(){
  _updateOutgoing();
  if (_updateNeeded()){
    showOutgoing();
    _send();
  }
}
