#ifndef LED_H
#define LED_H

#include<Arduino.h>
#include "pwmMgr.h"

class Led {
  protected:
    const int _pin;
    bool _state;
  public:
    Led(int pin);
    virtual void display(bool state);    
    void display();    
    virtual void toggle();
};

class PWMLed: public Led {
  protected:
    const int _pwmChannel,
              _pwmResolution;
    int _pwmFreq,
        _pwmDutyCycle;        
  public:
    PWMLed(int pin,
           int freq       =PWM_DEFAULT_FREQUENCY,
           int resolution =PWM_DEFAULT_RESOLUTION, 
           int dutyCycle  =PWM_DEFAULT_DUTYCYCLE);
    void setDutyCycle(int dutyCycle);
    void setFreq(int freq);
    virtual void display(bool state);
};

class RGBLed{
  protected:
    static const int _fullOnFreq  =  10000;
   
    uint32_t color;
    Led *_ledVec[3];  //{Red,Green,Blue};
    
  public:
   static const uint32_t _red    = 0xFF0000,
                         _green  = 0xFF00,
                         _blue   = 0xFF;
    RGBLed(const int *pin,uint32_t color);  // 0xRRGGBB
    virtual void display(bool state);
    void setColor(uint32_t color);
};
/*
class RGBPWMLed:public RGBLed{
  protected:
    int _freqVec[3];
    
  public:
    RGBPWMLed( const int *pin,
               uint32_t color);
    void setFreq(int freq);
    void setFreq(int *freqVec);
};
*/
#endif
