#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include <AccelStepper.h>
#include "stripper.h"
#include "espMgr.h"
#include "potMgr.h"
#include "led.h"
#include "buttonLed.h"
#include "config.h"


class ESPMgr;
class Robot;

class App{
  protected:
    static const int _msgLength = 2,
                     _nbMotors  = 2;
    ESPMgr *espMgr;
  public:
    App(const uint8_t *peer, Robot *robotPtr = NULL);
    virtual void loop() = 0;
};

class Robot: public App{
  /*
   * This Robot model is based on 2 byte messages:
    ** Direction Byte : 4 bits indicating which motor and which direction to turn it
   *                  : MSBF :  RF RR LF LR
   ** Speed Byte :  [0,255] gives the 8 bit resolution of the speed to set the motors at,
   */
  protected:
    volatile uint8_t  _incomingVec[_msgLength] = {0,0};

    Stripper *_lStripper,*_rStripper,
             *_stripperVec[2];// = {r,l};

    void _updateStrippers();
    void _setupNeoPixels();
    void _updateIncoming(const uint8_t *incomingData);
    virtual void _runSpeed();
  public:
    Robot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
    void updateIncoming(const uint8_t *incomingData);
};

class StepperRobot : public Robot{
  protected:
    static const int _speedFacotorVec[]; // = {0,1,-1},         

    const int _fastSpeedValue = 600,
              _slowSpeedValue = 400,
              _lStep      = LSTEP,
              _lDir       = LDIR,
              _rStep      = RSTEP,
              _rDir       = RDIR;
    int _speedValue = _slowSpeedValue;
    
    AccelStepper *_leftStepper, //(AccelStepper::DRIVER, _lStep,_lDir), // stepPin, dirPin)
                 *_rightStepper; //(AccelStepper::DRIVER, _rStep,_rDir); // stepPin, dirPin)
    struct mvStruct{
      AccelStepper *motor;
      int          lastSpeed;
    };

    mvStruct *_motorVec; //= {{_rightStepper, 0},    // motor, lastSpeed}
                         //{_leftStepper,0}};
    
    //void _toggleSpeedRange();  // obsoleted 2021 07 26
    void _setSpeed(int index,int speed);
    void _processIncoming();
    void _runSpeed(int i);
    void _runSpeeds();
  
  public:
    StepperRobot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
};

class Controller : public App{
  protected:
    static const int _nbButtonLedCombos =App::_nbMotors;
                     
    const int _butPinVecVec[2][2] = {{RFBUTPIN,RRBUTPIN},
                                     {LFBUTPIN,LRBUTPIN}},
              _ledPinVecVec[2][3] = {{RRLEDPIN,RFLEDPIN,RSLEDPIN},
                                    {LRLEDPIN,LFLEDPIN,LSLEDPIN}},
              _potPin             = POTINPUTPIN,
              _potLedPin          = POTLEDPIN;   
    
                                             
    byte _outgoingVec[App::_msgLength]     = {0,0},  // {direction bits(0BLLRR) where both LL and RR are 0bRF, 
                                                     //  speed byte}
         _lastOutgoingVec[App::_msgLength] = {0xFF,0xFF};
    PotSpeedControl *_psc;
    ButtonLedCombo *_blcVec[_nbButtonLedCombos];
    
    void _send();
    void _updateOutgoing();
    bool _updateNeeded();
    
  public:
    Controller(const uint8_t *peer = ESPMgr::ESP32_C_Address);
    virtual void loop();
    void showOutgoing() const;
};

#endif
