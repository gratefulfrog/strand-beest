#include "potMgr.h"

PotMgr::PotMgr(int pPin):_potPin(pPin){
  pinMode(_potPin,INPUT);
}
byte PotMgr::getPotValue() const{
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  // reduce the resolution to steps of 10 on [0,255] i.e. one byte
  //Serial.println(potValue);
  byte res = map(potValue,
                  0,_adcMaxVal,
                  0, _outputMaxVal);  
  return res;
}
byte  PotSpeedControl::_getFreq(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  return map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
}


PotSpeedControl::PotSpeedControl(int pPin,int lPin):
  PotMgr(pPin),
  _pwmLed(lPin){
  getAndUpdate();
}
byte PotSpeedControl::getFreq() const{
  return _getFreq(getPotValue());
}

void PotSpeedControl::update(){
  byte newFreq = getFreq();
  _pwmLed.setFreq(newFreq);
}

byte PotSpeedControl::getAndUpdate(){
  update();
  return getFreq();
}
  
