#ifndef PWMMGR_H
#define PWMMGR_H
#include <Arduino.h>


#define PWM_DEFAULT_FREQUENCY  (1)
#define PWM_DEFAULT_RESOLUTION (8)
#define PWM_DEFAULT_DUTYCYCLE  (112)  // i.e. 50%

class PWMMgr{
  protected:
    static int       _nextChannel;
    const static int _nbChannels  = 16,
                     _maxOkPin     = 33;
  public:
    static int getNextChannel(int pin);  // return -1 if error
};


#endif
