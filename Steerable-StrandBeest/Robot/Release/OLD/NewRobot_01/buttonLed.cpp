#include "buttonLed.h"

ButtonLedCombo::ButtonLedCombo(const int *buttonPinVec, const int *ledPinVec):
   _rgbLed(ledPinVec,RGBLed::_blue){
  for (int i=0;i<_nbButtons;i++){
    _buttonPinVec[i] = buttonPinVec[i];
    pinMode(_buttonPinVec[i],INPUT_PULLUP);
   }
}
byte ButtonLedCombo::getAndUpdate(){
  byte res = 0; 
  for (int i=0;i<_nbButtons;i++){
    byte val = !digitalRead(_buttonPinVec[i]);   
    res |= (val)<<i;
  }
  _rgbLed.setColor(res == 0 ? RGBLed::_blue 
                             : res == 1 ? RGBLed::_green 
                                        : RGBLed::_red);
  return res;
}
