#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "app.h"
#include "motor.h"
#include "neoPixelMgr.h"
//#include "stripper.h"

class Robot: public App {
  protected:    
    byte _incomingVec[2] = {0,START_SPEED};
    NeoPixelMgr *_pm;
    PixelSegment *_stripperVec[2]; // = {r,l};
    DCMotor *_motorVec[_nbMotors];
    
    void _setupStrippers();
    byte _getMotorDirectionFromIncoming(int index) const;
  public:
    virtual void runSpeed();
    void setDirection();
    void setSpeed();
    void showIncoming() const;
    void updateIncoming(const uint8_t *incomingData);
    virtual void loop();
    Robot();
};

class DCRobot:public Robot {
  protected:
    DCRobot();
};

class DCRobot3Pin:public DCRobot{
  protected:
    const int _pinVec[6] = {ENB,IN3,IN4,
                            ENA,IN1,IN2};
  public: 
    DCRobot3Pin(); 
};

class DCRobot2Pin:public DCRobot{
  protected:
    const int _pinVec[4] = {IN1,IN2,
                            IN3,IN4};      
  public:
    DCRobot2Pin();
};

#endif
