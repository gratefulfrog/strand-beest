#ifndef LED_H
#define LED_H

#include<Arduino.h>
#include "updateable.h"

class Led {
  protected:
    const int _pin;
    bool _state;
  public:
    Led(int pin);
    virtual void display(bool state);    
    void display();    
    virtual void toggle();
};

class BlinkingLed: public Led, public Updateable{
  protected:
    friend class BlinkingRGBLed;
    virtual void _update(long unsigned now);      
  public:
    BlinkingLed(int pin,unsigned long blinkPeriod=9999);
};

class RGBLed{
  protected:
    uint32_t color;
    Led *_ledVec[3];  //{Red,Green,Blue};
 
  public:
    static const uint32_t _red    = 0xFF0000,
                          _green  = 0xFF00,
                          _blue   = 0xFF;
    RGBLed(const int *pin);  // 0xRRGGBB
    RGBLed(const int *pin,uint32_t color);  // 0xRRGGBB
    void setColor(uint32_t color);
    virtual void display(bool state);    
    void display();    
    virtual void toggle();
};

class BlinkingRGBLed:public RGBLed,public Updateable{
  protected:
    BlinkingLed *_ledVec[3];
    int _periodVec[3];
    virtual void _update(long unsigned now);  
    
  public:
    BlinkingRGBLed(const int *pinVec, const int* periodVec);
    BlinkingRGBLed(const int *pinVec, const int period);
    void setPeriod(const int *periodVec);
    void setPeriod(int period);
};

#endif
