#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include "espMgr.h"
#include "config.h"

class Robot;

class App{
  protected:
    static const int _msgLength = 2,
                     _nbMotors  = 2;
    ESPMgr *espMgr;
  public:
    App(const uint8_t *peer, Robot *robotPtr = NULL);
    virtual void loop() = 0;
};

#endif
