#include "robot.h"

Robot::Robot():App(CONTROLLER_PEER,this){
  _setupStrippers();
}

void Robot::loop(){
  setSpeed();
  setDirection();
  runSpeed();
}

void Robot::_setupStrippers(){
  int lightIndexArray[2][2] = {{RR_PIXEL_INDEX,RF_PIXEL_INDEX},
                               {LR_PIXEL_INDEX,LF_PIXEL_INDEX}};
  
  _pm = new NeoPixelMgr(NEOPIXELPIN);
  for (int i=0;i<_nbMotors;i++){
    _stripperVec[i] = new PixelSegment(*_pm, (i*8)+1,STRIPPER_NB_PIXELS,i);
    _stripperVec[i] ->setDirection(_getMotorDirectionFromIncoming(i));
    for (int j=0;j<2;j++){
      _lightArray[i][j] =  new PixelLight(*_pm, lightIndexArray[i][j]);
    }
  }
}

byte Robot::_getMotorDirectionFromIncoming(int index) const{
  // returns 0, 1 or 2
  return _incomingVec[0]>>(2*index)&0b11;
}

int Robot::_getHeadlightDir(int dir, int headlightI, int headlightJ) const{
  if (dir){
    return headlightI^headlightJ ? dir : -dir;
  }
  else{
    return 0;
  }
}

void Robot::setDirection(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    byte d = _getMotorDirectionFromIncoming(i);
    _motorVec[i]->setDirection(d);
    int pixelDirection = (d==0 ? 0            // stopped
                               : d== 1 ?  1   // forward
                                       : -1); // reverse
    _stripperVec[i]->setDirection(pixelDirection);
    for (int j=0;j<2;j++){
       _lightArray[i][j]->setDirection(_getHeadlightDir(pixelDirection,i,j)); // rear hedlight is opposite of front
    }
  }
}

void Robot::setSpeed(){
   for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->setSpeed(_incomingVec[1]);
    _stripperVec[i]->setSpeed(_incomingVec[1]);
  }
}

void Robot::runSpeed(){
  Updateable::updateAll();
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->runSpeed();
  }
}

void Robot::showIncoming() const{
  for (int i=_nbMotors*2-1;i>-1;i--){// 2 bits per motor
    Serial.print(_incomingVec[0]>>i&1);  
  }
  Serial.print(" : ");
  Serial.println(_incomingVec[1],DEC);
}

void Robot::updateIncoming(const uint8_t *incomingData){
  for (int i = 0; i< _msgLength;i++){
    _incomingVec[i] = incomingData[i];
  }
  showIncoming();
}

DCRobot::DCRobot():Robot(){
}

DCRobot3Pin::DCRobot3Pin():DCRobot(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor3Pin(_pinVec[0+i*DCMotor3Pin::nbPins],
                                    _pinVec[1+i*DCMotor3Pin::nbPins],
                                    _pinVec[2+i*DCMotor3Pin::nbPins]);
  }
}

DCRobot2Pin::DCRobot2Pin():DCRobot(){
 for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor2Pin(_pinVec[0+i*DCMotor2Pin::nbPins],
                                    _pinVec[1+i*DCMotor2Pin::nbPins]);
  }
}
