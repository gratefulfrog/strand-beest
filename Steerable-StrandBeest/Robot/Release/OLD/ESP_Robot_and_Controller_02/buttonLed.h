#ifndef BUTTONLED_H
#define BUTTONLED_H

#include "led.h"

class ButtonLedCombo{
  protected:
    static const int _nbButtons = 2;
    RGBLed _rgbLed;
    int _buttonPinVec[_nbButtons]; // {greenbutton, redbutton}

  public:
    ButtonLedCombo(const int *buttonPinVec, const int *ledPinVec);
    byte getAndUpdate();
};

#endif
