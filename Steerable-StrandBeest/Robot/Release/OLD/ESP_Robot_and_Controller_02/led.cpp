#include "led.h"

/// LED CLASS methods ////
Led::Led(int pin): _pin(pin){
  _state = false;
  pinMode(_pin, OUTPUT);
  display();
}
void Led::display(bool state){
  _state = state;
  digitalWrite(_pin,_state);
}
void Led::display(){
  display(_state);
}
void Led::toggle(){
  display(! _state);
}

/// blinking LED CLASS methods ////

BlinkingLed::BlinkingLed(int pin,unsigned long updateDelay):
  Led(pin),
  Updateable(updateDelay){
}

void BlinkingLed::_update(long unsigned now){
  toggle();
}

/// RGB LED CLASS methods ////
RGBLed::RGBLed(const int *pin){ // 0xBBGGRR
  for (int i=0;i<3;i++){
     _ledVec[i] =  new Led(pin[i]);
  }
}
RGBLed::RGBLed(const int *pin,uint32_t color):RGBLed(pin){
  setColor(color);
}
void RGBLed::setColor(uint32_t color){
  for (int i=0;i<3;i++){
    _ledVec[i]->display((color>>(16-8*i) & 0xFF));
  }
}

void RGBLed::display(bool state){
  for (int i=0;i<3;i++){
    _ledVec[i]->display(state);
  }
}  

void RGBLed::display(){
  for (int i=0;i<3;i++){
    _ledVec[i]->display();
  }
}
void RGBLed::toggle(){
  for (int i=0;i<3;i++){
    _ledVec[i]->toggle();
  }
}

BlinkingRGBLed::BlinkingRGBLed(const int *pinVec, const int* periodVec):RGBLed(pinVec),Updateable(){
  setPeriod(periodVec);
}
BlinkingRGBLed::BlinkingRGBLed(const int *pinVec, const int period):RGBLed(pinVec),Updateable(){
  setPeriod(period);
}
void BlinkingRGBLed::setPeriod(const int *periodVec){
  for (int i=0;i<3;i++){
    _ledVec[i]->setPeriod(periodVec[i]);
  }
}
void BlinkingRGBLed::setPeriod(int period){
  for (int i=0;i<3;i++){
    _ledVec[i]->setPeriod(period);
  }
}
void BlinkingRGBLed::_update(long unsigned now){
   for (int i=0;i<3;i++){
    _ledVec[i]->_update(now);
  }
}
