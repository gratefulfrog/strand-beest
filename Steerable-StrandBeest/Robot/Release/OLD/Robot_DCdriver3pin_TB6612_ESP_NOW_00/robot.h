#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "motor.h"
#include "stripper.h"
#include "espMgr.h"

#define STRIP_PINL   (5)
#define STRIP_PINR   (17)
#define START_SPEED  (100)  // just so the strips will blink on startup before connection with peer

class DCRobot{
  protected:
    static const int _nbMotors  = 2,
                     _msgLength = 2;
    DCMotor *_motorVec[_nbMotors];
    byte _incomingVec[2] = {0,START_SPEED};
    ESPMgr *_espMgr;
    
    virtual void _unused() = 0;  // making this an abstract class!
    const int _stripperPinVec[2] = {STRIP_PINR,STRIP_PINL};
    Stripper *_stripperVec[2];// = {r,l};

    void _updateStrippers();
    void _setupStrippers();
    byte _getMotorDirectionFromIncoming(int index) const;
    
  public:
    DCRobot();
    void runSpeed();
    void setDirection();
    void setSpeed();
    void showIncoming() const;
    void updateIncoming(const uint8_t *incomingData);
};

class DCRobot3Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot3Pin(const int *pinVec);
};

class DCRobot2Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot2Pin(const int *pinVec);
};

#endif
