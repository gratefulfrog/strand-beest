#ifndef STRIPPER_H
#define STRIPPER_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

//#define DEBUG

class Stripper{
  protected:
    const static uint32_t _red   = 0xFF0000,
                          _green = 0x00FF00,
                          _blue  = 0x0000FF,
                          _colorVec[]; //={blue,green,red};
    const static int _defaultBrightness = 30,
                     _cycleNbOn         =  3,
                     _flashNbOn         =  2,
                     _defaultNbPixels   =  8,
                     _speedRangeFactor  =  2;
    const static unsigned long _defaultUpdateDelay = 50,
                               _maxDelay           =  10000;
  
    void _cycle();
    void _flash();

    unsigned long _updateDelay,
                  _lastUpdateTime = 0;

    const int     _nbPixels;

    int _direction  = 0,
        _brightness = _defaultBrightness,
        _startPix   = 0;

    Adafruit_NeoPixel *strip;
    
  public:
    Stripper(int pin, 
             int nbPix               = _defaultNbPixels,
             unsigned long updateDel = _defaultUpdateDelay,
             int           bright    = _defaultBrightness);
    void setDirection(int dir);  // 0 stopped, 1 forward, 2 reverse
    void toggleCadence();
    void updateCadence(byte newSpeed);
    void update(bool force = false);  
};


#endif
