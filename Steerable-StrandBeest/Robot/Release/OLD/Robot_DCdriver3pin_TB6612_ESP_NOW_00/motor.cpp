 #include "motor.h"
 
 // abstract motor class
 void DCMotor::setDirection(byte dir){
  //_direction = ((dir > 0) ? 1 : ((dir < 0) ? -1 : 0));
  _direction = ((dir  == 2) ? -1 : dir);
}

void DCMotor::runSpeed() const{
  _runSpeed();
}

void DCMotor::setSpeed(byte newSpeed){
  _speedValue = newSpeed;
}
DCMotor::DCMotor(int iPin0, int iPin1):
  _pinVec({iPin0,iPin1}){
}

// DC Motor concrete class

int DCMotor::_nextIndex = 0;
int DCMotor::_getIndex(){
  return DCMotor::_nextIndex++;
}

void DCMotor3Pin::_runSpeed() const{
  switch (_direction){
    case -1: // backward
      digitalWrite(_pinVec[1], HIGH); 
      digitalWrite(_pinVec[0], LOW);
      break;
    case 1:  // forward
      digitalWrite(_pinVec[0], HIGH); 
      digitalWrite(_pinVec[1], LOW); 
      break;
    default: //case 0: turn off
      digitalWrite(_pinVec[0],LOW);
      digitalWrite(_pinVec[1],LOW);
      break;
  }
  ledcWrite(_pwmIndex, _speedValue);
}

DCMotor3Pin::DCMotor3Pin(int ePin, int i1Pin, int i2Pin):
  DCMotor(i1Pin,i2Pin),
  _enablePin(ePin),
  _pwmIndex(DCMotor::_getIndex()){
    _speedValue = _slowSpeedValue;
    //Serial.println("Enable pin: " + String(_enablePin));
    //Serial.println("_pinVec[0]: " + String(_pinVec[0]));
    //Serial.println("_pinVec[1: " + String(_pinVec[1]));
    pinMode(_enablePin, OUTPUT);
    pinMode(_pinVec[0], OUTPUT);
    pinMode(_pinVec[1], OUTPUT);
    
    ledcSetup(_pwmIndex, _pwmFrequency, _pwmResolution);
    ledcAttachPin(_enablePin, _pwmIndex);
}


void DCMotor2Pin::_runSpeed() const{
  int index = (_direction>0 ? 0 : (_direction < 0 ?  1 : -1));
  for (int i=0;i<_nbPWMChannels;i++){
    ledcWrite(_pwmIndex[i],(i==index ? _speedValue : 0));
  }
}

DCMotor2Pin::DCMotor2Pin(int i1Pin, int i2Pin):
  DCMotor(i1Pin,i2Pin),
  _pwmIndex({DCMotor::_getIndex(),DCMotor::_getIndex()}){
    _speedValue = _slowSpeedValue;
    //Serial.println("Creating DCL9110 motor on pins: " + String(i1Pin) + "," + String(i2Pin));
    pinMode(_pinVec[0], OUTPUT);
    pinMode(_pinVec[1], OUTPUT);
    for (int i =0;i<_nbPWMChannels;i++){
      //Serial.println("setting up PWM: " + String(_pwmIndex[i]));
      ledcSetup(_pwmIndex[i], _pwmFrequency, _pwmResolution);
      ledcAttachPin(_pinVec[i], _pwmIndex[i]);
    }
}
