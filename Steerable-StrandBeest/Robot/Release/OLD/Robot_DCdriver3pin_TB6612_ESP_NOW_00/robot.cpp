#include "robot.h"


DCRobot::DCRobot(){
  _setupStrippers();
  _espMgr = new ESPMgr(ESPMgr::ESP32_D_Address,this);
}
void DCRobot::_setupStrippers(){
  for (int i=0;i<_nbMotors;i++){
    _stripperVec[i] = new Stripper(_stripperPinVec[i]);
    _stripperVec[i] ->setDirection(_getMotorDirectionFromIncoming(i));
  }
}

byte DCRobot::_getMotorDirectionFromIncoming(int index) const{
  // returns 0, 1 or 2
  return _incomingVec[0]>>(2*index)&0b11;
}

void DCRobot::setDirection(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    byte d = _getMotorDirectionFromIncoming(i);
    _motorVec[i]->setDirection(d);
    _stripperVec[i]->setDirection(d);
  }
}

void DCRobot::setSpeed(){
   for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->setSpeed(_incomingVec[1]);
    _stripperVec[i]->updateCadence(_incomingVec[1]);
  }
}

void DCRobot::runSpeed(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i]->runSpeed();
    _stripperVec[i]->update();
  }
}

void DCRobot::_updateStrippers(){
  for (int i = 0; i< _nbMotors;i++){
    _stripperVec[i]->update();
  }
}

void DCRobot::showIncoming() const{
  for (int i=_nbMotors*2-1;i>-1;i--){// 2 bits per motor
    Serial.print(_incomingVec[0]>>i&1);  
  }
  Serial.print(" : ");
  Serial.println(_incomingVec[1],DEC);
}


void DCRobot::updateIncoming(const uint8_t *incomingData){
  for (int i = 0; i< _msgLength;i++){
    _incomingVec[i] = incomingData[i];
  }
  showIncoming();
}


DCRobot3Pin::DCRobot3Pin(const int *pinVec):DCRobot(){
  for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor3Pin(pinVec[0+i*DCMotor3Pin::nbPins],
                                     pinVec[1+i*DCMotor3Pin::nbPins],
                                     pinVec[2+i*DCMotor3Pin::nbPins]);
  }
}

DCRobot2Pin::DCRobot2Pin(const int *pinVec):DCRobot(){
 for (int i=0;i<DCRobot::_nbMotors;i++){
    _motorVec[i] =  new DCMotor2Pin(pinVec[0+i*DCMotor2Pin::nbPins],
                                     pinVec[1+i*DCMotor2Pin::nbPins]);
  }
}
