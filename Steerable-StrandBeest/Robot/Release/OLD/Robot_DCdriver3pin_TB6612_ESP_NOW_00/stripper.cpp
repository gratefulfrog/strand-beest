#include "stripper.h"

const uint32_t Stripper::_colorVec[] ={Stripper::_blue,
                                      Stripper::_green,
                                      Stripper::_red};
  
void Stripper::_cycle(){
  int count = 0,
      i     = _startPix,
      dir   = _direction&1;
  strip->clear(); 
  while (count++<_nbPixels){
   if (i == _startPix){
      for (int j=0;j<_cycleNbOn;j++){
        strip->setPixelColor(i, _colorVec[_direction]);
        i = dir ? (i+1)%_nbPixels : (i == 0 ? 7 : i-1); 
        count++;
      }
    }
    strip->setPixelColor(i,0);
    i = dir ? (i+1)%_nbPixels : (i == 0 ? 7 : i-1);
  }
  strip->show();
  _startPix = i = dir ? (i+1)%_nbPixels : (i == 0 ? 7 : i-1);
}
  
void Stripper::_flash(){
//void theaterChaseLoop(int nbOn, uint32_t color,Adafruit_NeoPixel &strip,int &b,int bright=10) {
  strip->clear();     
  // 'c' counts up from 'b' to end of strip in steps of nbOn...
  for(int c=_startPix; c<_nbPixels; c += _flashNbOn) {
    strip->setPixelColor(c, _blue); 
  }
  strip->show(); // Update strip with new contents
  _startPix=(_startPix+1)%_flashNbOn; 
}

    
Stripper::Stripper(int pin, int nbP, unsigned long updateDel, int bright): 
      _nbPixels(nbP),
      _updateDelay(updateDel),
      _lastUpdateTime(millis()){
  strip = new Adafruit_NeoPixel(_nbPixels, pin, NEO_GRB + NEO_KHZ800);
  strip->begin();
  strip->setBrightness(_brightness);
  strip->clear();
  strip->show();
  setDirection(0);
}

void Stripper::setDirection(int dir){
  //int temp = dir < 0  ? 1 : (dir > 0 ? 2 : 0 );
  if (dir != _direction){
    _direction= dir;
    update(true);
  }  
}

void Stripper::toggleCadence(){
  _updateDelay = ((_updateDelay == _defaultUpdateDelay) ? _defaultUpdateDelay/_speedRangeFactor 
                                                       : _defaultUpdateDelay);
}

void Stripper::updateCadence(byte newSpeed){
  _updateDelay = (newSpeed == 0 ? _maxDelay : (unsigned long) (_defaultUpdateDelay * 255/(newSpeed)));
  //Serial.println("Stripper set cadence: " + String(newSpeed));
}

void Stripper::update(bool force){
  long unsigned now = millis();
  if (force || (now-_lastUpdateTime > _updateDelay)){
    _lastUpdateTime = now;
    if (_direction){
      //Serial.println("Striper cycling");
      _cycle();
    }
    else{
      //Serial.println("Striper flashing");
      _flash();
    }
  }
}
