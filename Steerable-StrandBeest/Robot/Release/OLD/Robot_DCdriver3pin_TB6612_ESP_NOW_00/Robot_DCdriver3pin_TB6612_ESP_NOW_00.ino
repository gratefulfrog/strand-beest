#include "robot.h"

#define ROBOT_3Pin  // this works with both the L298N and TB6612 drivers
// if not defined, then it works with the L9110 driver

const int enA = 33;  
const int in1 = 25;  
const int in2 = 26;  
const int in3 = 27;  
const int in4 = 14; 
const int enB = 12;  
const int pinVec3Pin[] = {enA,in1,in2,
                           enB,in3,in4};
const int pinVec2Pin[] = {in1,in2,
                          in4,in3};                           
DCRobot *robot;

void setup(){
  Serial.begin(115200);
  #ifdef ROBOT_3Pin
    Serial.println("Using 3Pin Robot...");
    robot = new DCRobot3Pin(pinVec3Pin);
  #else
    Serial.println("Using 2Pin Robot...");
    robot = new DCRobot2Pin(pinVec2Pin);
  #endif
}

void loop(){
  robot->setSpeed();
  robot->setDirection();
  robot->runSpeed();
}
