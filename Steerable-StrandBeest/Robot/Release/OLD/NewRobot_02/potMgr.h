#ifndef POTMGR_H
#define POTMGR_H

#include <Arduino.h>
#include "led.h"

class PotMgr{
  protected:
    static const int _adcResolutionBits       = 12,
                     _adcMaxVal               = pow(2,_adcResolutionBits)-1,
                     _outputResolutionBits    = 8,
                     _outputMaxVal            = pow(2,_outputResolutionBits)-1,
                     _outputResolutionDecimal = 10,
                     _nbValues                = 10;
                     
    const int _potPin;
    int _valueVec[_nbValues],
        _valueVecIndex = 0;

  public:
    PotMgr(int potPin);
    byte getPotValue();
};

class PotSpeedControl: public PotMgr{
  protected:
    static const int _maxfreq      = 25,
                     _minFreq      = 1;

    BlinkingLed _blinkingLed;
    byte  _getFreq(int potVal) const;
    unsigned long _getPeriod(int potVal) const;
  public:
    PotSpeedControl(int potPin, int ledPin);
    byte getFreq();
    unsigned long getPeriod();
    void update();
    byte getFreqAndUpdate();
    byte getPotValueAndUpdate();
};


#endif
