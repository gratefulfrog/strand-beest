#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

///////////////////////////////////////
////////   Robot Pin defs  ////////////
///////////////////////////////////////

// NeoPixels Pins
#define STRIP_PINL (26)
#define STRIP_PINR (25)

// Motor Pins
#define LSTEP      (27)
#define LDIR       (14)
#define RSTEP      (12)
#define RDIR       (13)

///////////////////////////////////////
//////  END Robot Pin defs ////////////
///////////////////////////////////////

///////////////////////////////////////
//////  Controller Pin defs  //////////
///////////////////////////////////////

// motor direction indicator LEDS: Red Green Blue
#define RRLEDPIN (33)  // RED   right reverse led
#define RFLEDPIN (32)  // GREEN right forward led
#define RSLEDPIN (23)  // BLUE  right stop    led

#define LRLEDPIN (14)  // RED   left reverse led
#define LFLEDPIN (27)  // GREEN left forward led
#define LSLEDPIN (22)  // BLUE  left stop    led

// button input pins: Forward Reverse, ie Green Red
#define LFBUTPIN (13)  // GREEN left forward button
#define LRBUTPIN (12)  // RED   left reverse button

#define RFBUTPIN (26)  // GREEN right forward button
#define RRBUTPIN (25)  // RED   right reverse button

// POT Pins: analog input and led indicator
#define POTINPUTPIN (34)
#define POTLEDPIN   (5)

#endif
