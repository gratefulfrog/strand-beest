#include "potMgr.h"

PotMgr::PotMgr(int pPin):_potPin(pPin){
  pinMode(_potPin,INPUT);
   for (int i=0; i<_nbValues;i++){
    _valueVec[i] = analogRead(_potPin);                            
  }
}
byte PotMgr::getPotValue() {
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  _valueVec[_valueVecIndex] = map(potValue,
                                  0,_adcMaxVal,
                                  0, _outputMaxVal);  
                                  
  _valueVecIndex = (_valueVecIndex+1)%_nbValues;
  int res = 0;
  for (int i=0; i<_nbValues;i++){
    res +=  _valueVec[i];                              
  }
  return res/10;
}
byte  PotSpeedControl::_getFreq(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  return map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
}

unsigned long PotSpeedControl::_getPeriod(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  int freq = map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
  return 1000/freq;
}


PotSpeedControl::PotSpeedControl(int pPin,int lPin):
  PotMgr(pPin),
  _blinkingLed(lPin){
  update();
}
byte PotSpeedControl::getFreq(){
  return _getFreq(getPotValue());
}

unsigned long  PotSpeedControl::getPeriod(){
  return _getPeriod(getPotValue());
}

void PotSpeedControl::update(){
  _blinkingLed.setPeriod(getPeriod());
}

byte PotSpeedControl::getFreqAndUpdate(){
  update();
  return getFreq();
}

byte PotSpeedControl::getPotValueAndUpdate(){
  update();
  return getPotValue();
}

  
