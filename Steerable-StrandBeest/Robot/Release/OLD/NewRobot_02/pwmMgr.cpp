#include "pwmMgr.h"

int PWMMgr::_nextChannel=0;

int PWMMgr::getNextChannel(int pin){  // return -1 if error
  int badRes = -1;
  if (_nextChannel<_nbChannels &&
      pin <= _maxOkPin){
        return _nextChannel++;
  }
  Serial.println("Couldn't obtain pwm channel for pin: " + String(pin));
  Serial.println("Next channel is : " + String(_nextChannel));
  return badRes;
}
      
