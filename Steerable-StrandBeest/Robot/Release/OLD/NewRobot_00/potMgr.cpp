#include "potMgr.h"

PotMgr::PotMgr(int pPin,int ledPin):_potPin(pPin),_ledPin(ledPin){
  pinMode(_potPin,INPUT);
  pinMode(_ledPin,OUTPUT);
}

byte PotMgr::getNormedValue() const{
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  // reduce the resolution to steps of 10 on [0,255] i.e. one byte
  //Serial.println(potValue);
  byte res = (byte)min(_outputMaxVal,
                       (int)(_outputResolutionDecimal*round(map(potValue,0,_adcMaxVal,0,_outputMaxVal)/_outputResolutionDecimal)));
  //Serial.println("PotMgr returning normed value: " + String(res));
  return res;
}
void PotMgr::update(){
  byte newSpeed = getNormedValue();
  _updateDelay = (newSpeed == 0 ? _maxDelay : _defaultUpdateDelay * _outputMaxVal/newSpeed);

  unsigned long now = millis();
  if (now - _lastUpdateTime > _updateDelay){
    _lastUpdateTime = now;
    digitalWrite(_ledPin, !digitalRead(_ledPin));
  }
}
  
