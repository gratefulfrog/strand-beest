#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include <AccelStepper.h>
#include "stripper.h"
#include "espMgr.h"
#include "potMgr.h"

// Robot Pin defs
// NeoPixels Pins
#define STRIP_PINL (26)
#define STRIP_PINR (25)

// Stepper Pins
#define LSTEP      (27)
#define LDIR       (14)
#define RSTEP      (12)
#define RDIR       (13)

// Controller Pins
// motor direction indicators
#define RFLEDPIN (32)  // right forward led
#define RRLEDPIN (33)  // right reverse led
#define RSLEDPIN (23)  // right stop    led

#define LFLEDPIN (27)
#define LRLEDPIN (14)
#define LSLEDPIN (22)

// speed indicators
//#define FSTLEDPIN   (19)  // 2 speed model is obsoleted 2021 07 26
//#define SLWLEDPIN   (18)
#define SPEEDLEDPIN (5)

// button input pins
#define LFBUTPIN (13)
#define LRBUTPIN (12)

#define RFBUTPIN (26)
#define RRBUTPIN (25)

// POT Input Pin
#define POTPIN   (34)

//// end of defines //////

class ESPMgr;
class Robot;

class App{
  protected:
    static const int _msgLength  = 2;
    ESPMgr *espMgr;
  public:
    App(const uint8_t *peer, Robot *robotPtr = NULL);
    virtual void loop() = 0;
};

class Robot: public App{
  /*
   * This Robot model is based on 2 byte messages:
    ** Direction Byte : 4 bits indicating which motor and which direction to turn it
   *                  : MSBF :  RF RR LF LR
   ** Speed Byte :  [0,255] gives the 8 bit resolution of the speed to set the motors at,
   */
  protected:
    static const int  _nbMotors                = 2; 
        
    volatile uint8_t  _incomingVec[_msgLength] = {0,0};

    Stripper *_lStripper,*_rStripper,
             *_stripperVec[2];// = {r,l};

    void _updateStrippers();
    void _setupNeoPixels();
    void _updateIncoming(const uint8_t *incomingData);
    virtual void _runSpeed();
    void _update
  public:
    Robot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
};

class StepperRobot : public Robot{
  protected:
    static const int _speedFacotorVec[]; // = {0,1,-1},         

    const int _fastSpeedValue = 600,
              _slowSpeedValue = 400,
              _lStep      = LSTEP,
              _lDir       = LDIR,
              _rStep      = RSTEP,
              _rDir       = RDIR;
    int _speedValue = _slowSpeedValue;
    
    AccelStepper *_leftStepper, //(AccelStepper::DRIVER, _lStep,_lDir), // stepPin, dirPin)
                 *_rightStepper; //(AccelStepper::DRIVER, _rStep,_rDir); // stepPin, dirPin)
    struct mvStruct{
      AccelStepper *motor;
      int          lastSpeed;
    };

    mvStruct *_motorVec; //= {{_rightStepper, 0},    // motor, lastSpeed}
                         //{_leftStepper,0}};
    
    //void _toggleSpeedRange();  // obsoleted 2021 07 26
    void _setSpeed(int index,int speed);
    void _processIncoming();
    void _runSpeed(int i);
    void _runSpeeds();
  
  public:
    StepperRobot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
};

class Controller : public App{
  protected:
    static const int _nbLedPins       = 7, // 8,
                     _nbButtonPins    = 4; //5,
                     
    const int _rightForwardLedPin    = RFLEDPIN,
              _rightReverseLedPin    = RRLEDPIN,
              _rightStoppedLedPin    = RSLEDPIN,
              _leftForwardLedPin     = LFLEDPIN,
              _leftReverseLedPin     = LRLEDPIN,
              _leftStoppedLedPin     = LSLEDPIN,
              _speedLedPin           = SPEEDLEDPIN,
              //_fastLedPin            = FSTLEDPIN,
              //_slowLedPin            = SLWLEDPIN,
              _leftForwardButtonPin  = LFBUTPIN,
              _leftReverseButtonPin  = LRBUTPIN,
              _rightForwardButtonPin = RFBUTPIN,
              _rightReverseButtonPin = RRBUTPIN,
              //_speedToggleButtonPin  = STBUTPIN,
              _potPin                = POTPIN,
              *_buttonPinVec[_nbButtonPins] = { &_rightForwardButtonPin,
                                                &_rightReverseButtonPin,
                                                &_leftForwardButtonPin,
                                                &_leftReverseButtonPin},
              *_ledPinVec[_nbLedPins]    = {&_rightForwardLedPin,
                                            &_rightReverseLedPin, 
                                            &_leftForwardLedPin,
                                            &_leftReverseLedPin,
                                            &_rightStoppedLedPin,
                                            &_leftStoppedLedPin,
                                            &_speedLedPin};
                                            //&_fastLedPin,
                                            //&_slowLedPin}; 
    byte _outgoingVec[2]     = {0,0},  // {direction bits, speed byte}
         _lastOutgoingVec[2] = {0xFF,0xFF};
    PotMgr *_potMgr;
    
    void _send();
    void _updateOutgoing();
    bool _outgoingHasChanged();
    
  public:
    Controller(const uint8_t *peer = ESPMgr::ESP32_C_Address);
    virtual void loop();
};

#endif
