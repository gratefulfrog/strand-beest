#include "app.h"

void Controller::_send(){
  espMgr->sendByteVec(_outgoingVec,_msgLength);
}

void Controller::_updateOutgoing(){
  for (int i=0; i< _msgLength;i++){
    _outgoingVec[i] = 0;
  }
  
  for (int i=0;i<_nbButtonPins;i++){
    byte val = !digitalRead(*_buttonPinVec[i]);
    _outgoingVec[0] |= (val )<<i;   
    digitalWrite(*_ledPinVec[i],val);
  }
  _outgoingVec[1] = _potMgr->getNormedValue();
  //Serial.println("outgoingVec[1] = " +String(_outgoingVec[1]));
}

Controller::Controller(const uint8_t *peer): App(peer){
  _potMgr = new PotMgr(_potPin,_speedLedPin);
  for (int i=0;i<_nbButtonPins;i++){
    pinMode(*_buttonPinVec[i],INPUT_PULLUP);
  }
  for (int i=0;i<_nbLedPins;i++){
    pinMode(*_ledPinVec[i],OUTPUT);
  }
}

bool Controller::_outgoingHasChanged(){
  bool res = false;
  for (int i= 0; i < _msgLength;i++){
    if (_lastOutgoingVec[i] != _outgoingVec[i]){
      res = true;
      _lastOutgoingVec[i] =  _outgoingVec[i];
    }
  }
  return res;
}
void Controller::loop(){
  static byte lastOutgoing = 0;
  _updateOutgoing();
  if (_outgoingHasChanged()){
    _send();
  }
  _potMgr->update();
}
