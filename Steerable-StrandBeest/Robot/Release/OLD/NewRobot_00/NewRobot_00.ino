#include "app.h"
#include "potMgr.h"

#define CONTROLLER

App *app;

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("starting up...");
  app = new Controller();  
}

void loop() {
  app->loop();
}
