#ifndef POTMGR_H
#define POTMGR_H

#include <Arduino.h>

class PotMgr{
  protected:
    static const int _adcResolutionBits       = 12,
                     _adcMaxVal               = pow(2,_adcResolutionBits)-1,
                     _outputResolutionBits    = 8,
                     _outputMaxVal            = pow(2,_outputResolutionBits)-1,
                     _outputResolutionDecimal = 10;
                     
    const static unsigned long _defaultUpdateDelay = 25,
                               _maxDelay           =  100000;
    const int _potPin,
              _ledPin;   
    unsigned long _lastUpdateTime = 0,
                  _updateDelay    = _defaultUpdateDelay;
     
  public:
    PotMgr(int potPin,int ledPin);
    byte getNormedValue() const;
    void update();
};

#endif
