#include "espMgr.h"
#include "app.h"

const uint8_t ESPMgr::ESP32_B_Address[6] = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},    // current config robot
              ESPMgr::ESP32_C_Address[6] = {0x24, 0x0A, 0xC4, 0x59, 0xBA, 0x34},    // spare MCU 
              ESPMgr::ESP32_D_Address[6] = {0xAC, 0x67, 0xB2, 0x3D, 0x5F, 0x50},    // current config controller
              ESPMgr::ESP32_E_Address[6] = {0xAC, 0x67, 0xB2, 0x3D, 0x52, 0x30},    // spare MC
              ESPMgr::ESP32_F_Address[6] = {0xAC, 0x67, 0xB2, 0x50, 0xB3, 0xF8},    // spare MCU   
              ESPMgr::ESP32_G_Address[6] = {0xAC, 0x67, 0xB2, 0x50, 0x7C, 0xEC};    // spare MCU   

Robot *ESPMgr::robotPtr; 
ESPMgr *ESPMgr::thisPtr; 

esp_now_peer_info_t peerInfo;

////////////////////// EPS NOW Functions ///////////////
// Callback when data is sent
void ESPMgr::OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {  
  thisPtr->sendStatus = status;
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  //Serial.println(status);
}

// Callback when data is received
void ESPMgr::OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  robotPtr->updateIncoming(incomingData);
}

void  ESPMgr::setupWifi(){
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  
  WiFi.macAddress(_myMac);
  
  Serial.print("My MAC: ");
  printMac(_myMac);
 
  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    while(1);
  } 
}

void ESPMgr::registerAndAddPeer(){  
  peerInfo = new esp_now_peer_info_t();

  memcpy(peerInfo->peer_addr, _peerAddress,6);
  peerInfo->channel = 0;   
  peerInfo->encrypt = false;

  // Add peer        
  if (esp_now_add_peer(peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    while(1);
  }
}

ESPMgr::ESPMgr(const uint8_t *peerAddress, Robot *robotPtr){
  ESPMgr::thisPtr = this;
  if (robotPtr != NULL){
    ESPMgr::robotPtr = robotPtr;
  }
  memcpy(_peerAddress, peerAddress, 6);
  Serial.print("Receiver MAC: ");
  printMac(_peerAddress);

  setupWifi(); 
   
  // register data Send CB
  esp_now_register_send_cb(OnDataSent);

  // register peer
  registerAndAddPeer();
  
  // Register data receive callback
  esp_now_register_recv_cb(OnDataRecv);
  Serial.println("exiting ESPMgr contructor...");
}

void ESPMgr::sendByteVec(byte outgoing[],int msgLen) const{
  /*
  Serial.print("outgoing data: ");
  for (int i=0; i<msgLen;i++){
    Serial.print(outgoing[i],BIN);
    Serial.print(":");
  }
  Serial.println();
  */
  esp_err_t result = esp_now_send(_peerAddress, (uint8_t *) outgoing, msgLen);
  Serial.println((result == ESP_OK) ? "Sent with success" : "Error sending the data"); 
}
