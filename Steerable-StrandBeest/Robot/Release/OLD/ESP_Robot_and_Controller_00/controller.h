#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <Arduino.h>
#include "app.h"
#include "potMgr.h"
#include "buttonLed.h"

class Controller : public App{
  protected:
    static const int _nbButtonLedCombos = App::_nbMotors,
                     _minSpeedChange    = 3;
                     
    const int _butPinVecVec[2][2] = {{RFBUTPIN,RRBUTPIN},
                                     {LFBUTPIN,LRBUTPIN}},
              _ledPinVecVec[2][3] = {{RRLEDPIN,RFLEDPIN,RSLEDPIN},
                                    {LRLEDPIN,LFLEDPIN,LSLEDPIN}},
              _potPin             = POTINPUTPIN,
              _potLedPin          = POTLEDPIN;   
    
                                             
    byte _outgoingVec[App::_msgLength]     = {0,0},  // {direction bits(0BLLRR) where both LL and RR are 0bRF, 
                                                     //  speed byte}
         _lastOutgoingVec[App::_msgLength] = {0xFF,0xFF};
    PotSpeedControl *_psc;
    ButtonLedCombo *_blcVec[_nbButtonLedCombos];
    
    void _send();
    void _updateOutgoing();
    bool _updateNeeded();
    
  public:
    Controller(const uint8_t *peer = ROBOT_PEER);
    virtual void loop();
    void showOutgoing() const;
};
#endif
