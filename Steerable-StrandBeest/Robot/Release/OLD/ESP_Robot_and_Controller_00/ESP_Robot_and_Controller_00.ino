#include "controller.h"
#include "robot.h"

//#define CONTROLLER
#define MOTOR2PIN

App *app;

#ifdef CONTROLLER
void getApp(){
  app = new Controller();
}
#else
  #ifdef MOTOR2PIN
  void getApp(){
    app = new DCRobot2Pin();
  }
  #else
  void getApp(){
    app = new DCRobot3Pin();
  }
  #endif
#endif

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("starting up...");
  getApp();
}

void loop() {
  app->loop();
}
