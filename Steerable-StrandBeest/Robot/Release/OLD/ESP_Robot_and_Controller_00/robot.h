#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "app.h"
#include "motor.h"
#include "stripper.h"

class Robot: public App {
  protected:    
    byte _incomingVec[2] = {0,START_SPEED};
    const int _stripperPinVec[2] = {STRIP_PINR,STRIP_PINL};
    Stripper *_stripperVec[2]; // = {r,l};
    DCMotor *_motorVec[_nbMotors];
    
    void _updateStrippers();
    void _setupStrippers();
    byte _getMotorDirectionFromIncoming(int index) const;
  public:
    virtual void runSpeed();
    void setDirection();
    void setSpeed();
    void showIncoming() const;
    void updateIncoming(const uint8_t *incomingData);
    virtual void loop();
    Robot();
};

class DCRobot:public Robot {
  protected:
    DCRobot();
};

class DCRobot3Pin:public DCRobot{
  protected:
    const int _pinVec[6] = {ENA,IN1,IN2,
                            ENB,IN3,IN4};
  public: 
    DCRobot3Pin(); 
};

class DCRobot2Pin:public DCRobot{
  protected:
    const int _pinVec[4] = {IN1,IN2,
                            IN3,IN4};      
  public:
    DCRobot2Pin();
};

/*
class StepperRobot : public Robot{
  protected:
    static const int _speedFacotorVec[]; // = {0,1,-1},         

    const int _fastSpeedValue = 600,
              _slowSpeedValue = 400,
              _lStep      = LSTEP,
              _lDir       = LDIR,
              _rStep      = RSTEP,
              _rDir       = RDIR;
    int _speedValue = _slowSpeedValue;
    
    AccelStepper *_leftStepper, //(AccelStepper::DRIVER, _lStep,_lDir), // stepPin, dirPin)
                 *_rightStepper; //(AccelStepper::DRIVER, _rStep,_rDir); // stepPin, dirPin)
    struct mvStruct{
      AccelStepper *motor;
      int          lastSpeed;
    };

    mvStruct *_motorVec; //= {{_rightStepper, 0},    // motor, lastSpeed}
                         //{_leftStepper,0}};
    
    //void _toggleSpeedRange();  // obsoleted 2021 07 26
    void _setSpeed(int index,int speed);
    void _processIncoming();
    void _runSpeed(int i);
    void _runSpeeds();
  
  public:
    StepperRobot(const uint8_t *peer = ESPMgr::ESP32_D_Address);
    virtual void loop();
};
*/


#endif
