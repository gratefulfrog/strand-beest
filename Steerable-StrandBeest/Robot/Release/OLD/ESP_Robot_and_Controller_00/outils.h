#ifndef OUTILS_H
#define OUTILS_H

#include <Arduino.h>

extern bool macEquals(const uint8_t macL[], const uint8_t macR[]);
extern void printMac(const uint8_t mac[]);

#endif
