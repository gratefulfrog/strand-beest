#include "neoPixelMgr.h"

NeoPixelMgr::NeoPixelMgr(int pin, int nbPixels, byte brightness){
  NeoPixels = new Adafruit_NeoPixel(nbPixels, pin, NEO_GRB + NEO_KHZ800);
  NeoPixels->begin();
  NeoPixels->show();
  NeoPixels->setBrightness(brightness);
}
  
void  PixelSegment::_flash(PixelSegment *ps){
  for(int c=ps->_lastStartPix+ps->_startPix; c<ps->_startPix+ps->_nbPix; c += _flashNbOn ) {
    ps->_pixels.NeoPixels->setPixelColor(c, NeoPixelMgr::_blue); 
  }
  ps->_lastStartPix=(ps->_lastStartPix+1)%_flashNbOn;
}
void  PixelSegment::_cycle(PixelSegment *ps){  
}
    
PixelSegment::PixelSegment(NeoPixelMgr &pm, 
                           int startPix, 
                           int nbPix, 
                           bool invertDirection):
  _pixels(pm),
  _startPix(startPix),
  _nbPix(nbPix),
  _directionInverter(invertDirection ? -1 : 1),
  _direction(0),
  _lastStartPix(0),
  _updateDelay(_defaultUpdateDelay ){    
}
void PixelSegment::setDirection(int dir){  // -1,0,1 reverse, stop, forward
  _direction = dir*_directionInverter;
}
void PixelSegment::setLastStartPixel(int newPix){
  _lastStartPix = newPix;
}
void PixelSegment::setSpeed(byte newSpeed){ 
  _updateDelay = (newSpeed == 0 ? _maxDelay : (unsigned long) (_defaultUpdateDelay * 255/(newSpeed)));
}

void PixelSegment::update(long unsigned now, bool force){
  if (force || (now-_lastUpdateTime > _updateDelay)){
    _lastUpdateTime = now;
    if (_direction){
      //Serial.println("Striper cycling");
      PixelSegment::_cycle(this);
    }
    else{
      //Serial.println("Striper flashing");
      PixelSegment::_flash(this);
    }
  }
}
