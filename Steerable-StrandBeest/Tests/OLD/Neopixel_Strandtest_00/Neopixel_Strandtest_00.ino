// A basic everyday NeoPixel strip test program.

// NEOPIXEL BEST PRACTICES for most reliable operation:
// - Add 1000 uF CAPACITOR between NeoPixel strip's + and - connections.
// - MINIMIZE WIRING LENGTH between microcontroller board and first pixel.
// - NeoPixel strip's DATA-IN should pass through a 300-500 OHM RESISTOR.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel strip,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.
// (Skipping these may work OK on your workbench but can fail in the field)

#include <Adafruit_NeoPixel.h>

#define LED_PINX    (32)
//#define LED_PINY    (33)
#define LED_COUNT   (16) 

// Declare our NeoPixel strip object:
Adafruit_NeoPixel stripX(LED_COUNT, LED_PINX, NEO_GRB + NEO_KHZ800);
//Adafruit_NeoPixel stripY(LED_COUNT, LED_PINY, NEO_GRB + NEO_KHZ800);

//Adafruit_NeoPixel *stripVec[] = {&stripX,&stripY};

//const int nbStrips = 1;

// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)


// setup() function -- runs once at startup --------------------------------


const int lPause =50,
          brightness = 10;
          
void setup() {
  stripX.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  stripX.show();            // Turn OFF all pixels ASAP
  stripX.setBrightness(brightness); // 

  Serial.begin(115200);
  while(!Serial);  
  Serial.println("Starting up!");
  delay(500);
}
void theaterChaseLoop(int nbOn, 
                      uint32_t color,
                      Adafruit_NeoPixel &strip,
                      int &lastStart,
                      int startPix,
                      int nbPix) {
  // 'c' counts up from 'b' to end of strip in steps of nbOn...
  for(int c=lastStart+startPix; c<startPix+nbPix; c += nbOn) {
    strip.setPixelColor(c, color); 
  }
  lastStart=(lastStart+1)%nbOn;
}

void runRunner(int dir,  // +1  forward; -1 reverse
               int nbOn, 
               uint32_t color,
               int &nextStart,
               int startPix,
               int nbPix,
               Adafruit_NeoPixel &strip = stripX) {
                
  nextStart = (nextStart <= startPix ? startPix+nbPix 
                                    : nextStart);

  for (int count=0;count<nbOn; count++){
    int onPix = (nextStart+count)%(startPix+nbPix);
    onPix = onPix<startPix ? startPix+onPix : onPix;
    strip.setPixelColor(onPix, color);
    Serial.println("turning on: " + String(onPix));
    }
  nextStart = (nextStart+dir)%(nbPix + startPix);
 
}

void loop(){
  const static uint32_t cVec[] = {0xFF0000,0x00FF00};
  const static int dirCount    = 100;
  static       int siVec[]  = {0,6},
                   aa          = 0,
                   bb          = 0,
                   cc          = 0,
                   dd          = 0,
                   ee          = 0,
                   dir         = 0;
  static unsigned long lastUpdate = millis(),
                       last2pdate = millis(),
                       dirChanger = 0;

  unsigned long now = millis();
  if (now-lastUpdate>lPause){
    lastUpdate = now;
    //dir = dirChanger++%dirCount ? dir : dir^1;
    stripX.clear();
    runRunner(1,2,0x00FF00,aa,0,5);
    runRunner(-1,2,0xFF0000,bb,11,5);
   //runRunner(2,0xFF0000,bb,8,6);
    //runRunner(!dir,3,cVec[dir],stripX,siVec[1]);
    theaterChaseLoop(2,0x0000FF,stripX,cc,5,6);
    //theaterChaseLoop(2,0x00FF00,stripX,dd,2,5);
    //theaterChaseLoop(2,0xFF0000,stripX,ee,0,1);
    stripX.show();
    //delay(500);
  }
}
/*
void loop() {
  while(1){
    for (int i=0;i<nbStrips;i++){
      // Fill along the length of the strip in various colors...
      stripVec[i]->setBrightness(brightness);
      colorWipe(stripVec[i]->Color(255,   0,   0), lPause,*stripVec[i]); // Red
      colorWipe(stripVec[i]->Color(  0, 255,   0), lPause,*stripVec[i]); // Green
      colorWipe(stripVec[i]->Color(  0,   0, 255), lPause,*stripVec[i]); // Blue
    }
    for (int i=0;i<nbStrips;i++){
      // Do a theater marquee effect in various colors...
      stripVec[i]->setBrightness(brightness);
      theaterChase(stripVec[i]->Color(127, 127, 127), lPause,*stripVec[i]); // White, half brightness
      theaterChase(stripVec[i]->Color(127,   0,   0), lPause,*stripVec[i]); // Red, half brightness
      theaterChase(stripVec[i]->Color(  0,   0, 127), lPause,*stripVec[i]); // Blue, half brightness
    }
    for (int i=0;i<nbStrips;i++){
      stripVec[i]->setBrightness(brightness);
      rainbow(lPause/5,*stripVec[i]);             // Flowing rainbow cycle along the whole strip
      theaterChaseRainbow(lPause,*stripVec[i]); // Rainbow-enhanced theaterChase variant
    }
  }
}
*/
// Some functions of our own for creating animated effects -----------------

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait,Adafruit_NeoPixel &strip ) {
  for (int j=0;j<10;j++){
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
      delay(wait);                           //  Pause for a moment
    }
    strip.clear();
  }
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la strip.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void theaterChase(uint32_t color, int wait,Adafruit_NeoPixel &strip) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(int wait,Adafruit_NeoPixel &strip) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void theaterChaseRainbow(int wait,Adafruit_NeoPixel &strip) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  //for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (strip.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show();                // Update strip with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  //}
}
