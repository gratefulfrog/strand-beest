// Potentiometer is connected to GPIO 34 (Analog ADC1_CH6) 
const int potPin = 14;

// variable for storing the potentiometer value
int potValue = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("Starting up...");
  pinMode(potPin,INPUT);
}

void loop() {
  // Reading potentiometer value
  potValue = analogRead(potPin);
  Serial.println(map(potValue,0,4095,0,255));
  //delay(1000);
}
