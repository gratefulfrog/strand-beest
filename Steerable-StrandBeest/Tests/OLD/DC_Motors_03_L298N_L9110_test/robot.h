#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "motor.h"

class DCRobot{
  protected:
    static const int nbMotors = 2;
    DCMotor2Speed *_motorVec[nbMotors];
    virtual void _unused() = 0;
  public:
    //DCRobot(int *pinVec);
    void setDirection(int dir);
    void toggleSpeedRange();
    void runSpeed();
};

class DCRobot3Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot3Pin(const int *pinVec);
};

class DCRobot2Pin:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobot2Pin(const int *pinVec);
};

#endif
