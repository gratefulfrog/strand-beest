#include "robot.h"

void DCRobot::setDirection(int dir){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->setDirection(dir);
  }
}
void DCRobot::toggleSpeedRange(){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->toggleSpeedRange();
  }
}
void DCRobot::runSpeed(){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->runSpeed();
  }
}

DCRobot3Pin::DCRobot3Pin(const int *pinVec){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i] =  new DCMotor3Pin(pinVec[0+i*DCMotor3Pin::nbPins],
                                     pinVec[1+i*DCMotor3Pin::nbPins],
                                     pinVec[2+i*DCMotor3Pin::nbPins]);
  }
}

DCRobot2Pin::DCRobot2Pin(const int *pinVec){
 for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i] =  new DCMotor2Pin(pinVec[0+i*DCMotor2Pin::nbPins],
                                     pinVec[1+i*DCMotor2Pin::nbPins]);
  }
}
