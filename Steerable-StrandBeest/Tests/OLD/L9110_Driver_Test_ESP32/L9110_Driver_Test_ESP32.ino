const int BIA           = 25,
          BIB           = 26,
          pwmIndex0     =  0,
          pwmIndex1     =  1,
          pwmFrequency  = 10, 
          pwmResolution =  15; //Resolution 8, 10, 12, 15


int speed = pow(2,pwmResolution)-1;
void setup() { 
  Serial.begin(115200);
  Serial.println("starting up");
  pinMode(BIA, OUTPUT);   
  pinMode(BIB, OUTPUT);
  
  ledcSetup(pwmIndex0, pwmFrequency, pwmResolution);
  ledcSetup(pwmIndex1, pwmFrequency, pwmResolution);
  ledcAttachPin(BIA, pwmIndex0); 
  ledcAttachPin(BIB, pwmIndex1); 
}

void loop() {   
  static bool goForward = true;
  Serial.println(String(goForward  ? "Forward: "  : "Backward: ") + String(speed));
  if (goForward ){
    forward();    
  }
  else{
    backward();
  }
  //delay(5);   
  goForward = (speed == 0) ? !goForward  : goForward;
  speed = (speed == 0) ? pow(2,pwmResolution)-1 : speed-1; //255; //(speed +16)%256;  
}
  
void backward() { 
  digitalWrite(BIA, 0); 
  ledcWrite(pwmIndex1,speed); 
} 
  
void forward() { 
  digitalWrite(BIB, 0);
  ledcWrite(pwmIndex0,speed); 
}
