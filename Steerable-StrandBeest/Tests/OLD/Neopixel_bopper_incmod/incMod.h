#ifndef INCMOD_H
#define INCMOD_H

#include <Arduino.h>

class IncMod{
  protected:
    const int _mod;
  public:
    IncMod(int md);
    void incMod(int &i) const;
};
#endif
