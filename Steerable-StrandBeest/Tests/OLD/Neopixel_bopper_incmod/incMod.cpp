#include "incMod.h"

IncMod::IncMod(int md):_mod(md){};

void IncMod::incMod(int &i) const{
  i = (i+1)%_mod;
}
