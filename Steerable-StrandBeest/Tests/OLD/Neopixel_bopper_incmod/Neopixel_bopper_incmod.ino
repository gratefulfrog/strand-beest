// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// Released under the GPLv3 license to match the rest of the
// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#include "incMod.h"

#define PIN        (26)
#define NUMPIXELS  (20) 
#define PAUSE     (100)

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

IncMod im = IncMod(NUMPIXELS);
int i=0;

void setup() {
  pixels.begin();
  pixels.setBrightness(10);
  pixels.clear();
}

void loop() {
  pixels.setPixelColor(i, 0);
  im.incMod(i);
  pixels.setPixelColor(i, 0x00FF00);
  pixels.show();
  delay(PAUSE);
}
