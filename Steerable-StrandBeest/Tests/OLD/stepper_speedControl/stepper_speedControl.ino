
/*
 Stepper Motor Control - speed control

 This program drives a unipolar or bipolar stepper motor.
 The motor is attached to digital pins 8 - 11 of the Arduino.
 A potentiometer is connected to analog input 0.

 The motor will rotate in a clockwise direction. The higher the potentiometer value,
 the faster the motor speed. Because setSpeed() sets the delay between steps,
 you may notice the motor is less responsive to changes in the sensor value at
 low speeds.

 Created 30 Nov. 2009
 Modified 28 Oct 2010
 by Tom Igoe

 */

#include <Stepper.h>

const int stepsPerRevolution = 32;  // change this to fit the number of steps per revolution
// for your motor


// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8,10,9, 11);  // note that pins 2 & 3 are inverted!!!


void setup() {
  // nothing to do inside the setup
  myStepper.setSpeed(500);
}

void loop() {
  // Rotate CW slowly
  //myStepper.setSpeed(100);
  //myStepper.step(stepsPerRevolution);
  //delay(1000);
  
  // Rotate CCW quickly
  
  myStepper.step(stepsPerRevolution);
  //delay(1000);
}
