#include "led.h"

/// LED CLASS methods ////
Led::Led(int pin): _pin(pin){
  _state = false;
  pinMode(_pin, OUTPUT);
}

void Led::display(bool state){
  _state = state;
  digitalWrite(_pin,_state);
}

void Led::display(){
  display(_state);
}

void Led::toggle(){
  display(! _state);
}


/// PWM LED CLASS methods ////

PWMLed::PWMLed(int pin,
               int freq,
               int resolution, 
               int dutyCycle): 
                 Led(pin),
                 _pwmChannel(PWMMgr::getNextChannel(pin)),
                 _pwmFreq(freq),
                 _pwmDutyCycle(dutyCycle),
                 _pwmResolution(resolution){
    if(_pwmChannel<0){
      while(true);
    }
    ledcSetup(_pwmChannel, _pwmFreq, _pwmResolution);
    ledcAttachPin(_pin, _pwmChannel);
    ledcWrite(_pwmChannel, _pwmDutyCycle) ;
    Serial.println("exit PWMLed constructor");
}

void PWMLed::setDutyCycle(int dutyCycle){
  _pwmDutyCycle = dutyCycle;
  ledcWrite(_pwmChannel, _pwmDutyCycle);
}
void PWMLed::setFreq(int freq){
  _pwmFreq = max(1,freq);

  Serial.println("writing frequency: " + String(_pwmFreq) + " to channel: " + String(_pwmChannel));
  ledcWriteTone(_pwmChannel,_pwmFreq);
  int lds = _state ? _pwmDutyCycle : 0;
  ledcWrite(_pwmChannel, lds) ;
  delay(1010/_pwmFreq);
}

void PWMLed::display(bool state){
  _state = state;
  int lds = _state ? _pwmDutyCycle : 0;
  ledcWrite(_pwmChannel, lds) ;
  Serial.println("writing duty cycle: " + String(lds) + " to channel: " + String(_pwmChannel));
}

/// RGB LED CLASS methods ////
RGBLed::RGBLed(const int pin[3],uint32_t color){ // 0xBBGGRR
  Serial.println(color,HEX);
  for (int i=0;i<3;i++){
    uint8_t b = (color>>(16-(8*i))) & 0xFF;
     Serial.println(b,HEX);
    Serial.println("i:" + String(i)+"  " + String(b));
    _pwmLedVec[i] =  new PWMLed(pin[i],
                                _fullOnFreq,
                                PWM_DEFAULT_RESOLUTION,
                                color>>(16-8*i) & 0xFF);
  }
}
void RGBLed::display(bool state){
  for (int i=0;i<3;i++){
    _pwmLedVec[i]->display(state);
  }
}
void RGBLed::setColor(uint32_t color){
  for (int i=0;i<3;i++){
    _pwmLedVec[i]->setDutyCycle(color>>(16-8*i) & 0xFF);
  }
}

/// RGB PWM LED CLASS methods ////
RGBPWMLed::RGBPWMLed(const int pin[3],
                     uint32_t color):
  RGBLed(pin,color){}
  
//virtual void display();
void RGBPWMLed::setFreq(int freq){
  for(int i=0;i<3;i++){
    _pwmLedVec[i]->setFreq(freq);
  }
}
void RGBPWMLed::setFreq(int *freqVec){
  for(int i =0;i<3;i++){
    _pwmLedVec[i]->setFreq(freqVec[i]);
  }
}
