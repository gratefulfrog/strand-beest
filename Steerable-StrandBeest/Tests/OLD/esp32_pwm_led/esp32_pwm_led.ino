
#include "led.h"
#include "pwmMgr.h"
#include "potMgr.h"

const int simpleLedPin = 5;
const int pwmLedPin = 27;

const int RGBLedPinVec[] = {14,27,22};
const int potPin = 34;
const long unsigned ledBlinkTime = 2000;

// setting PWM properties
int freq = 1;
int ledChannel;
const int resolution = 8;
const int dutyCycle = (pow(2,resolution)-1)/2;

Led *led, *ledVec[2];
PWMLed *pwmLed;
RGBLed *rgbLed;
RGBPWMLed *rgbPwmLed;

PotSpeedControl *psc;


void setup(){
  Serial.begin(112500);
  Serial.println("starting up...");
  psc = new PotSpeedControl(potPin,pwmLedPin);
  while(1){
    psc->update();
    Serial.println("Pot Value: " + String(psc->getPotValue()));
  }
}

/*
void setup(){
  Serial.begin(112500);
  Serial.println("starting up...");
  pinMode(potPin,INPUT);

  led  = new Led(simpleLedPin);
  ledVec[0] = led;
  
  //pwmLed = new PWMLed(pwmLedPin);
  //ledVec[1] = pwmLed;
  rgbPwmLed = new RGBPWMLed(RGBLedPinVec,0xFFFFFF);
  rgbPwmLed->display(true);
  delay(5000);
  rgbPwmLed->setFreq(1); // = new RGBLed(RGBLedPinVec,0x8A90C4);
  rgbPwmLed->display(true);
  delay(15000);
  int f[] = {0,0,17};
  rgbPwmLed->setFreq(f); // = new RGBLed(RGBLedPinVec,0x8A90C4);
  rgbPwmLed->display(true);
  //delay(5000);
}
int getFreq(int newSpeed){
  // newSpeed is value on [0,pow(2,resolution)-1]
  const int maxfreq      = 25,
            minFreq      = 1,
            outputMaxVal = pow(2,resolution)-1;
  return map(newSpeed,0,outputMaxVal,minFreq,maxfreq);
}
*/
void loop(){}
/*
  static long unsigned  lastSimpleLedToggle  = millis();
 
  int speed = map(analogRead(potPin),0,4095,0,255);
  ((PWMLed *)ledVec[1])-> setFreq(getFreq(speed)); 
  
  unsigned long now = millis();
  if(now-lastSimpleLedToggle > ledBlinkTime){
    lastSimpleLedToggle = now;
    ledVec[0]->toggle();
    ledVec[1]->toggle();
  }
}

*/
