
#include "neoPixelMgr.h"

#define PIXEL_PIN           (15)


PixelSegment *psA,*psB,*psC;

void setup(){
 Serial.begin(115200);
  while(!Serial);  
  Serial.println("Starting up!");
  delay(500);
  NeoPixelMgr *pm = new NeoPixelMgr(PIXEL_PIN);
  psA = new PixelSegment(*pm, 0,6,false);
  psB = new PixelSegment(*pm, 10,6,true);
  psC = new PixelSegment(*pm, 6,4,false);
  psA->setDirection(1);
  psB->setDirection(-1);
  psC->setDirection(0);
  psA->setSpeed(255);
  psB->setSpeed(255);
  psC->setSpeed(255);
}

void loop(){
  Updateable::updateAll();
}
