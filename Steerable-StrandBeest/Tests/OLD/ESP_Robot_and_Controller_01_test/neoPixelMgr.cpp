#include "neoPixelMgr.h"

NeoPixelMgr::NeoPixelMgr(int pin, int nbPixels, byte brightness){
  NeoPixels = new Adafruit_NeoPixel(nbPixels, pin, NEO_GRB + NEO_KHZ800);
  NeoPixels->begin();
  NeoPixels->show();
  NeoPixels->setBrightness(brightness);
}
  
void  PixelSegment::_flash(PixelSegment *ps){
  for(int c=ps->_nextStartPixelOffset+ps->_startPixel; c<ps->_startPixel+ps->_nbPixels; c += _flashNbOn ) {
    ps->_pixels.NeoPixels->setPixelColor(c, NeoPixelMgr::blue); 
  }
  ps->_nextStartPixelOffset=(ps->_nextStartPixelOffset+1)%_flashNbOn;
}

void  PixelSegment::_cycle(PixelSegment *ps){    
  ps->_nextStartPixelOffset += ps->_direction*ps->_directionInverter;

  if (ps->_nextStartPixelOffset < 0){
    ps->_nextStartPixelOffset  +=  ps->_nbPixels;
  }
  else if (ps->_nextStartPixelOffset >= ps->_nbPixels){
    ps->_nextStartPixelOffset  -=  ps->_nbPixels;
  }
  
  for (int count=0;count<_cycleNbOn; count++){
    int onPix = (ps->_nextStartPixelOffset+count)%(ps->_nbPixels) + ps->_startPixel ;
    ps->_pixels.NeoPixels->setPixelColor(onPix, 
                                         ps->_direction>0 ? NeoPixelMgr::green 
                                                          : NeoPixelMgr::red);
  }
}
    
PixelSegment::PixelSegment(NeoPixelMgr &pm, 
                           int startPix, 
                           int nbPix, 
                           bool invertDirection):                        
   Updateable(_defaultUpdateDelay),
   _pixels(pm),
  _startPixel(startPix),
  _nbPixels(nbPix),
  _directionInverter(invertDirection ? -1 : 1),
  _direction(0),
  _nextStartPixelOffset(0){    
}
void PixelSegment::setDirection(int dir){  // -1,0,1 reverse, stop, forward
  _direction = dir;
}

void PixelSegment::setSpeed(byte newSpeed){ 
  setPeriod(newSpeed == 0 ? _maxDelay : (unsigned long) (_defaultUpdateDelay * 255/(newSpeed)));
}

void PixelSegment::clear(){
  for (int i=0; i<_nbPixels;i++){
    _pixels.NeoPixels->setPixelColor(_startPixel+i,0);
  }
}

void PixelSegment::_update(long unsigned now){
  clear();
  if (_direction){
    PixelSegment::_cycle(this);
  }
  else{
    PixelSegment::_flash(this);
  }
  _pixels.NeoPixels->show();
}
