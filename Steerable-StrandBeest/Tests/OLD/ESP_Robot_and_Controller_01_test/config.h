#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

///////////////////////////////////////
////////    General defs   ////////////
///////////////////////////////////////

#define CONTROLLER_PEER  (ESPMgr::ESP32_D_Address)
#define ROBOT_PEER       (ESPMgr::ESP32_F_Address)

///////////////////////////////////////
////////   Robot     defs  ////////////
///////////////////////////////////////

// NeoPixels Pins
#define NEOPIXELPIN  (15)
#define START_SPEED  (100)  // just so the strips will blink on startup before connection with peer

// DC Motor Pins
#define ENA  (5)  //(33)  
#define IN1  (17) //(25)  
#define IN2  (16) //(26)  
#define IN3  (4)  //(27)  
#define IN4  (0)  //(14) 
#define ENB  (2)  //(12)  

/*
// Stepper Motor Pins
#define LSTEP      (27)
#define LDIR       (14)
#define RSTEP      (12)
#define RDIR       (13)
*/
///////////////////////////////////////
//////  END Robot     defs ////////////
///////////////////////////////////////

///////////////////////////////////////
//////  Controller Pin defs  //////////
///////////////////////////////////////

// motor direction indicator LEDS: Red Green Blue
#define RRLEDPIN (33)  // RED   right reverse led
#define RFLEDPIN (32)  // GREEN right forward led
#define RSLEDPIN (23)  // BLUE  right stop    led

#define LRLEDPIN (14)  // RED   left reverse led
#define LFLEDPIN (27)  // GREEN left forward led
#define LSLEDPIN (22)  // BLUE  left stop    led

// button input pins: Forward Reverse, ie Green Red
#define LFBUTPIN (13)  // GREEN left forward button
#define LRBUTPIN (12)  // RED   left reverse button

#define RFBUTPIN (26)  // GREEN right forward button
#define RRBUTPIN (25)  // RED   right reverse button

// POT Pins: analog input and led indicator
#define POTINPUTPIN (34)
#define POTLEDPIN   (5)

#endif
