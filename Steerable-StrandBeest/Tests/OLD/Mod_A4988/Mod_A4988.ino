#include <AccelStepper.h>

#define dirPin 14
#define stepPin 27

//#define motorInterfaceType 1


// Create a new instance of the AccelStepper class:
AccelStepper stepper = AccelStepper(AccelStepper::DRIVER, stepPin, dirPin);

int s = 600;
long unsigned tempo = 5000;
void setup() {
  // Set the maximum speed in steps per second:
  stepper.setMaxSpeed(s);
  stepper.setSpeed(s);
}

void loop() {
  static long unsigned lastChange = millis();
  long unsigned now = millis();

  if (now-lastChange > tempo){
    lastChange = now;
    s = -s;
    stepper.setSpeed(s);
  } 
  // Step the motor with a constant speed as set by setSpeed():
  stepper.runSpeed();
}
