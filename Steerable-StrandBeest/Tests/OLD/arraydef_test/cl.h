#ifndef CL_H
#define CL_H
#define RRLEDPIN (33)  // RED   right reverse led
#define RFLEDPIN (32)  // GREEN right forward led
#define RSLEDPIN (23)  // BLUE  right stop    led

#define LRLEDPIN (14)  // RED   left reverse led
#define LFLEDPIN (27)  // GREEN left forward led
#define LSLEDPIN (22)  // BLUE  left stop    led

// button input pins: Forward Reverse, ie Green Red
#define LFBUTPIN (13)  // GREEN left forward button
#define LRBUTPIN (12)  // RED   left reverse button

#define RFBUTPIN (26)  // GREEN right forward button
#define RRBUTPIN (25)  // RED   right reverse button

// POT Pins: analog input and led indicator
#define POTINPUTPIN (34)
#define POTLEDPIN   (5)





class cl{
  protected:
     int _butPinVecVec[2][2] = {{RFBUTPIN,RRBUTPIN},
                                     {LFBUTPIN,LRBUTPIN}};

};
#endif
