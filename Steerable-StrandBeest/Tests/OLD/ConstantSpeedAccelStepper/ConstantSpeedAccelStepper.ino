// ConstantSpeed.pde
// -*- mode: C++ -*-
//
// Shows how to run AccelStepper in the simplest,
// fixed speed mode with no accelerations
/// \author  Mike McCauley (mikem@airspayce.com)
// Copyright (C) 2009 Mike McCauley
// $Id: ConstantSpeed.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $

#include <AccelStepper.h>

#define DEBUG

const int speed           = 500,
          leftForwardPin  = 18, //13,
          leftReversePin  = 5, //12,
          rightForwardPin = 17, //7,
          rightReversePin = 16, //6,
          *pinVec[] = {&leftForwardPin,&leftReversePin,&rightForwardPin,&rightReversePin};

AccelStepper leftStepper(AccelStepper::FULL4WIRE, 15,0,2,4); // 8,10,9,11);  // note that pins 2 & 3 are inverted!!! 
AccelStepper rightStepper(AccelStepper::FULL4WIRE, 23,21,22,19);  // note that pins 2 & 3 are inverted!!! 

void setup(){  
   leftStepper.setMaxSpeed(speed);  // max speed for the 28BYJ-48 
   rightStepper.setMaxSpeed(speed);  // max speed for the 28BYJ-48 
   leftStepper.setSpeed(0);  
   rightStepper.setSpeed(0);  
   for (int i =0;i<4;i++){
    pinMode(*pinVec[i],INPUT_PULLUP);
   }
   Serial.begin(115200);
   Serial.println("Starting up");
}

void showPins(){
  Serial.print("(");
  Serial.print(digitalRead(leftForwardPin));
  Serial.print(" ");
  Serial.print(digitalRead(leftReversePin));
  Serial.print(") (");
  Serial.print(digitalRead(rightForwardPin));
  Serial.print(" ");
  Serial.print(digitalRead(rightReversePin));
  Serial.println(")");
}

void loop(){
  #ifdef DEBUG
  showPins();
  #endif
  
  int leftSpeed  = (!digitalRead(leftForwardPin) ? speed 
                                                 : !digitalRead(leftReversePin) ? -speed 
                                                                                : 0), 
      rightSpeed = (!digitalRead(rightForwardPin) ? -speed 
                                                  : !digitalRead(rightReversePin) ? speed 
                                                                                  : 0); 
  if (leftSpeed){                                                                               
    leftStepper.setSpeed(leftSpeed); 
    leftStepper.runSpeed();
  }
  else{
    leftStepper.disableOutputs();
  }
  
  if (rightSpeed){                                                                               
    rightStepper.setSpeed(rightSpeed); 
    rightStepper.runSpeed();
  }
  else{
    rightStepper.disableOutputs();
  }
  
  //leftStepper.setSpeed(-speed); 
    //leftStepper.runSpeed();
}


/*
int speed0 = 500,
    speed1 = -speed0;

void setup(){  
   stepper0.setMaxSpeed(500);  // max speed for the 28BYJ-48 
   stepper1.setMaxSpeed(500);  // max speed for the 28BYJ-48 
   stepper0.setSpeed(speed0);	
   stepper1.setSpeed(speed1);  
   Serial.begin(115200);
   Serial.println("Starting up");
}

void loop(){
  static int sign = 1;
  if (Serial.available()){  
    Serial.read();
    sign =-sign;
    stepper0.setSpeed(speed0*sign);  
    stepper1.setSpeed(speed1*sign); 
  }
   stepper0.runSpeed();
   stepper1.runSpeed();
}
*/

/*
void loop(){  
  static double count = 0;
  static const double rCount = 100000;
  static int sign = 1;
  if (count++ == rCount){
    sign = -sign;
    stepper0.setSpeed(speed0*sign);  
    stepper1.setSpeed(speed1*sign); 
    count = 0;
  }
   stepper0.runSpeed();
   stepper1.runSpeed();
}
*/
