// ESP32 BLink

const int LED_PIN = LED_BUILTIN;

void setup() {
  pinMode(LED_PIN,OUTPUT);
}

void loop() {
  digitalWrite(LED_PIN,!digitalRead(LED_PIN));
  delay(500);
}
