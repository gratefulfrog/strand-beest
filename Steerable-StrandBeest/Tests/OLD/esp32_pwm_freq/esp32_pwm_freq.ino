// the number of the LED pin
const int ledPin = 27;  // 16 corresponds to GPIO16

const int potPin = 34;

// setting PWM properties
int freq = 1;
const int ledChannel = 0;
const int resolution = 8;


int getFreq(int newSpeed){
  // newSpeed is value on [0,pow(2,resolution)-1]
  const int maxfreq      = 25,
            minFreq      = 1,
            outputMaxVal = pow(2,resolution)-1;
  return map(newSpeed,0,outputMaxVal,minFreq,maxfreq);
}

void setup(){
  Serial.begin(115200);
  Serial.println("starting up...");

  pinMode(potPin,INPUT);
  
  // configure LED PWM functionalitites
  ledcSetup(ledChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(ledPin, ledChannel);
  ledcWrite(ledChannel, 112);
  while(1){
    int speed = map(analogRead(potPin),0,4095,0,255);
    
    freq = getFreq(speed);
    ledcWriteTone(0,freq);
    Serial.println(freq);
    delay(1100/freq);
  }
}
 
void loop(){
  // increase the LED brightness
  for(int dutyCycle = 0; dutyCycle <= 255; dutyCycle++){   
    // changing the LED brightness with PWM
    ledcWrite(ledChannel, dutyCycle);
    //delay(15);
  }

  // decrease the LED brightness
  for(int dutyCycle = 255; dutyCycle >= 0; dutyCycle--){
    // changing the LED brightness with PWM
    ledcWrite(ledChannel, dutyCycle);   
    //delay(15);
  }
}
