const int BIA = 10; 
const int BIB = 11; 

byte speed = 0;
void setup() { 
  Serial.begin(115200);
  Serial.println("starting up");
  pinMode(BIA, OUTPUT);   
  pinMode(BIB, OUTPUT); 
}

void loop() {   
  Serial.println(speed);
  forward();    
  delay(2000);   
  backward();   
  delay(2000); 
  speed = (speed +16)%256;
}
  
void backward() { 
  analogWrite(BIA, 0); 
  analogWrite(BIB, speed); 
} 
  
void forward() { 
  analogWrite(BIA, speed); 
  analogWrite(BIB, 0); 
}
