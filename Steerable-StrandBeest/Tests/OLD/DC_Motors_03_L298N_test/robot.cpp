#include "robot.h"

void DCRobot::setDirection(int dir){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->setDirection(dir);
  }
}
void DCRobot::toggleSpeedRange(){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->toggleSpeedRange();
  }
}
void DCRobot::runSpeed(){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i]->runSpeed();
  }
}

DCRobotL298N::DCRobotL298N(const int *pinVec){
  for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i] =  new DCMotorL298N(pinVec[0+i*DCMotorL298N::nbPins],
                                     pinVec[1+i*DCMotorL298N::nbPins],
                                     pinVec[2+i*DCMotorL298N::nbPins]);
  }
}

DCRobotL9110::DCRobotL9110(const int *pinVec){
 for (int i=0;i<DCRobot::nbMotors;i++){
    _motorVec[i] =  new DCMotorL9110(pinVec[0+i*DCMotorL9110::nbPins],
                                     pinVec[1+i*DCMotorL9110::nbPins]);
  }
}
