#include "robot.h"

//#define ROBOT_L928N

const int enA = 33;  
const int in1 = 25;  
const int in2 = 26;  
const int in3 = 27;  
const int in4 = 14; 
const int enB = 12;  
const int pinVecL298N[] = {enA,in1,in2,
                           enB,in3,in4};
const int pinVecL9110[] = {in1,in2,
                          in4,in3};                           

DCRobot *robot;

void setup(){
  Serial.begin(115200);
  #ifdef ROBOT_L928N
    Serial.println("Using L928N driver...");
    robot = new DCRobotL298N(pinVecL298N);
  #else
    Serial.println("Using L9110 driver...");
    robot = new DCRobotL9110(pinVecL9110);
  #endif
}

void loop(){
  static int dir = 0;
  Serial.println("Setting Direction: " + String(dir));
  robot->setDirection(dir);
  robot->runSpeed();
  delay(2000);
  robot->setDirection(0);
  robot->runSpeed();
  Serial.println("Waiting for your input!");
  while (!Serial.available());    
    int val = Serial.parseInt();
    if (abs(val)>1){
      robot->toggleSpeedRange();
      Serial.println("Toggling Speed: ");
    }
    else{
      dir = (val > 0 ? 1 : val < 0 ? -1 : 0);
    }
}
