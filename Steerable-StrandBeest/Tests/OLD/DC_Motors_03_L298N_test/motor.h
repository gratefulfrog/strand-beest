 #ifndef BMOTOR_H
 #define BMOTOR_H

#include <Arduino.h>

// abstract motor class

class DCMotor2Speed{
  protected:
    static const int _pwmFrequency   =  10, 
                     _pwmResolution  =   8, 
                     _slowSpeedValue = 100, 
                     _fastSpeedValue = 255; 

    static int _nextIndex;
    static int _getIndex();
    const int _pinVec[2];

    int _direction  = 0, // +1 positive speed, -1 negative speed, 0 stopped
        _speedValue = 0; // (set in concrete class to _fast or _slow speed value
    virtual void _runSpeed() const = 0;
    void _setDirection(int dir); // -1,0,1
    void _toggleSpeedRange();
  public:
    void runSpeed() const;
    void setDirection(int dir); // -1,0,1
    void toggleSpeedRange();
    DCMotor2Speed(int iPin0, int iPin1);
};

// DC motor concrete class
class DCMotorL298N : public DCMotor2Speed{
  protected:
    const int  _pwmIndex;
    const int _enablePin;

    void _runSpeed() const;
  public:
    static const int nbPins = 3;
    DCMotorL298N(int ePin, int i1Pin, int i2Pin);
};

// DC motor concrete class
class DCMotorL9110 : public DCMotor2Speed{    
    static const int  _nbPWMChannels = 2;
    
    const int  _pwmIndex[_nbPWMChannels];
                 
    void _runSpeed() const;
       
  public:
    static const int nbPins = 2;
    DCMotorL9110(int i1Pin, int i2Pin);
};

#endif
