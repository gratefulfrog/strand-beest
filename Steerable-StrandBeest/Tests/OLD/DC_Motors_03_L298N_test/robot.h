#ifndef ROBOT_H
#define ROBOT_H

#include <Arduino.h>
#include "motor.h"

class DCRobot{
  protected:
    static const int nbMotors = 2;
    DCMotor2Speed *_motorVec[nbMotors];
    virtual void _unused() = 0;
  public:
    //DCRobot(int *pinVec);
    void setDirection(int dir);
    void toggleSpeedRange();
    void runSpeed();
};

class DCRobotL298N:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobotL298N(const int *pinVec);
};

class DCRobotL9110:public DCRobot{
  protected:
    virtual void _unused(){};
  public:
    DCRobotL9110(const int *pinVec);
};

#endif
