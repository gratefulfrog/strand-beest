 #include "motor.h"
 
 // abstract motor class
 void DCMotor2Speed::setDirection(int dir){
  _direction = ((dir > 0) ? 1 : ((dir < 0) ? -1 : 0));
}

void DCMotor2Speed::runSpeed() const{
  _runSpeed();
}
void DCMotor2Speed::toggleSpeedRange(){
  _speedValue = ((_speedValue == _fastSpeedValue) ? _slowSpeedValue 
                                                  : _fastSpeedValue);
}

DCMotor2Speed::DCMotor2Speed(int iPin0, int iPin1):
  _pinVec({iPin0,iPin1}){
}

// DC Motor concrete class

int DCMotor2Speed::_nextIndex = 0;
int DCMotor2Speed::_getIndex(){
  return DCMotor2Speed::_nextIndex++;
}

void DCMotorL298N::_runSpeed() const{
  switch (_direction){
    case -1: // backward
      digitalWrite(_pinVec[1], HIGH); 
      digitalWrite(_pinVec[0], LOW);
      break;
    case 1:  // forward
      digitalWrite(_pinVec[0], HIGH); 
      digitalWrite(_pinVec[1], LOW); 
      break;
    default: //case 0: turn off
      digitalWrite(_pinVec[0],LOW);
      digitalWrite(_pinVec[1],LOW);
      break;
 
  }
  ledcWrite(_pwmIndex, _speedValue);
}

DCMotorL298N::DCMotorL298N(int ePin, int i1Pin, int i2Pin):
  DCMotor2Speed(i1Pin,i2Pin),
  _enablePin(ePin),
  _pwmIndex(DCMotor2Speed::_getIndex()){
    _speedValue = _slowSpeedValue;
    pinMode(_enablePin, OUTPUT);
    pinMode(_pinVec[0], OUTPUT);
    pinMode(_pinVec[1], OUTPUT);
    
    ledcSetup(_pwmIndex, _pwmFrequency, _pwmResolution);
    ledcAttachPin(_enablePin, _pwmIndex);
}


void DCMotorL9110::_runSpeed() const{
   switch (_direction){
    case -1: // backward
      //Serial.println("Running Backward: " + String(_speedValue));
      digitalWrite(_pinVec[0], LOW); 
      ledcWrite(_pwmIndex[1],_speedValue); 
      break;
    case 1:  // forward
      digitalWrite(_pinVec[1], LOW); 
      ledcWrite(_pwmIndex[0],_speedValue); 
      break;
    default: //case 0: turn off
      for (int i=0;i<_nbPWMChannels;i++){
        digitalWrite(_pinVec[i],LOW);
        ledcWrite(_pwmIndex[i],0);
      }
      break;
  }
}

DCMotorL9110::DCMotorL9110(int i1Pin, int i2Pin):
  DCMotor2Speed(i1Pin,i2Pin),
  _pwmIndex({DCMotor2Speed::_getIndex(),DCMotor2Speed::_getIndex()}){
    _speedValue = _slowSpeedValue;
    //Serial.println("Creating DCL9110 motor on pins: " + String(i1Pin) + "," + String(i2Pin));
    pinMode(_pinVec[0], OUTPUT);
    pinMode(_pinVec[1], OUTPUT);
    for (int i =0;i<_nbPWMChannels;i++){
      //Serial.println("setting up PWM: " + String(_pwmIndex[i]));
      ledcSetup(_pwmIndex[i], _pwmFrequency, _pwmResolution);
      ledcAttachPin(_pinVec[i], _pwmIndex[i]);
    }
}
