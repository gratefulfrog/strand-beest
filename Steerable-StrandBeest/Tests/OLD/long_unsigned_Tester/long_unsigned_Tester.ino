void setup() {
  Serial.begin(115200);
}

void loop() {
  static unsigned long val = millis();
  Serial.println("Val: " + 
                 String(val) + 
                 " times 0.5: " + 
                 String(val*0.5) + 
                 " as long unsigned: " +
                 String((ulong unsigned)(val*0.5))); 
  val +=val+millis();                  
  delay(500);
}
