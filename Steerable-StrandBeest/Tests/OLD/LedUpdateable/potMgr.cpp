#include "potMgr.h"

PotMgr::PotMgr(int pPin):_potPin(pPin){
  pinMode(_potPin,INPUT);
}
byte PotMgr::getPotValue() const{
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  byte res = map(potValue,
                  0,_adcMaxVal,
                  0, _outputMaxVal);  
  return res;
}
byte  PotSpeedControl::_getFreq(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  return map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
}

unsigned long PotSpeedControl::_getPeriod(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  int freq = map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
  return 1000/freq;
}


PotSpeedControl::PotSpeedControl(int pPin,int lPin):
  PotMgr(pPin),
  _blinkingLed(lPin){
  getAndUpdate();
}
byte PotSpeedControl::getFreq() const{
  return _getFreq(getPotValue());
}

unsigned long  PotSpeedControl::getPeriod() const{
  return _getPeriod(getPotValue());
}

void PotSpeedControl::update(){
  _blinkingLed.setPeriod(getPeriod());
}

byte PotSpeedControl::getAndUpdate(){
  update();
  return getFreq();
}
  
