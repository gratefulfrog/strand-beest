#include "updateable.h"

Updateable *u;
String n = "Frank",
       nn ="jj",
       nnn = "Frank, le retour!";

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("starting up...");
  new UTester(1000,n);
  new UTester(1500,nn);
  new UTester(1700,nnn);
}

void loop() {
  Updateable::updateAll();
}
