#ifndef LED_BAR_CONTROL_H
#define LED_BAR_CONTROL_H

#include <Arduino.h>
#define DEBUG

class LedBarController{
  protected:
    static const int nbLeds = 5;
    static const long unsigned incInterval = 75; //milliseconds

    int  ledOffIndex = 2,
         ledPinVec[5],
         colorPin;
    long unsigned lastUpdateTime;
    void allOff();
    void updatePins(bool forward);
    
  public:
    LedBarController(int cPin, int *pinVec);
    void setSpeedColor(int speed);
    void update();    
    #ifdef DEBUG
    bool show();
    #endif
};
#endif
