#include "ledBarControl.h"

LedBarController::LedBarController(int cPin, int *pinVec) {
  colorPin = cPin;
  pinMode(colorPin,OUTPUT);
  digitalWrite(colorPin,0);
  for (int i = 0;i< nbLeds;i++){
    ledPinVec[i] = pinVec[i];
    pinMode(ledPinVec[i],OUTPUT);
    digitalWrite(ledPinVec[i], i == ledOffIndex);
    Serial.print(digitalRead(ledPinVec[i]));
  }
  Serial.println();
}

void LedBarController::allOff(){
  digitalWrite(colorPin,0);
  for (int i = 0;i< nbLeds;i++){
    digitalWrite(ledPinVec[i], 0);
  }
  ledOffIndex = 2;
  lastUpdateTime = millis();
}

void LedBarController::updatePins(bool forward){
  for (int i = 0;i< nbLeds;i++){
    digitalWrite(ledPinVec[(forward ? i : nbLeds-1-i)], i == ledOffIndex);
  }
                              
}

void LedBarController::update(){
  static bool forward = true;
  unsigned long now = millis();
  if (now - lastUpdateTime < incInterval){
    return;
  }
  lastUpdateTime = now;
  ledOffIndex = (ledOffIndex+1)%nbLeds;
  updatePins(forward);
  forward = show();
}

void LedBarController::setSpeedColor(int speed){
  if (!speed){
    allOff();
  }
  else{
    digitalWrite(colorPin,speed>0);
  }
}


#ifdef DEBUG
bool LedBarController::show(){
  static int count        = 0,
             reverseLimit = 100;
  static bool res = true;
  Serial.print(digitalRead(colorPin));
  Serial.print(":");
  for (int i = 0;i< nbLeds;i++){
    Serial.print(digitalRead(ledPinVec[i]));
  }
  Serial.println();
  count = (count+1)%reverseLimit;
  //Serial.println(count);
  res = count == 0 ? !res : res;
  return res;
}
#endif
 
