#include "ledBarControl.h"

#define R0  (12)
#define R1  (14)
#define R2  (27)
#define R3  (26)
#define R4  (25)
/*
#define L0  (16)
#define L1   (4)
#define L2   (0)
#define L3   (2)
#define L4  (15)
*/
#define LEFT_COLOR_PIN  (23)
#define RIGHT_COLOR_PIN (15)

LedBarController *rlbc;

void setup() {
  Serial.begin(115200);
  Serial.println("Starting Up!");
  
  int rVec[] = {R0,R1,R2,R3,R4};
  
  rlbc =  new LedBarController(RIGHT_COLOR_PIN,rVec);
  rlbc->show();
}

void loop() {
  rlbc->update();
}
