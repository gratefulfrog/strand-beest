#include "robot.h"

#define ROBOT_3Pin  // this works with both the L298N and TB6612 drivers
// if not defined, then it works with the L9110 driver

const int enA = 33;  
const int in1 = 25;  
const int in2 = 26;  
const int in3 = 27;  
const int in4 = 14; 
const int enB = 12;  
const int pinVec3Pin[] = {enA,in1,in2,
                           enB,in3,in4};
const int pinVec2Pin[] = {in1,in2,
                          in4,in3};                           
const int potPin = 34;
DCRobot *robot;

void setup(){
  Serial.begin(115200);
  #ifdef ROBOT_3Pin
    Serial.println("Using 3Pin Robot...");
    robot = new DCRobot3Pin(pinVec3Pin);
  #else
    Serial.println("Using 2Pin Robot...");
    robot = new DCRobot2Pin(pinVec2Pin);
  #endif
  pinMode(potPin,INPUT);
}
void updateSpeedFromPot(){
  static byte newSpeed = 0;
  int potValue = analogRead(potPin);  // ESP32 has 12 bit ADC [0,4095]
  // reduce the resolution to steps of 10 on [0,255] i.e. one byte
  byte temp = min(255,(int)(10*round(map(potValue,0,4095,0,255)/10.0)));
  if (temp != newSpeed){
    newSpeed = temp;
    Serial.println("Setting Speed: " + String(newSpeed));
    robot->setSpeed(newSpeed);
  }
}

void loop(){
  static int dir = 1;
  updateSpeedFromPot();
  robot->setDirection(dir);
  robot->runSpeed();
}
