#include <SPI.h>

// SPI Params
static const int spiClk = 1000000; // 1 MHz

SPIClass * vspi = NULL;

// RGB Led and application params
// note: this version sends one led running for forward and backward movement

const long unsigned updateDelay = 100;
uint16_t res = 0;
  
const int  off = 0,
             B = 1,
             G = 2,
             R = 3,
        nbLeds = 5,
        countLim = 50;

int offIndex      = -1,
    colorIndex    =  B,
    normedSpeed   =  0,
    base3ValVec[] = {0,0,0,0,0};
    
void setSpeed (int s){
  normedSpeed = (s < 0 ? -1 : (s == 0 ? 0 : 1));
  offIndex    = 2;
  res = 0;
  switch(normedSpeed){
    case 0:
      colorIndex =  B;
      offIndex   = -1;
      break;
    case -1:
      colorIndex = R;
      break;
    default:
      colorIndex = G;
      break;
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("starting up!");
  //initialise two instances of the SPIClass attached to VSPI and HSPI respectively
  vspi = new SPIClass(VSPI);
  //hspi = new SPIClass(HSPI);
  
  //clock miso mosi ss
  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();

  //alternatively route through GPIO pins of your choice
  //vspi->begin(VSPI_SCLK, VSPI_MISO, VSPI_MOSI, VSPI_SS); //SCLK, MISO, MOSI, SS

  //set up slave select pins as outputs as the Arduino API
  //doesn't handle automatically pulling SS low
  pinMode(SS, OUTPUT); //VSPI SS
  setSpeed(0);
}

void incOffIndex(){
  if (normedSpeed ==0){
    return;
  }
  offIndex += normedSpeed;
  offIndex = (offIndex < 0 ? nbLeds -1 : (offIndex == nbLeds ? 0 : offIndex)) ; 
}

void changeSpeed(){
  const int speedVec[] =  {-1,0,1};
  static int curIndex = normedSpeed,
             count    = 0;

  if (count++ > countLim){
    count = 0;
    curIndex = (curIndex+1)%3;
    setSpeed(speedVec[curIndex]);
  }
}

void updateLedVec(){
  static long unsigned lastUpdate = millis();
  long unsigned now = millis();
  uint8_t data0 =0,
          data1 = 0;
          
  if (now - lastUpdate > updateDelay){
    lastUpdate = now;
    incOffIndex();
    Serial.print("offIndex: ");
    Serial.print(offIndex);
    Serial.print("  : ");
    Serial.print("colorIndex: ");
    Serial.print(colorIndex);
    Serial.print("  : ");
    for (int i=0;i<nbLeds;i++){
      uint16_t rVal = 1<<((3*i)+colorIndex-1);
      if (offIndex == -1){
        res ^=  rVal;
      }
      else if (offIndex != i){
        res &= ~rVal;
      }
      else{
        res |= rVal;
      }
    }
    data1 = (res>>8)&0xFF;  //LSB First!
    data0 = res&0xFF;
    showByteBin(data0);
    Serial.print(" : ");
    showByteBin(data1);
    Serial.print("  : @ ");
    Serial.println(millis());
    send2Bytes(data0, data1);
    changeSpeed();
  }
}

// the loop function runs over and over again until power down or reset
void loop() {
  vspiCommand();
}

void showByteBin(byte b){
  for (int i=7;i>-1;i--){
    Serial.print(b&(1<<i)? 1 : 0);
  }
}

void send2Bytes(byte data0, byte data1){
  //use it as you would the regular arduino SPI API
  vspi->beginTransaction(SPISettings(spiClk, LSBFIRST, SPI_MODE0));
  digitalWrite(SS, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data0);  
  vspi->transfer(data1);
  digitalWrite(SS, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();
}

void vspiCommand() {
  const byte allBlues[]  = {0b10010010,
                            0b01001000},  // last is white on end
             allGreens[] = {0b01001001,
                            0b00100100},
             allReds[]   ={0b00100100,
                           0b10010010};

  updateLedVec();  
}
