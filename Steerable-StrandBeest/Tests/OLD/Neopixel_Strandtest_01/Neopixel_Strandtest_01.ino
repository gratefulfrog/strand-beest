// A basic everyday NeoPixel strip test program.

// NEOPIXEL BEST PRACTICES for most reliable operation:
// - if using lab power supply:  Add 1000 uF CAPACITOR between NeoPixel strip's + and - connections.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel strip,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.

#include <Adafruit_NeoPixel.h>

#define STRIP_PINX    (32)
#define STRIP_PINY    (33)

#include "stripper.h"

Stripper *l,*r;

void setup() {  
  l = new Stripper(STRIP_PINX);
  l->setSpeed(0);
  r = new Stripper(STRIP_PINY);
  r->setSpeed(1);
}
void loop(){
  l->update();
  r->update();
}
