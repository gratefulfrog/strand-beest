// connect motor controller pins to Arduino digital pins 

#include "motor.h"

int enA = 33; //19; 
int in1 = 25; //18; 
int in2 = 26; //5; 
// motor two  
int in3 = 27; //16; 
int in4 = 14; //17;
int enB = 12; //4; 

int speed = 10;  // range [11, 15]
long unsigned     d     = 2000*10/speed;

BMotor *motL, *motR;

void setSpeed(int s){
  Serial.print("Setting speed:" );
  Serial.println(s);
  speed = s;
  d =  s ? abs(2000*11/s) : d;
  motL->setSpeed(speed);
  motR->setSpeed(speed);
}


void setup(){
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up!");
  
  motL = new DCMotor(enA,in1,in2);
  motR = new DCMotor(enB,in3,in4);
  setSpeed(speed);
}

void runForD(){
  static long unsigned lastUpdate = millis();
  long unsigned now = millis();
  static bool ok = true;
  static int oldSpeed;

  if (now-lastUpdate > d){
    lastUpdate = now;
    if (ok){
      oldSpeed = speed;
      setSpeed(0);   
    }
    else{
      setSpeed(-oldSpeed);
      oldSpeed  = -oldSpeed;
    }
    ok = !ok;
  }
}

void loop(){
  if (Serial.available()){
    setSpeed(Serial.parseInt());
    Serial.print("Speed: ");
    Serial.println(speed);
    Serial.print("Delay: ");
    Serial.println(d);
  }
  runForD();
}
