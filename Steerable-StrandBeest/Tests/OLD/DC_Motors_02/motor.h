 #ifndef BMOTOR_H
 #define BMOTOR_H

#include <Arduino.h>

// abstract motor class

class BMotor{
  protected:
    int _direction = 0, // +1 positive speed, -1 negative speed
        _speed     = 0;
    virtual void _update() = 0;
    virtual void _setSpeed(int s) = 0;
    virtual void _setDirction(int dir);
  public:
    void update();
    void setSpeed(int s);
};

// DC motor class
class DCMotor : public BMotor{
  protected:
    static const int _pwmFrequency  = 100000,
                     _pwmResolution = 4,
                     _slowSpeedValue = 11,
                     _fastSpeedValue = 14;

    static int _nextIndex;
    static int _getIndex();
                 
    virtual void _update();
    void _setSpeed(int s);
    void _toggleSpeedRange();

    const int  _en,
               _in1,
               _in2,
               _pwmIndex;

   int _speedValue = _slowSpeedValue;
   
  public:
    DCMotor(int ePin, int i1Pin, int i2Pin);
};


 #endif
