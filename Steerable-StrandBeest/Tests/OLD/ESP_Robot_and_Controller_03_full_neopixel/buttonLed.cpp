#include "buttonLed.h"

ButtonLedCombo::ButtonLedCombo(int index, const int *buttonPinVec, NeoPixelMgr &pm){
  _pf = new PixelFlasher(pm, index == 0 ? 0 : CONTROLLER_NB_PIXELS - BUTTON_NB_PIXELS, BUTTON_NB_PIXELS);
  for (int i=0;i<_nbButtons;i++){
    _buttonPinVec[i] = buttonPinVec[i];
    pinMode(_buttonPinVec[i],INPUT_PULLUP);
   }
}
byte ButtonLedCombo::getAndUpdate(){
  byte res = 0; 
  for (int i=0;i<_nbButtons;i++){
    byte val = !digitalRead(_buttonPinVec[i]);   
    res |= (val)<<i;
  }
  _pf->setDirection(res == 0 ? 0
                             : res == 1 ? 1
                                        : -1);
  return res;
}
