#include "potMgr.h"

PotMgr::PotMgr(int pPin):_potPin(pPin){
  pinMode(_potPin,INPUT);
  /*pinMode(13,INPUT);
  
  while(1){
    Serial.println("getPotValue() value: " + String(_potPin) + " "  + String(getPotValue()));
  }
  */
   for (int i=0; i<_nbValues;i++){
    _valueVec[i] = analogRead(_potPin);                            
  }
}
byte PotMgr::getPotValue() {
  int potValue = analogRead(_potPin);  // ESP32 has 12 bit ADC [0,4095]
  //Serial.println("raw value: " + String(potValue));
  //Serial.println("raw raw value: " + String(analogRead(14)));
  _valueVec[_valueVecIndex] = map(potValue,
                                  0,_adcMaxVal,
                                  0, _outputMaxVal);  
  //Serial.println(  _valueVec[_valueVecIndex]);                                
  _valueVecIndex = (_valueVecIndex+1)%_nbValues;
  int res = 0;
  for (int i=0; i<_nbValues;i++){
    res +=  _valueVec[i];                              
  }
  return res/10;
}
byte  PotSpeedControl::_getFreq(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  return map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
}

unsigned long PotSpeedControl::_getPeriod(int potVal) const{
  // potVal is value on [0,pow(2,resolution)-1]          
  int freq = map(potVal,0,_outputMaxVal,_minFreq,_maxfreq);
  return 1000/freq;
}

PotSpeedControl::PotSpeedControl(int pPin, NeoPixelMgr &pm, int startPixel, int nbPixels):
  PotMgr(pPin){
    _pcs =  new PixelColorSlider(pm,startPixel,nbPixels);
    update();
}
byte PotSpeedControl::getFreq(){
  return _getFreq(getPotValue());
}

unsigned long  PotSpeedControl::getPeriod(){
  return _getPeriod(getPotValue());
}

void PotSpeedControl::update(){
  _pcs->setSpeed(getPotValue());
}

byte PotSpeedControl::getFreqAndUpdate(){
  update();
  return getFreq();
}

byte PotSpeedControl::getPotValueAndUpdate(){
  update();
  //Serial.println("pot value : " +String(getPotValue()));
  return getPotValue();
}

  
