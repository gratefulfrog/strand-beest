#ifndef UPDATEABLE_H
#define UPDATEABLE_H

#include <Arduino.h>

class Updateable;
struct uCell;

typedef struct uCell {
  Updateable   *u;
  uCell        *next;
} uCell;

class Updateable{
  protected:
    static void     _newUpdateable(Updateable *newUpdateable);
    static uCell   *_updateableLinkedList;
    long unsigned   _updatePeriod;       // milliseconds
    long unsigned   _lastUpdateTime = 0;  // milliseconds

    virtual void _update(long unsigned now) = 0;  // declared like this means it must be defined in each child class
  
  public:
    static void updateAll();
    Updateable(long unsigned updatePeriod);
    Updateable();
    void update(unsigned long now);
    void setPeriod(unsigned long newDelay);
    unsigned long getDelay() const;
};

class UTester: public Updateable{
  protected:
    String &_name;
    virtual void _update(long unsigned now);
  public:
    UTester(long unsigned udPeriod,String &name);
};

#endif
