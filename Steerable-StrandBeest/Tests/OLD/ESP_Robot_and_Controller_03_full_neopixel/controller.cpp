#include "controller.h"

void Controller::_send(){
  espMgr->sendByteVec(_outgoingVec,_msgLength);
}

void Controller::_updateOutgoing(){
  for (int i=0; i< _msgLength;i++){
    _outgoingVec[i] = 0;
  }
  for (int i=0;i<_msgLength;i++){
    _outgoingVec[0] |= (_blcVec[i]->getAndUpdate())<<(i*2);
  }
  _outgoingVec[1] = _psc->getPotValueAndUpdate();
}

Controller::Controller(const uint8_t *peer): App(peer){
  //_pm = new NeoPixelMgr(CONTROLLER_NEOPIXELPIN,CONTROLLER_NB_PIXELS);
  //_psc = new PotSpeedControl(14,*_pm,POT_SLIDER_START_PIXEL,POT_SLIDER_NB_PIXELS);
  
  for (int i=0;i<_nbButtonLedCombos;i++){
   // _blcVec[i] = new ButtonLedCombo(i,_butPinVecVec[i],*_pm); 
  }
}

bool Controller::_updateNeeded(){
  bool res = ((_lastOutgoingVec[0] != _outgoingVec[0]) ||
              (abs((_lastOutgoingVec[1]- _outgoingVec[1]))>_minSpeedChange));
  if (res){ 
    for (int i=0; i< _msgLength;i++){
      _lastOutgoingVec[i] = _outgoingVec[i];  
    }
  }
  return res;
}

void Controller::showOutgoing() const{
  for (int i=_nbButtonLedCombos*2-1;i>-1;i--){// 2 bits per combo
    Serial.print(_outgoingVec[0]>>i&1);  
  }
  Serial.print(" : ");
  Serial.println(_outgoingVec[1],DEC);
}
void Controller::loop(){
  Updateable::updateAll();
  
  _updateOutgoing();
  if (_updateNeeded()){
    showOutgoing();
    _send();
  }
}
