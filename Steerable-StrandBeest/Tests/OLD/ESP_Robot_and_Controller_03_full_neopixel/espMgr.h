#ifndef ESPMGR_H
#define ESPMGR_H

#include <Arduino.h>
#include <esp_now.h>
#include <WiFi.h>

#include "outils.h"
//#include "robot.h"

class Robot;

class ESPMgr{
  public:
    static const uint8_t ESP32_B_Address[6], // = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},    // current config robot
                         ESP32_C_Address[6], // = {0x24, 0x0A, 0xC4, 0x59, 0xBA, 0x34},    // spare MCU 
                         ESP32_D_Address[6], // = {0xAC, 0x67, 0xB2, 0x3D, 0x5F, 0x50},     // current config controller
                         ESP32_E_Address[6], // = {0xAC, 0x67, 0xB2, 0x3D, 0x52, 0x30},    // spare MCU   
                         ESP32_F_Address[6], // = {0xAC, 0x67, 0xB2, 0x50, 0xB3, 0xF8},    // spare MCU   
                         ESP32_G_Address[6]; // = {0xAC, 0x67, 0xB2, 0x50, 0x7C, 0xEC},    // spare MCU   
                         
  protected:    
    static Robot *robotPtr; 
    static ESPMgr *thisPtr;                        
    
    uint8_t _myMac[6],
            _peerAddress[6];

    // cannot go out of scope, for some reason??
    esp_now_peer_info_t *peerInfo;

    ////////////////////// EPS NOW Functions ///////////////
    // Callback when data is sent
    static void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status);
    // Callback when data is received
    static void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len);
 
    void setupWifi();
    void registerAndAddPeer();

  public:
    esp_now_send_status_t sendStatus = (esp_now_send_status_t)!ESP_NOW_SEND_SUCCESS;
    
    ESPMgr(const uint8_t *peerAddress, Robot *robotPtr = NULL);
    void sendByteVec(byte outgoing[],int msgLen = 1) const;
};

#endif
