#ifndef BUTTONLED_H
#define BUTTONLED_H

//#include "led.h"
#include "neoPixelMgr.h"

class ButtonLedCombo{
  protected:
    static const int _nbButtons = 2;
    //RGBLed _rgbLed;
    int _buttonPinVec[_nbButtons]; // {greenbutton, redbutton}
    PixelFlasher *_pf;

  public:
    ButtonLedCombo(int index,const int *buttonPinVec, NeoPixelMgr &pm);
    byte getAndUpdate();
};

#endif
