#include "updateable.h"

uCell *Updateable::_updateableLinkedList = NULL;

void Updateable::_newUpdateable(Updateable *newUpdateable){
  if (_updateableLinkedList == NULL){
    _updateableLinkedList = new uCell({newUpdateable,NULL});
    return;
  }
  uCell *current = _updateableLinkedList;
  while (current->next){
    current = current->next;
  }
  current->next = new uCell({newUpdateable,NULL});
}

Updateable::Updateable(long unsigned updatePeriod):_updatePeriod(updatePeriod) {
  _newUpdateable(this);
}
Updateable::Updateable(){
  _updatePeriod = 99999999;
  _newUpdateable(this);
}
void Updateable::update(unsigned long now){
  if(now-_lastUpdateTime>= _updatePeriod){
    _lastUpdateTime = now;
    _update(now);
  }
}
void Updateable::setPeriod(unsigned long newPeriod){
  _updatePeriod = newPeriod;
}
unsigned long Updateable::getDelay() const{
  return _updatePeriod;
}

void Updateable::updateAll(){
  uCell *current = _updateableLinkedList;
  unsigned long now = millis();
  while(current!=NULL){
    if (current->u != NULL){
      current->u->update(now);
    }
    current = current->next;
  }
}

void UTester::_update(long unsigned now){
  Serial.println(_name + " is updating");
}

UTester::UTester(long unsigned udPeriod,String &name):
  Updateable(udPeriod),_name(name){}
