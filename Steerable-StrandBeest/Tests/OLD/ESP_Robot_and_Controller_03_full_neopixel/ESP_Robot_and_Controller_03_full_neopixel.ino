#include "controller.h"
#include "robot.h"

#define CONTROLLER
//#define MOTOR2PIN
// nothing defined means Motor 3 Pin

App *app;

#ifdef CONTROLLER
void getApp(){
  app = new Controller();
}
#else
  #ifdef MOTOR2PIN
  void getApp(){
    app = new DCRobot2Pin();
  }
  #else
  void getApp(){
    app = new DCRobot3Pin();
  }
  #endif
#endif
PotSpeedControl *psc;
NeoPixelMgr *pm ;

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("starting up...");
  pm = new NeoPixelMgr(32,8); //CONTROLLER_NEOPIXELPIN,CONTROLLER_NB_PIXELS);
  psc = new PotSpeedControl(2, *pm, 2, 4);
  //PotMgr pm = PotMgr(POTINPUTPIN);
  //PixelColorSlider pcs= PixelColorSlider(pm, 2,4);
  //pcs.setSpeed(200);
  //pcs.setDirection(1);
  //while (1){
    //Updateable::updateAll();
    //Serial.println(pm.getPotValue());
  //}
  getApp();
  
  pinMode(2,INPUT);
}

void loop() {
  //app->loop();
  Serial.println("raw raw value: " + String(psc->getPotValue()));
}
