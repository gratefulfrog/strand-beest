// connect motor controller pins to Arduino digital pins 

#include "motor.h"


int enA = 33; //19; 
int in1 = 25; //18; 
int in2 = 26; //5; 
// motor two  
int in3 = 27; //16; 
int in4 = 14; //17;
int enB = 12; //4; 

int speed = 11;  // range [11, 15]
long unsigned     d     = 2000*12/speed;

BMotor *motL, *motR;

void setSpeed(int s){
  Serial.print("Setting speed:" );
  Serial.println(s);
  speed = s;
  d =  s ? abs(2000*11/s) : d;
  motL->setSpeed(speed);
  motR->setSpeed(speed);
}


void setup(){
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up!");
  
  motL = new DCMotor(enA,in1,in2);
  motR = new DCMotor(enB,in3,in4);
  setSpeed(speed);
}

void runForD(){
  static long unsigned lastUpdate = millis();
  long unsigned now = millis();
  static bool ok = true;
  static int oldSpeed;

  if (now-lastUpdate > d){
    lastUpdate = now;
    if (ok){
      oldSpeed = speed;
      setSpeed(0);   
    }
    else{
      setSpeed(-oldSpeed);
      oldSpeed  = -oldSpeed;
    }
    ok = !ok;
  }

}

void loop(){
  if (Serial.available()){
    setSpeed(Serial.parseInt());
    Serial.print("Speed: ");
    Serial.println(speed);
    Serial.print("Delay: ");
    Serial.println(d);
  }
  runForD();
}
/*
 int *pwmPinVec[] =  {&enA,&enB};

void setupPWM(){
  const int freq = 10000;
  const int resolution = 4;
  for (int i = 0; i<2;i++){
    ledcSetup(i, freq, resolution);
    ledcAttachPin(*pwmPinVec[i], i);
  }
}
void setup() {
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up!"); 
  // set all the motor control pins to outputs 
  pinMode(enA, OUTPUT); 
  pinMode(enB, OUTPUT); 
  
  pinMode(in1, OUTPUT); 
  pinMode(in2, OUTPUT); 
  pinMode(in3, OUTPUT); 
  pinMode(in4, OUTPUT); 
  setupPWM();
} 

void setSpeed(int s){
  speed = s;
  d =   2000*11/s;
}

void demoOne() { 
  // this function will run the motors in both directions at a fixed speed 
  // turn on motor A 
  Serial.println("Demo 1");
  digitalWrite(in1, HIGH); 
  digitalWrite(in2, LOW);  
  
  digitalWrite(in3, HIGH); 
  digitalWrite(in4, LOW); 
  
  ledcWrite(0, speed);
  ledcWrite(1, speed);
  delay(d); 
  // now turn off motors 
  digitalWrite(in1, LOW); 
  digitalWrite(in2, LOW);   
  digitalWrite(in3, LOW); 
  digitalWrite(in4, LOW); 
  delay(d);
   
   // now change motor directions 
  digitalWrite(in1, LOW); 
  digitalWrite(in2, HIGH);   
  digitalWrite(in3, LOW); 
  digitalWrite(in4, HIGH);  
  ledcWrite(0, speed);
  ledcWrite(1, speed);
  delay(d); 
  
  // now turn off motors 
  digitalWrite(in1, LOW); 
  digitalWrite(in2, LOW);   
  digitalWrite(in3, LOW); 
  digitalWrite(in4, LOW); 
} 
void loop() { 
  if (Serial.available()){
    setSpeed(Serial.parseInt());
    Serial.print("Speed: ");
    Serial.println(speed);
    Serial.print("Delay: ");
    Serial.println(d);
  }
  demoOne(); 
  delay(d); 
}
*/
/*
void demoTwo() { 
  // this function will run the motors across the range of possible speeds 
  // note that maximum speed is determined by the motor itself and the operating voltage 
  // the PWM values sent by analogWrite() are fractions of the maximum speed possible  
  // by your hardware 
  // turn on motors 
  Serial.println("Demo 2");
  digitalWrite(in1, LOW); 
  digitalWrite(in2, HIGH);   
  digitalWrite(in3, LOW); 
  digitalWrite(in4, HIGH);  
  // accelerate from zero to maximum speed 
  for (int i = 0; i < 256; i++) { 
    //analogWrite(enA, i); 
    ledcWrite(0, i);
    //analogWrite(enB, i); 
    ledcWrite(1,i);
    delay(50); 
  }  
  // decelerate from maximum speed to zero 
  for (int i = 255; i >= 0; --i) 
  { 
    //analogWrite(enA, i); 
    ledcWrite(0, i);
    //analogWrite(enB, i); 
    ledcWrite(1,i);
    delay(50); 
  }  
  // now turn off motors 
  digitalWrite(in1, LOW); 
  digitalWrite(in2, LOW);   
  digitalWrite(in3, LOW); 
  digitalWrite(in4, LOW);   
} 
*/
