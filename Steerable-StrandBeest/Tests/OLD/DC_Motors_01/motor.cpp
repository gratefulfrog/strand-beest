 #include "motor.h"
 
 // abstract motor class

void BMotor::update(){
  _update();
}
void BMotor::setSpeed(int s){
  _setSpeed(s);
}

int DCMotor::nextIndex = 0;
int DCMotor::getIndex(){
  return DCMotor::nextIndex++;
}
                 
void DCMotor::_update(){}
void DCMotor::_setSpeed(int s){
  if (!s){
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
  }
  else if (s>0){
    digitalWrite(in1, HIGH); 
    digitalWrite(in2, LOW);  
  }
  else{
    digitalWrite(in2, HIGH); 
    digitalWrite(in1, LOW);  
  }
  ledcWrite(pwmIndex, min((int)(pow(2,pwmResolution+1)-1),abs(s)));
}

DCMotor::DCMotor(int ePin, int i1Pin, int i2Pin):
  en(ePin),
  in1(i1Pin),
  in2(i2Pin),
  pwmIndex(DCMotor::getIndex()){
    pinMode(en, OUTPUT);
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
    ledcSetup(pwmIndex, pwmFrequency, pwmResolution);
    ledcAttachPin(en, pwmIndex);
  }
