/*
 * create a SPDT switch using one pin and 2 transistors
 * 
 * R1 = R2 = R3 = R4 = 1K
 * 
 * 5V -> LED1 -> R1 -> Transistor1_Collector;
 * 5V -> R2 -> Transistor1_Base;
 * pin -> Transistor1_Emitter;
 *  
 * 5V -> LED2 -> R3 -> Transistor2_Collector;
 * pin (Transistor1_Emitter) -> R4 -> Transistor2_Base;
 * Transistor2_Emitter -> GND;
 * 
 * if pin == 1, there is 5V at the Transistor 1 emitter, it is switched off
 * but then transistor2 is switched on!
 * 
 * if pin == 0, then current flows transistor 1 base to emitter, it is on
 * but no current flows in transistor2, it is off!
 * 
 */


int p = 32;



void setup() {
  pinMode(p,OUTPUT);
  digitalWrite(p,1);
}

void loop() {
  digitalWrite(p,!digitalRead(p));
  delay(500);
  // put your main code here, to run repeatedly:

}
