#include "WiFi.h"
 
void setup(){
  Serial.begin(115200);
  WiFi.mode(WIFI_MODE_STA);
  Serial.println(WiFi.macAddress());
}
 
void loop(){

}
/*
 * ESP32_A
 * 24:62:AB:F6:1E:64
 * ESP32_B
 * F0:08:D1:D1:C7:CC
 * ESP32_C
 * 24:0A:C4:59:BA:34
 * ESP32_D
 * AC:67:B2:3D:5F:50
 * ESP32_E
 * AC:67:B2:3D:52:30
 * ESP32_F
 * AC:67:B2:50:B3:F8
 * ESP32_G
 * AC:67:B2:50:7C:EC
 * ESP32_H
 * 94:B9:7E:C2:EF:98
 * ESP32_I
 * 94:B9:7E:C0:C8:38
 * ESP32_J
 * 94:B9:7E:D2:F3:8C
 */
