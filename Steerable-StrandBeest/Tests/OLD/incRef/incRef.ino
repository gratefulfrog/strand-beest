void setup() {
  Serial.begin(115200);
  Serial.println("starting up!");
}
void incWithDirection(uint8_t &index,int dir,uint8_t nb){
  int val = dir>=0 ? 1 : -1;
  if (val <0){
    index = index? index + val
                 : nb    + val;
  }
  else{ 
    index =  index+val == nb ? 0
                             : index+val;
  }
}

void loop() {
  static uint8_t index = 0;

  incWithDirection(index,1,10);
  Serial.println(index);
  delay(500);
}
