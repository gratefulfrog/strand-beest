// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// Released under the GPLv3 license to match the rest of the
// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        (26)
#define POTPIN     (27)

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 20 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 20 // Time (in milliseconds) to pause between pixels

void setup() {
  pinMode(POTPIN,INPUT);
  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.setBrightness(20);
  pixels.clear(); // Set all pixel colors to 'off'
}

void loop() {
  for(int i=0; i<NUMPIXELS; i++) { 
    uint8_t potVal = map(analogRead(POTPIN),0,4095,1,255),
            inversePotVal = 256-potVal,
            avg           = (potVal+inversePotVal)/2.0;
    pixels.setPixelColor(i, pixels.Color(potVal,inversePotVal,avg));
    pixels.show();
    delay(DELAYVAL); 
  }
  for(int i=0; i<NUMPIXELS; i++) { 
    uint8_t potVal = map(analogRead(POTPIN),0,4095,1,255),
            inversePotVal = 256-potVal,
            avg           = (potVal+inversePotVal)/2.0;
    pixels.setPixelColor(i, pixels.Color(avg,potVal,inversePotVal));
    pixels.show(); 
    delay(DELAYVAL);
  }
  for(int i=0; i<NUMPIXELS; i++) {
    uint8_t potVal = map(analogRead(POTPIN),0,4095,1,255),
            inversePotVal = 256-potVal,
            avg           = (potVal+inversePotVal)/2.0; 
    pixels.setPixelColor(i, pixels.Color(inversePotVal,avg,potVal));
    pixels.show(); 
    delay(DELAYVAL); 
  } 
}
