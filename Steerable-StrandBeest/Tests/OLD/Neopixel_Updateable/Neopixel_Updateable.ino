
#include "updateable.h"

Updateable *u,*v,*w;
String uname = "Frank_09",
       vname = "Bill_10",
       wname = "Sally_00";

void setup(){
  Serial.begin(115200);
  u = new UTester(1000,uname);
  u->setDutyCycle(0.9);
  v = new UTester(1000,vname);
  v->setDutyCycle(1.);
  w = new UTester(1000,wname);
  w->setDutyCycle(0);
  Serial.println("Starting up...");
}
void loop(){
  Updateable::updateAll();
}

/*

#include <Adafruit_NeoPixel.h>

const int potPin      = 27;
const int neoPixelPin = 26;

const int buttonPinVec[] = {12,14,25,33}; //RR RF

int potValue = 0;

Adafruit_NeoPixel pixels(8, neoPixelPin, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(115200);
  pinMode(potPin,INPUT);
  pixels.begin(); 
  pixels.setBrightness(10);
  for (int i=0;i<4;i++){
    pinMode(buttonPinVec[i],INPUT_PULLUP);
  }
  Serial.println("Starting up...");
}
void showDir(int rl,int val){ //r =0, l=1, -1 = red, 0 = blue, 1 = Green
  int startPos = 6*rl,
      endPos   = startPos+2;
  uint32_t color =(val < 0 ? 0xFF0000 // red
                           : val > 0 ? 0x00FF00  // green
                                     : 0X0000FF);  // blue
  for (int i = startPos;i<endPos;i++){
    pixels.setPixelColor(i,color);
  }
}

void doButtons(){
  if(!digitalRead(buttonPinVec[0])){
    showDir(0,-1);
  }
  else if (!digitalRead(buttonPinVec[1])){
    showDir(0,1);
  }
  else{
    showDir(0,0);
  }
  if(!digitalRead(buttonPinVec[2])){
    showDir(1,-1);
  }
  else if (!digitalRead(buttonPinVec[3])){
    showDir(1,1);
  }
  else{
    showDir(1,0);
  }
}
int getNbOn(uint8_t potVal){
  int limVec[] = {0,61,122,183,260};
  for (int i=1;i<5;i++){    
    if (potVal >= limVec[i-1] && 
        potVal < limVec[i]){
      return i;
    }
  }
}
void loop() {
  static unsigned long lastOn= 0;
  static bool on = true;
  uint8_t potValue = map(analogRead(potPin),0,4095,1,255);
  unsigned long flashDelay = (254950 -950*potValue)/254,
                now = millis();
  Serial.println(String(potValue) + " : " + String(flashDelay) + " : " + String(getNbOn(potValue)));
  if (now - lastOn > flashDelay/2){
    lastOn = now;
    on = !on;
    if (!on){
      pixels.clear(); // Set all pixel colors to 'off'
    }
    else{
      int lim = getNbOn(potValue);
      for(int i=0; i<lim; i++) { // For each pixel...
       pixels.setPixelColor(5-i, pixels.Color(255,255,0)); // Color(255-potValue, potValue, 0));   
      }
    }
  }
  doButtons();
  pixels.show();   // Send the updated pixel colors to the hardware.
}

*/
