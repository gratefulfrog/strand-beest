
#ifndef NEOPLIXELMGR_H
#define NEOPLIXELMGR_H
/*
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "updateable.h"
#include "config.h"

class NeoPixelMgr{
  public:
    const static uint32_t black = 0x0,
                          red   = 0xFF0000,
                          green = 0x00FF00,
                          blue  = 0x0000FF,
                          white = 0xFFFFFF;

  protected:
    static const int _defaultBrightness =  DEFAULT_BRIGHTNESS;

  public:
    Adafruit_NeoPixel *NeoPixels;
    NeoPixelMgr(int pin, int nbPixels = NB_PIXELS, byte brightness = DEFAULT_BRIGHTNESS);
};

class PixelSegment: public Updateable{
  protected:
    const static unsigned long _defaultUpdateDelay = 50,
                               _maxDelay           = 10000;
    const static int _cycleNbOn         =  3,
                     _flashNbOn         =  2;

    static void  _flash(PixelSegment *ps);
    static void  _cycle(PixelSegment *ps);
    
    NeoPixelMgr  &_pixels;
    const int     _startPixel,
                  _nbPixels,
                  _directionInverter;  // 1== no inversion
     int          _direction = 0,
                  _nextStartPixelOffset;
    virtual void _update(long unsigned now); 

  public:
    PixelSegment(NeoPixelMgr &pm, int startPix, int nbPix, bool invertDirection);
    void setDirection(int dir);
    void setLastStartPixel(int newPix = 0);
    void setSpeed(byte newSpeed);
    void clear();
};


class PixelLight: public PixelSegment{
  protected:
    virtual void _update(long unsigned now); 

  public:
    PixelLight(NeoPixelMgr &pm, int startPix);
};

*/
#endif
