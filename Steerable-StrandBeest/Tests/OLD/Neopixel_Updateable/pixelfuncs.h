/* what should they do?
 *  first they need to be initialized, all of them nbPixels
 *  then segments need to be created of size less than or equal to nbPixels, no checking for overlap, 
 *  yes checking for max size,
 *  then they should get one (or more ?) display func
 *  seg.setDisplayFunc(&func)
 *  then they should get updated
 *  seg.update(now)
 *  
 *  0.pixelMgr(pin,nbTotalPixels)
 *  1.pixelSegment(pixelMgr,startPixel,nbPixels <= nbTotalPixels)
 *  - setBrightness(brightness);
 *  - setBrightness(index, brightness)
 *  - clear()
 *  - clear (index)
 *  - setColor(color)
 *  - setColor(index, color)
 *  - setColor(r,g,b)
 *  - setColor(index,r,g,b)
 *  2.pixelFunc(pixelSegment,updatePeriod,dutyCycle=1,minPeriod=50, maxPeriod=1000)  // periods in ms 
 *    abstract class,
 *    friend of pixelSegment, 
 *    inherits from updateable.
 *  - _update(now) // from updateable
 *  - virtual void exec() = 0 // to be defined in concrete classes
 *  - setDutyCycle(newdutyCycle) // float on [0,1.0]
 *  - setSpeed(speedByte)  // conversion to period, 
 *                         // with current duty cycle 
 *                         // (do not move to updateable! since min, max periods are needed)
 *  2.1.blinkPixelFunc(dutyCycle = 0.5)
 *  - virtual void exec()  // concrete func
 *  2.2.nbOnPixelFunc(nbOn)
 *  - lastStartPixelIndex // zero based within segment
 *  - setNbOn(newNbOn)
 *  2.2.1. cyclePixelFunc()
 *  2.2.2.flashPixelFunc()
 */
