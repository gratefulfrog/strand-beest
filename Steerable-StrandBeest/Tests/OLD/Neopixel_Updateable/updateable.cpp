#include "updateable.h"

uCell *Updateable::_updateableLinkedList = NULL;

void Updateable::_newUpdateable(Updateable *newUpdateable){
  if (_updateableLinkedList == NULL){
    _updateableLinkedList = new uCell({newUpdateable,NULL});
    return;
  }
  uCell *current = _updateableLinkedList;
  while (current->next){
    current = current->next;
  }
  current->next = new uCell({newUpdateable,NULL});
}

Updateable::Updateable(long unsigned updatePeriod,float dutyCycle):
  _updatePeriod(updatePeriod){
    setDutyCycle(dutyCycle);
    _newUpdateable(this);
}
Updateable::Updateable(long unsigned updatePeriod){
  setPeriod(updatePeriod);
  _newUpdateable(this);
}
Updateable::Updateable(){
  _newUpdateable(this);
}

void Updateable::update(unsigned long now){
  if(now-_lastUpdateTime>= _currentUpdatePeriod){
    _lastUpdateTime = now;
    if (_nextUpdateActionOn){
      _onFunc(now);
    }
    else{
      _offFunc(now);
    }
    // never turn off if duty cycle is 1.0, never turn on if duty cycle is zero
    _nextUpdateActionOn = _dutyCycle == 1.0 ? true 
                                            : _dutyCycle == 0.0 ? false
                                                                : !_nextUpdateActionOn;
    _setCurrentUpdatePeriod();
  }
}
void Updateable::setPeriod(unsigned long newPeriod){
  _updatePeriod = newPeriod;
  _setCurrentUpdatePeriod();
}

unsigned long Updateable::getPeriod() const{
  return _updatePeriod;
}
void Updateable::setDutyCycle(float newDutyCycle){
  _dutyCycle = newDutyCycle;
  _setCurrentUpdatePeriod();
}
float Updateable::getDutyCycle() const{
  return _dutyCycle;
}

void Updateable::_setCurrentUpdatePeriod(){
  _currentUpdatePeriod = _dutyCycle == 1.0 ? _updatePeriod 
                                           : _dutyCycle == 0.0 ? maxPeriod 
                                                               :  _updatePeriod *  (_nextUpdateActionOn ? (1-_dutyCycle) : _dutyCycle) ;
}

void Updateable::updateAll(){
  uCell *current = _updateableLinkedList;
  unsigned long now = millis();
  while(current!=NULL){
    current->u->update(now);
    current = current->next;
  }
}

void UTester::_onFunc(long unsigned now){
  Serial.println(_name + " is ON  : " + String(now) + " toggle time : " + String(now - _lastChangeTime));
  _lastChangeTime = now;
}
void UTester::_offFunc(long unsigned now){  
  Serial.println(_name + " is OFF : " + String(now) + " toggle time : " + String(now - _lastChangeTime));
  _lastChangeTime = now;
}

UTester::UTester(long unsigned udPeriod,String &name):
  Updateable(udPeriod),_name(name){
    _offFunc(millis());
}
