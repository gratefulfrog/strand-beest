#ifndef UPDATEABLE_H
#define UPDATEABLE_H

#include <Arduino.h>

class Updateable;
struct uCell;

typedef struct uCell {
  Updateable   *u;
  uCell        *next;
} uCell;

class Updateable{
  public:
    static const long unsigned   maxPeriod  = 4294967295; // milliseconds defalut vaule is nearly never on!

  protected:
    static void     _newUpdateable(Updateable *newUpdateable);
    static uCell   *_updateableLinkedList;
    long unsigned   _updatePeriod        = maxPeriod;
    long unsigned   _lastUpdateTime      = 0;       // milliseconds
    float           _dutyCycle           = 0.5;     // on for half the period!
    long unsigned   _currentUpdatePeriod = _updatePeriod * _dutyCycle;
    bool            _nextUpdateActionOn  = true;    // if the next time we get updated we should run the on function!

    virtual void _onFunc(long unsigned now) = 0;
    virtual void _offFunc(long unsigned now) = 0;
    void _setCurrentUpdatePeriod();
  
  public:
    static void updateAll();
    
    Updateable(long unsigned updatePeriod,float dutyCycle);
    Updateable(long unsigned updatePeriod);
    Updateable();
    
    void           update(unsigned long now);
    void           setPeriod(unsigned long newPeriod);
    unsigned long  getPeriod() const;
    void           setDutyCycle(float newdutyCycle);
    float          getDutyCycle() const;
};

class UTester: public Updateable{
  protected:
    unsigned long _lastChangeTime  = 0;
    String &_name;
    virtual void _onFunc(long unsigned now);
    virtual void _offFunc(long unsigned now);
  public:
    UTester(long unsigned udPeriod,String &name);
};

#endif
