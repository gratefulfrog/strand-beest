#ifndef NEOPLIXELMGR_H
#define NEOPLIXELMGR_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#define DEFAULT_BRIGHTNESS  (10)
#define DEFAULT_NB_PIXELS   (16)
#define PIXEL_PIN           (32)

class NeoPixelMgr{
  public:
    const static uint32_t red   = 0xFF0000,
                          green = 0x00FF00,
                          blue  = 0x0000FF;

  protected:
    static const int _defaultBrightness =  DEFAULT_BRIGHTNESS;

  public:
    Adafruit_NeoPixel *NeoPixels;
    NeoPixelMgr(int pin = PIXEL_PIN, int nbPixels = DEFAULT_NB_PIXELS, byte brightness = DEFAULT_BRIGHTNESS);
};

class PixelSegment{
  protected:
    const static unsigned long _defaultUpdateDelay = 50,
                               _maxDelay           = 10000;
    const static int _cycleNbOn         =  3,
                     _flashNbOn         =  2;

    static void  _flash(PixelSegment *ps);
    static void  _cycle(PixelSegment *ps);
    
    NeoPixelMgr  &_pixels;
    const int     _startPixel,
                  _nbPixels,
                  _directionInverter;  // 1== no inversion
    long unsigned _updateDelay,
                  _lastUpdateTime;;
    int           _direction = 0,
                  _nextStartPixelOffset;

  public:
    PixelSegment(NeoPixelMgr &pm, int startPix, int nbPix, bool invertDirection);
    void setDirection(int dir);
    void setLastStartPixel(int newPix = 0);
    void setSpeed(byte newSpeed);
    void clear();
    void update(long unsigned now, bool force = false);
};

#endif
