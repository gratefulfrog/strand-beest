#include "neoPixelMgr.h"

NeoPixelMgr::NeoPixelMgr(int pin, int nbPixels, byte brightness){
  NeoPixels = new Adafruit_NeoPixel(nbPixels, pin, NEO_GRB + NEO_KHZ800);
  NeoPixels->begin();
  NeoPixels->show();
  NeoPixels->setBrightness(brightness);
}
  
void  PixelSegment::_flash(PixelSegment *ps){
  for(int c=ps->_nextStartPixelOffset+ps->_startPixel; c<ps->_startPixel+ps->_nbPixels; c += _flashNbOn ) {
    ps->_pixels.NeoPixels->setPixelColor(c, NeoPixelMgr::blue); 
  }
  ps->_nextStartPixelOffset=(ps->_nextStartPixelOffset+1)%_flashNbOn;
}
void  PixelSegment::_cycle(PixelSegment *ps){   
 
  ps->_nextStartPixelOffset += ps->_direction*ps->_directionInverter;

  if (ps->_nextStartPixelOffset < 0){
    ps->_nextStartPixelOffset  +=  ps->_nbPixels;
  }
  else if (ps->_nextStartPixelOffset >= ps->_nbPixels){
    ps->_nextStartPixelOffset  -=  ps->_nbPixels;
  }
  
  for (int count=0;count<_cycleNbOn; count++){
    int onPix = (ps->_nextStartPixelOffset+count)%(ps->_nbPixels) + ps->_startPixel ;
    ps->_pixels.NeoPixels->setPixelColor(onPix, ps->_direction>0 ? NeoPixelMgr::green : NeoPixelMgr::red);
    Serial.println("turning on: " + String(onPix));
    }
  Serial.println("ps->_nextStartPixelOffset: " + String(ps->_nextStartPixelOffset));
  
  Serial.println("ps->_direction: " + String(ps->_direction));
  Serial.println("ps->_directionInverter: " + String(ps->_directionInverter));
}
    
PixelSegment::PixelSegment(NeoPixelMgr &pm, 
                           int startPix, 
                           int nbPix, 
                           bool invertDirection):
  _pixels(pm),
  _startPixel(startPix),
  _nbPixels(nbPix),
  _directionInverter(invertDirection ? -1 : 1),
  _direction(0),
  _nextStartPixelOffset(0),
  _updateDelay(_defaultUpdateDelay ){    
}
void PixelSegment::setDirection(int dir){  // -1,0,1 reverse, stop, forward
  _direction = dir;
}

void PixelSegment::setSpeed(byte newSpeed){ 
  _updateDelay = (newSpeed == 0 ? _maxDelay : (unsigned long) (_defaultUpdateDelay * 255/(newSpeed)));
}

void PixelSegment::clear(){
  for (int i=0; i<_nbPixels;i++){
    _pixels.NeoPixels->setPixelColor(_startPixel+i,0);
  }
}

void PixelSegment::update(long unsigned now, bool force){
  if (force || (now-_lastUpdateTime > _updateDelay)){
    _lastUpdateTime = now;
    clear();
    if (_direction){
      //Serial.println("Striper cycling");
      PixelSegment::_cycle(this);
    }
    else{
      //Serial.println("Striper flashing");
      PixelSegment::_flash(this);
    }
    _pixels.NeoPixels->show();
  }
}
