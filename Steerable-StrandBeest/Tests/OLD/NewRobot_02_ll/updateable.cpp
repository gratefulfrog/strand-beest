#include "updateable.h"

uCell *Updateable::_updateableLinkedList = NULL;

void Updateable::_newUpdateable(Updateable *newUpdateable){
  if (_updateableLinkedList == NULL){
    _updateableLinkedList = new uCell({newUpdateable,NULL});
    return;
  }
  uCell *current = _updateableLinkedList;
  while (current->next){
    current = current->next;
  }
  current->next = new uCell({newUpdateable,NULL});
}

Updateable::Updateable(long unsigned updatePeriod):_updatePeriod(updatePeriod) {
  _newUpdateable(this);
}

void Updateable::update(unsigned long now){
  if(now-_lastUpdateTime>= _updatePeriod){
    _lastUpdateTime = now;
    _update(now);
  }
}

void Updateable::updateAll(){
  uCell *current = _updateableLinkedList;
  unsigned long now = millis();
  while(current!=NULL){
    current->u->update(now);
    current = current->next;
  }
}

Other::Other(int xx):x(xx){}

void UTester::_update(long unsigned now){
  Serial.println(_name + " is updating");
}

UTester::UTester(long unsigned udPeriod,String &name):
  Other(1),Updateable(udPeriod),_name(name){}
