

/* The ESP32 has four SPi buses, however as of right now only two of
 * them are available to use, HSPI and VSPI. Simply using the SPI API 
 * as illustrated in Arduino examples will use VSPI, leaving HSPI unused.
 * 
 * However if we simply intialise two instance of the SPI class for both
 * of these buses both can be used. However when just using these the Arduino
 * way only will actually be outputting at a time.
 * 
 * Logic analyser capture is in the same folder as this example as
 * "multiple_bus_output.png"
 * 
 * created 30/04/2018 by Alistair Symonds
 */
#include <SPI.h>

// Define ALTERNATE_PINS to use non-standard GPIO pins for SPI bus

#ifdef ALTERNATE_PINS
  #define VSPI_MISO   2
  #define VSPI_MOSI   4
  #define VSPI_SCLK   0
  #define VSPI_SS     33

  #define HSPI_MISO   26
  #define HSPI_MOSI   27
  #define HSPI_SCLK   25
  #define HSPI_SS     32
#else
  #define VSPI_MISO   MISO
  #define VSPI_MOSI   MOSI
  #define VSPI_SCLK   SCK
  #define VSPI_SS     SS

  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15
#endif

static const int spiClk = 1000000; // 1 MHz

//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;
SPIClass * hspi = NULL;

void setup() {
  Serial.begin(115200);
  Serial.println("starting up!");
  //initialise two instances of the SPIClass attached to VSPI and HSPI respectively
  vspi = new SPIClass(VSPI);
  hspi = new SPIClass(HSPI);
  
  //clock miso mosi ss

#ifndef ALTERNATE_PINS
  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();
#else
  //alternatively route through GPIO pins of your choice
  vspi->begin(VSPI_SCLK, VSPI_MISO, VSPI_MOSI, VSPI_SS); //SCLK, MISO, MOSI, SS
#endif

#ifndef ALTERNATE_PINS
  //initialise hspi with default pins
  //SCLK = 14, MISO = 12, MOSI = 13, SS = 15
  hspi->begin();
#else
  //alternatively route through GPIO pins
  hspi->begin(HSPI_SCLK, HSPI_MISO, HSPI_MOSI, HSPI_SS); //SCLK, MISO, MOSI, SS
#endif

  //set up slave select pins as outputs as the Arduino API
  //doesn't handle automatically pulling SS low
  pinMode(VSPI_SS, OUTPUT); //VSPI SS
  pinMode(HSPI_SS, OUTPUT); //HSPI SS

}

const int  off = 0,
             R = 1,
             G = 2,
             B = 3,
        nbLeds = 5;

int base3ValVec[] = {0,0,0,0,0};

void incValVec(){
  const int maxV = 4;
  int carry = 1;
  for (int i=0;i<nbLeds;i++){
    base3ValVec[i] += carry;
    if (base3ValVec[i] == maxV){
      base3ValVec[i] = 0;
      carry = 1;
    }
    else{
      carry = 0;
    }
  }
  
}

uint16_t update2Bytes (){
  uint16_t res = 0;
  for (int i=0;i<nbLeds;i++){
    if (base3ValVec[i]){
     res |= 1<<((3*i)+base3ValVec[i]-1);
    }
  }
  return res;
}

// the loop function runs over and over again until power down or reset
void loop() {
  //use the SPI buses
  vspiCommand();
  //hspiCommand();
  delay(200);
}

void showByteBin(byte b){
  for (int i=7;i>-1;i--){
    Serial.print(b&(1<<i)? 1 : 0);
  }
//  Serial.println();
}
void vspiCommand() {
  const byte allBlues[]  = {0b10010010,
                            0b01001000},  // last is white on end
             allGreens[] = {0b01001001,
                            0b00100100},
             allReds[]   ={0b00100100,
                           0b10010010};
  static byte data0 = 0,
              data1 = 0;

  Serial.print("Val Vec: ");
  for (int i=0;i<nbLeds;i++){
    Serial.print(base3ValVec[i]);
  }
  Serial.println();
             
  uint16_t val = update2Bytes ();
  showByteBin((byte)(val&0xFF));
  Serial.print(" : ");
  showByteBin((byte)(val>>8)&0xFF);
  Serial.println();
  
  //  Serial.println(val,BIN);
  data1 = (val>>8)&0xFF;
  data0 = val&0xFF;
  incValVec();
   
  //use it as you would the regular arduino SPI API
  vspi->beginTransaction(SPISettings(spiClk, LSBFIRST, SPI_MODE0));
  digitalWrite(VSPI_SS, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data0);  
  vspi->transfer(data1);
  digitalWrite(VSPI_SS, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();
  
  /*
  data0++;
  if (!data0){
    data1+=3;
  }
  showByteBin(data0);
  Serial.print(" : ");
  showByteBin(data1);
  Serial.println();
  */
}
/*
void hspiCommand() {
  byte stuff = 0b11001100;
  
  hspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(HSPI_SS, LOW);
  hspi->transfer(stuff);
  digitalWrite(HSPI_SS, HIGH);
  hspi->endTransaction();
}
*/
