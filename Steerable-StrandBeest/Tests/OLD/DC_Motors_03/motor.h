 #ifndef BMOTOR_H
 #define BMOTOR_H

#include <Arduino.h>

// abstract motor class

class BMotor{
  protected:
    int _direction  = 0, // +1 positive speed, -1 negative speed, 0 stopped
        _speedValue = 0; // (set in concrete class to _fast or _slow speed value
    virtual void _runSpeed() const = 0;
    virtual void _setDirection(int dir) = 0; // -1,0,1
    virtual void _toggleSpeedRange() = 0;
  public:
    void runSpeed() const;
    void setDirection(int dir); // -1,0,1
    void toggleSpeedRange();
};

// DC motor class
class DCMotor : public BMotor{
  protected:
    static const int _pwmFrequency   =  10, 
                     _pwmResolution  =   8, 
                     _slowSpeedValue = 100, 
                     _fastSpeedValue = 255; 

    static int _nextIndex;
    static int _getIndex();
                 
    void _toggleSpeedRange();
    void _runSpeed() const;
    void _setDirection(int dir); // -1,0,1

    const int  _en,
               _in1,
               _in2,
               _pwmIndex;

  public:
    DCMotor(int ePin, int i1Pin, int i2Pin);
};
 #endif
