// connect motor controller pins to Arduino digital pins 

#include "motor.h"

int enA = 33; //19; 
int in1 = 25; //18; 
int in2 = 26; //5; 
// motor two  
int in3 = 27; //16; 
int in4 = 14; //17;
int enB = 12; //4; 

BMotor *motL, *motR;

void setup(){
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Starting up!");
  
  motL = new DCMotor(enA,in1,in2);
  motR = new DCMotor(enB,in3,in4);
  /*digitalWrite(in1, HIGH); 
  digitalWrite(in2, LOW);
  ledcWrite(0,11);
  digitalWrite(in3, HIGH); 
  digitalWrite(in4, LOW);
  ledcWrite(1,11);
  */
 // motL->runSpeed();
  
}

void setDirections(int dir){
  motL->setDirection(dir);
  motR->setDirection(dir);
}
void toggleSpeeds(){
  motL->toggleSpeedRange();
  motR->toggleSpeedRange();
}
void runSpeeds(){
  long unsigned now = millis();
  Serial.println("Running speeds");
  while (millis()-now <2000){
    motL->runSpeed();
    motR->runSpeed();
  }
  
  Serial.println("End of Running speeds");
}
void loop(){
  static int dir = 0;
  Serial.println("Setting Direction: " + String(dir));
  setDirections(dir);
  runSpeeds();
  setDirections(0);
  runSpeeds();
  Serial.println("Waiting for your input!");
  while (!Serial.available());    
    int val = Serial.parseInt();
    if (abs(val)>1){
      toggleSpeeds();
      Serial.println("Toggling Speed: ");
    }
    else{
      dir = (val > 0 ? 1 : val < 0 ? -1 : 0);
    }
  
}
