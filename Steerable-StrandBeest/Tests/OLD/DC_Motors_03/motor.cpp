 #include "motor.h"
 
 // abstract motor class
 void BMotor::setDirection(int dir){
  _setDirection(dir);
 }

void BMotor::runSpeed() const{
  _runSpeed();
}
void BMotor::toggleSpeedRange(){
  _toggleSpeedRange();
}


// DC Motor concrete class


int DCMotor::_nextIndex = 0;
int DCMotor::_getIndex(){
  return DCMotor::_nextIndex++;
}
                 
void DCMotor::_toggleSpeedRange(){
  _speedValue = ((_speedValue == _fastSpeedValue) ? _slowSpeedValue 
                                                  : _fastSpeedValue);
  Serial.println("Motor : "  + String(_pwmIndex) + ": Set speedValue to : "  + String(_speedValue));
  /*for (int i=0;i<_nbMotors;i++){
      _stripperVec[i]->toggleCadence();
  }
  */
}

void DCMotor::_setDirection(int dir){
  _direction = ((dir > 0) ? 1 : ((dir < 0) ? -1 : 0));
}

void DCMotor::_runSpeed() const{
  switch (_direction){
    case 0:
      digitalWrite(_in1,LOW);
      digitalWrite(_in2,LOW);
      ledcWrite(_pwmIndex, 0);
      break;
    case -1:
      digitalWrite(_in2, HIGH); 
      digitalWrite(_in1, LOW);
      ledcWrite(_pwmIndex, _speedValue); 
      break;
    case 1:
      digitalWrite(_in1, HIGH); 
      digitalWrite(_in2, LOW); 
      ledcWrite(_pwmIndex, _speedValue);
      break;        
  }
}

DCMotor::DCMotor(int ePin, int i1Pin, int i2Pin):
  _en(ePin),
  _in1(i1Pin),
  _in2(i2Pin),
  _pwmIndex(DCMotor::_getIndex()){
    _speedValue = _slowSpeedValue;
    pinMode(_en, OUTPUT);
    pinMode(_in1, OUTPUT);
    pinMode(_in2, OUTPUT);
    _setDirection(0);
    
    Serial.print(String("Motor: ") + String(_pwmIndex) + String(" : "));
    Serial.println(ledcSetup(_pwmIndex, _pwmFrequency, _pwmResolution));
    ledcAttachPin(_en, _pwmIndex);
  }
