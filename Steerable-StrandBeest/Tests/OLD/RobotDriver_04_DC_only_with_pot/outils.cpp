#include "outils.h"

bool macEquals(const uint8_t macL[],const uint8_t macR[]){
  for (int i=0;i<6;i++){
    if (macL[i] != macR[i]){
      return false;
    }
  }
  return true;
}

void printMac(const uint8_t mac[]){
  for (int i=0;i<5;i++){
    Serial.print(mac[i],HEX);
    Serial.print(":");
  }
  Serial.println(mac[5],HEX);
}
/*
void showPins(){
  Serial.print("(");
  Serial.print(digitalRead(leftForwardPin));
  Serial.print(" ");
  Serial.print(digitalRead(leftReversePin));
  Serial.print(") (");
  Serial.print(digitalRead(rightForwardPin));
  Serial.print(" ");
  Serial.print(digitalRead(rightReversePin));
  Serial.println(")");
}
*/
