#include "app.h"

void Controller::_send(){
  espMgr->sendByte(_outgoing);
}

void Controller::_updateOutgoing(){
  _outgoing = 0;
  
  for (int i=0;i<_nbButtonPins-1;i++){
    byte val = !digitalRead(*_buttonPinVec[i]);
    _outgoing |= (val )<<i;    
    digitalWrite(*_ledPinVec[i],val);
  }
  for (int i =0;i<2;i++){
    digitalWrite(*_ledPinVec[i+4],
                 !(digitalRead(*_ledPinVec[0])||digitalRead(*_ledPinVec[i+1])));
  }
  _doSpeedChange(); 
}

void Controller::_doSpeedChange(){
  static unsigned long lastSpeedUpdate = millis();
  static const unsigned speedDebounce = 50;
  static bool speedChangeOK =  true;
  
  unsigned long now = millis();
  if(now - lastSpeedUpdate < speedDebounce){
    return;
  }
  speedChangeOK |= digitalRead(*_buttonPinVec[_nbButtonPins-1]);
  if (!speedChangeOK){
    return;
  }
  lastSpeedUpdate = now;
  byte val = !digitalRead(*_buttonPinVec[_nbButtonPins-1]);
  if (val){
    speedChangeOK = false;
    _outgoing ^= (1)<<(_nbButtonPins-1);
  }
}

void Controller::_updateSpeedLED(){
  for (int i = _nbLedPins-2;i<_nbLedPins;i++){
    digitalWrite(*_ledPinVec[i], !digitalRead(*_ledPinVec[i]));
  }
}

Controller::Controller(const uint8_t *peer): App(peer){
  for (int i=0;i<_nbButtonPins;i++){
    pinMode(*_buttonPinVec[i],INPUT_PULLUP);
  }
  for (int i=0;i<_nbLedPins;i++){
    pinMode(*_ledPinVec[i],OUTPUT);
  }
  digitalWrite(_fastLedPin,LOW);
  digitalWrite(_slowLedPin,HIGH);
}

void Controller::loop(){
  static byte lastOutgoing = 0;
  _updateOutgoing();
  if (_outgoing != lastOutgoing){
    lastOutgoing = _outgoing;
    _send();
    // update speed led if needed
    if ((_outgoing & 1<<(_nbButtonPins-1)) && 
        (espMgr->sendStatus  == ESP_NOW_SEND_SUCCESS)){
      _updateSpeedLED();
    }
  }
}
