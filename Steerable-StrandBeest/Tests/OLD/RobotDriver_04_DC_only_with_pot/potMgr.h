#ifndef POTMGR_H
#define POTMGR_H

#include <Arduino.h>

class PotMgr{
  protected:
    static const int _adcResolutionBits       = 12,
                     _adcMaxVal               = pow(2,_adcResolutionBits)-1,
                     _outputResolutionBits    = 8,
                     _outputMaxVal            = pow(2,_outputResolutionBits)-1,
                     _outputResolutionDecimal = 10;
    const int _potPin;    
  public:
    PotMgr(int potPin);
    byte getNormedValue() const;
};

#endif
