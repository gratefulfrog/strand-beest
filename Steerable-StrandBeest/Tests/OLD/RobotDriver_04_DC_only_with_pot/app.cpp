#include "app.h"
#include "espMgr.h"

App::App(const uint8_t *peer, Robot *robotPtr){
  Serial.println("in App constructor, init espMgr...");
  espMgr = new ESPMgr(peer,robotPtr);
}

Robot::Robot(const uint8_t *peer) : App(peer,this){}

const int Robot::_nbMotors = 2;

void Robot::_setupNeoPixels(){
  _lStripper = new Stripper(STRIP_PINL);
  _lStripper->setSpeed(0);
  _rStripper = new Stripper(STRIP_PINR);
  _rStripper->setSpeed(0);
  _stripperVec[0] = _rStripper;
  _stripperVec[1] = _lStripper;
}

void Robot::_updateStrippers(){
  for (int i = 0; i< _nbMotors;i++){
    _stripperVec[i]->update();
  }
}

void Robot::updateIncoming(const uint8_t *incomingData){
  _incoming = *incomingData;
}
