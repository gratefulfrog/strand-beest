/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-two-way-communication-esp32/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <esp_now.h>
#include <WiFi.h>

uint8_t ESP32_A_Address[] = {0x24, 0x62, 0xAB, 0xF6, 0x1E, 0x64},
        ESP32_B_Address[] = {0xF0, 0x08, 0xD1, 0xD1, 0xC7, 0xCC},
        myMac[6],
        receiverAddress[6];

// cannot go out of scope, for some reason??
esp_now_peer_info_t peerInfo;

//Structure example to send and receive data
typedef struct struct_message {
    unsigned b;
} struct_message;

struct_message outgoing,
               incoming;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {  
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
  
// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&incoming, incomingData, sizeof(incoming));
  //Serial.print("Bytes received: ");
  //Serial.println(len);
  Serial.print("incoming data: ");
  Serial.println(incoming.b);
}

bool macEquals(uint8_t macL[],uint8_t macR[]){
  for (int i=0;i<6;i++){
    if (macL[i] != macR[i]){
      return false;
    }
  }
  return true;
}

void printMac(uint8_t mac[]){
  for (int i=0;i<5;i++){
    Serial.print(mac[i],HEX);
    Serial.print(":");
  }
  Serial.println(mac[5],HEX);
}

void setupWifi(){
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  
  WiFi.macAddress(myMac);
  
  Serial.print("My MAC: ");
  printMac(myMac);

  if (macEquals(myMac,ESP32_A_Address)){
    memcpy(receiverAddress, ESP32_B_Address, 6);
  }
  else{
    memcpy(receiverAddress, ESP32_A_Address, 6);
  }
  Serial.print("Receiver MAC: ");
  printMac(receiverAddress);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    while(1);
  } 
}

void registerAndAddPeer(){  
  memcpy(peerInfo.peer_addr, receiverAddress,6);
  peerInfo.channel = 0;   
  peerInfo.encrypt = false;

  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    while(1);
  }
}

void setup() {
  // Init Serial Monitor
  Serial.begin(115200);

  setupWifi();

  // register data Send CB to
  esp_now_register_send_cb(OnDataSent);
  
  registerAndAddPeer();
  
  // Register data rcev callback
  esp_now_register_recv_cb(OnDataRecv);

  // init outgoing and incoming
  outgoing.b = 0;
  incoming.b = -1;
}

void updateOutgoing(){
  outgoing.b++;
}

void send(){
  Serial.print("outgoing data: ");
  Serial.println(outgoing.b);
  
  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(receiverAddress, (uint8_t *) &outgoing, sizeof(outgoing));
  Serial.println((result == ESP_OK) ? "Sent with success" : "Error sending the data"); 
}
 
void loop() {
  updateOutgoing();
  send();  
  delay(10);
}
