

// this is just an outline of what we will do

int speedVec[] = {0,1,-1};

void setSpeed(int motorId, int speedIndex){
  // motorVec[motorId].setSpeed(speedVec[speedIndex]*speedValue);
  Serial.println("set motor "    + 
                 String(motorId) + 
                 " speed "       + 
                 String(speedVec[speedIndex]));
}

void processIncoming (byte incoming){
  for (int i=0;i<2;i++){
    setSpeed(i,incoming>>(2*i)&3);
  }
}

void setup(){
  Serial.begin(115200);
  Serial.println("Starting...");
  for (byte i=0;i<3;i++){
    for (byte j=0;j<3;j++){
      byte incoming = i<<2|j;
      Serial.println(incoming);
      processIncoming(incoming);
    }
  }
  Serial.println("Done.");
}
void loop(){}
