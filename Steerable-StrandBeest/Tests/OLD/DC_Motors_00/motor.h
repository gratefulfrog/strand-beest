 #ifndef BMOTOR_H
 #define BMOTOR_H

#include <Arduino.h>

// abstract motor class

class BMotor{
  protected:
    virtual void _update() = 0;
    virtual void _setSpeed(int s) = 0;
  public:
    void update();
    void setSpeed(int s);
};

// DC motor class
class DCMotor : public BMotor{
  protected:
    static const int pwmFrequency  = 10000,
                     pwmResolution = 4;
    static int nextIndex;
    static int getIndex();
                 
    virtual void _update();
    void _setSpeed(int s);

    const int  en,
               in1,
               in2,
               pwmIndex;
  public:
    DCMotor(int ePin, int i1Pin, int i2Pin);
};


 #endif
