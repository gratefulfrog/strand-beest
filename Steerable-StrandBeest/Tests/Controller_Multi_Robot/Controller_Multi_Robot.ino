#include "app.h"

#define CONTROLLER

App *app;

void setup() {
#ifdef CONTROLLER
  app = new ControllerApp(controllerDefs);
#else
  app = new RobotApp(robotDefs);
#endif  
}

void loop() {
  app->loop(); 
} 
