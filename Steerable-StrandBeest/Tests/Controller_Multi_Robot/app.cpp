#include "app.h"

////////////////  DEFINITION STRUCTS /////////////////////////////

robotDefStruct robotDefs{
  ROBOT_PIXELS_PIN,
  ROBOT_NB_PIXELS,
  {ROBOT_MA_0_PIN,
   ROBOT_MA_1_PIN,
   ROBOT_MB_0_PIN,
   ROBOT_MB_1_PIN}
};

controllerDefStruct controllerDefs{
  CONTROLLER_PIXELS_PIN,
  CONTROLLER_NB_PIXELS,
  CONTROLLER_POT_PIN,
  {CONTROLLER_BUTTON_RR_PIN,
   CONTROLLER_BUTTON_RF_PIN,
   CONTROLLER_BUTTON_LR_PIN,
   CONTROLLER_BUTTON_LF_PIN},
  {ESPMgr::ESP32_E_Address,
   ESPMgr::ESP32_D_Address},
   CONTROLLER_NB_ROBOTS
};

////////////////  APP Methods /////////////////////////////
bool App::_updateCurrentMsg() { // return true if something new, this will be the real method for the robot as well!
 if(anyNotEqual(_currentMsg,_lastMsg,_msgLength)){
    for(uint8_t i= 0;i<_msgLength;i++){
      _lastMsg[i] =  _currentMsg[i];
    }
    return true;
  }
  return false;
}

App::App(ESPMgr *espMgr, uint8_t pixelPin, uint8_t nbPixels):
  _espMgr(espMgr),
  _pm(new PixelMgr(pixelPin,nbPixels)){
    Serial.begin(115200);
    Serial.println("starting up...");
}

////////////////  CONTROLLER APP Methods /////////////////////////////

bool ControllerApp::_updateOutgoing(){ 
  _currentMsg[0] = _bm->getByteValue();
  _currentMsg[1] = _sc->getSpeed(); 
  return _updateCurrentMsg();
}  

ControllerApp::ControllerApp(controllerDefStruct &defs):
  App(new ESPMgr(_currentMsg,defs.robotAddressVec,defs.nbRobots),defs.pixelPin,defs.nbPixels){
    for (uint8_t i=0;i<DirectionIndicatorMgr::nbDirectionIndicators;i++){
      _disVec[i] = new PixelSegment(*_pm,
                                    i ? CONTROLLER_NB_PIXELS-i
                                      : 0,
                                      1);
    }
    _dim = new DirectionIndicatorMgr(_disVec);
    _bm  = new ButtonMgr(defs.buttonPinVec);
    _potMgr = new PotMgr(defs.potPin);
    _speedBPS = new UpdateableBlinkerPixelSegment(*_pm,
                                                  1,
                                                  CONTROLLER_NB_PIXELS-2,
                                                  500,
                                                  PixelMgr::yellow,
                                                  1);
    _sc  = new SpeedControl(*_potMgr,*_speedBPS);
    _sendCurrentMsg();
}

void ControllerApp::_sendCurrentMsg() const{
  _espMgr->sendByteVec(_currentMsg,_msgLength);
  Serial.print(_espMgr->name + " TX : ");
  showByteVec(_currentMsg,_msgLength);
  delay(100);
}

void ControllerApp::loop(){
  _sc->update();
  _dim->update(_bm->getByteValue());
  Updateable::updateAll();
  _pm->show();
  if(_updateOutgoing()) {
    _sendCurrentMsg();
  }
}

////////////////  ROBOT APP Methods /////////////////////////////

RobotApp::RobotApp(robotDefStruct &defs):
  App(new ESPMgr(_currentMsg),defs.pixelPin,defs.nbPixels){
    // motor mgr
    _mm    = new MotorMgr(defs.motorPinVec,nbMotors);

    // headlights
    uint8_t headlightIndexVec[] = {0,9,10,defs.nbPixels-1};
    for (uint8_t i=0;i<HeadLightMgr::nbHeadLights;i++){
      _hlsVec[i] = new PixelSegment(*_pm,headlightIndexVec[i],1);
    }
    _hlm = new HeadLightMgr(_hlsVec);

    // Flasher Runners
    uint8_t flasherStartIndexVec[] = {1,11};
    for (uint8_t i=0;i<FRMgr::nbFRs;i++){
      UpdateableTheaterFlasherPixelSegment *fps = new UpdateableTheaterFlasherPixelSegment(*_pm,
                                                                                           flasherStartIndexVec[i],
                                                                                           8,
                                                                                           500,
                                                                                           1,
                                                                                           1,
                                                                                           PixelMgr::blue);
    UpdateableTheaterRunnerPixelSegment *rps = new UpdateableTheaterRunnerPixelSegment(*_pm,
                                                                                       flasherStartIndexVec[i],
                                                                                       8,
                                                                                       500,
                                                                                       3,
                                                                                       i? PixelMgr::green : PixelMgr::green,
                                                                                       i? 1 : -1);
    _frVec[i] = new FlasherRunner(*fps,*rps);
  }
  _frm = new FRMgr(_frVec);
}

void RobotApp::_updateManagers(){
  Serial.print(_espMgr->name + " RX : ");
  showByteVec(_currentMsg,_msgLength);
  _frm->update(_currentMsg);
  _hlm->update(_currentMsg[0]);
  _mm->update(_currentMsg);
}
void RobotApp::loop(){
  if(_updateCurrentMsg()) {
    _updateManagers();
  }
  _mm->runSpeed();
  Updateable::updateAll();
  _pm->show();
}
