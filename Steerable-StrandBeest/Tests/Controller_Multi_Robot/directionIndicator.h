#ifndef DIRECTION_INDICATOR_H
#define DIRECTION_INDICATOR_H

#include <Arduino.h>
#include "pixelMgr.h"

class DirectionIndicator{
  public:
    static const uint32_t  stopColor    = PixelMgr::blue,
                           forwardColor = PixelMgr::green,
                           reverseColor = PixelMgr::red;
    static const uint8_t  nbColors = 3;
  protected:
    const uint32_t _colorVec[nbColors];
    PixelSegment &_ps;

  public:
    DirectionIndicator(PixelSegment &ps);
    void update(uint8_t rawDirectionValue); // where it is 0 for stopped, 1 for reverse, 2 for forward;
};

class DirectionIndicatorMgr{
  public:
    static const uint8_t nbDirectionIndicators = 2;
    
  protected:
    DirectionIndicator *_diVec[nbDirectionIndicators];

  public:
    DirectionIndicatorMgr(PixelSegment *psVec[nbDirectionIndicators]);
    void update(uint8_t rawDirectionByte); //0bJJKK, where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
};

#endif
