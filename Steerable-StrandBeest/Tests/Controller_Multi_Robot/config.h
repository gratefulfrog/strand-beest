#ifndef CONFIG_H
#define CONFIG_H


///////// PINS ETC//////
//// ROBOT PINS ETC //
#define ROBOT_NB_PIXELS     (20)  // 2x 8-pixel strips + 4x headlights
#define ROBOT_NB_MOTOR_PINS (4)
#define ROBOT_MA_E_PIN            // enable pin for 3pin motor drivers
#define ROBOT_MB_E_PIN            // enable pin for 3pin motor drivers
#define ROBOT_MB_1_PIN      (12)
#define ROBOT_MB_0_PIN      (13)
#define ROBOT_PIXELS_PIN    (25)
#define ROBOT_MA_0_PIN      (26)
#define ROBOT_MA_1_PIN      (27)

//// CONTROLLER PINS ETC //
#define CONTROLLER_NB_PIXELS       (8)  // 1x 8-pixel strip
#define CONTROLLER_NB_ROBOTS       (2)   // how many robots are controlled by the controller
#define CONTROLLER_NB_BUTTON_PINS  (4)

#define CONTROLLER_BUTTON_RR_PIN   (14)
#define CONTROLLER_BUTTON_LF_PIN   (25)
#define CONTROLLER_BUTTON_LR_PIN   (26)
#define CONTROLLER_BUTTON_RF_PIN   (27)  // note: external pullup resistor required on pins 34,35,36,39
#define CONTROLLER_PIXELS_PIN      (32)
#define CONTROLLER_POT_PIN         (33)


#endif
