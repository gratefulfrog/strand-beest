#ifndef PIXELMGR_H
#define PIXELMGR_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "updateable.h"

#define DEFAULT_BRIGHTNESS         (255)
#define DEFAULT_MIN_UPDATE_PERIOD  (40) // millisec
#define DEFAULT_MAX_UPDATE_PERIOD  (1000) // millisec

class PixelMgr{
  friend class PixelSegment;
  public:
    const static uint32_t black       = 0x0,
                          red         = 0x0A0000,  // 0x0A == 10 decimal, the default brightness prior to headlights requiring real strong light
                          brightRed   = 0xFF0000,  // full brightness red 
                          green       = 0x000A00,  
                          blue        = 0x00000A,
                          white       = 0x0A0A0A,
                          brightWhite = 0xFFFFFF,  // full brightness white
                          yellow      = 0x0A0A00,
                          purple      = 0x0A000A,
                          turquoise   = 0x000A0A,
                          off         = black;

  protected:
    static const int _defaultBrightness =  DEFAULT_BRIGHTNESS;
    Adafruit_NeoPixel * const _neoPixels;

  public:
    // start with all off, and show them
    PixelMgr(uint8_t pin, 
             uint8_t nbPixels, 
             uint8_t brightness = DEFAULT_BRIGHTNESS);
    void show();
};

class PixelSegment{
  protected:
    const uint8_t _startIndex,
                  _endIndexPlusOne,
                  _nbPixels;
    PixelMgr      &_pixelMgr;  
    bool          _active = true;
    
  public:
    PixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels); // check that nbPixl is <= total pixesl managed
    void setBrightness(uint8_t brightness);
    virtual void clear();
    virtual void clear (uint8_t index);
    void setColor(uint32_t color);
    void setColor(uint8_t index,uint32_t color);
    void setColor(uint8_t r,uint8_t g,uint8_t b);
    void setColor(uint8_t index,uint8_t r,uint8_t g,uint8_t b);
    uint8_t getNbPixels() const;
    void activate(bool newVal) ;
};

class UpdateablePixelSegment:public Updateable, public PixelSegment{
  // abstract class placehoder
  public:
    static const unsigned long maxUpdatePeriod = DEFAULT_MAX_UPDATE_PERIOD,
                               minUpdatePeriod = DEFAULT_MIN_UPDATE_PERIOD;
  public:
    UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels);  
    UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels,long unsigned udPeriod);
    UpdateablePixelSegment(PixelMgr &pm,uint8_t startPixel,uint8_t nbPixels,
                           long unsigned udPeriod, float dutyCycle);
    virtual void setPeriod(long unsigned newPeriod);
};

class UpdateableBlinkerPixelSegment:public UpdateablePixelSegment{
  // general blinking pixels class
  protected:
    virtual void _onFunc(long unsigned now);
    virtual void _offFunc(long unsigned now);
    uint32_t _onColor,
             _offColor,
             _nbOn;
    int      _direction;
               
  public:
    UpdateableBlinkerPixelSegment(PixelMgr &pm,
                                  uint8_t startPixel,
                                  uint8_t nbPixels,
                                  long unsigned udPeriod,
                                  uint32_t onColor,
                                  uint8_t  nbOn,
                                  int direction = -1, // 1 is left to right from start pixel; -1 is right to left  from end pixel
                                  uint32_t offColor = PixelMgr::off);
    void setOnColor(uint32_t col);
    void setNbOn(uint8_t nb);
    uint8_t getNbOn() const {return _nbOn;}
    void setOffColor(uint32_t col);
    virtual void clear();
    virtual void clear (uint8_t index);
};

class UpdateableTheaterFlasherPixelSegment:public UpdateableBlinkerPixelSegment{
  protected:
    virtual void _onFunc(long unsigned now);
    virtual void _offFunc(long unsigned now);
    uint8_t _nbOff;
  
  public:
    UpdateableTheaterFlasherPixelSegment( PixelMgr &pm,
                                          uint8_t startPixel,
                                          uint8_t nbPixels,
                                          long unsigned udPeriod,
                                          uint8_t nbOn,
                                          uint8_t nbOff,
                                          uint32_t onColor,
                                          uint32_t offColor = PixelMgr::off);
    void setNbOff(uint8_t nb);
    uint8_t getNbOff() const{ return _nbOff;}
};

class UpdateableTheaterRunnerPixelSegment:public UpdateableTheaterFlasherPixelSegment{
  protected:
    virtual void _onFunc(long unsigned now);
    virtual void _offFunc(long unsigned now);
    uint8_t _firstOn;
    int     _direction;
  
  public:
    UpdateableTheaterRunnerPixelSegment(PixelMgr &pm,
                                        uint8_t startPixel,
                                        uint8_t nbPixels,
                                        long unsigned udPeriod,
                                        uint8_t nbOn,
                                        uint32_t onColor,
                                        int direction = 1, // 1 is left to right ; -1 is right to left
                                        uint32_t offColor = PixelMgr::off);

    void setDirection(int newDirection);    
    int getDirection() const {return _direction;};                                    
};

#endif
