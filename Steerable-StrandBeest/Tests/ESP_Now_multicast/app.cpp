#include "app.h"

////////////////  APP Methods /////////////////////////////
bool App::_updateIncoming() { // return true if something new, this will be the real method for the robot as well!
 if(anyNotEqual(_currentMsg,_lastMsg,_msgLength)){
    for(uint8_t i= 0;i<_msgLength;i++){
      _lastMsg[i] =  _currentMsg[i];
    }
    return true;
  }
  return false;
}

App::App(ESPMgr *espMgr):
  _espMgr(espMgr){
    Serial.begin(115200);
    Serial.println("starting up...");
}

////////////////  SENDER APP Methods /////////////////////////////
/*
bool SenderApp::_updateOutgoing(){  // for testing only
  static uint8_t incIndex = 1;
  if (!_currentMsg[incIndex])
    _currentMsg[incIndex]++;
  else 
    _currentMsg[incIndex]<<= 1;
  if (!_currentMsg[incIndex]){
    incIndex = (incIndex+1)%_msgLength;
    _updateOutgoing();
  }
  return true;
}  
*/
bool SenderApp::_updateOutgoing(){  // for testing only
  static uint8_t incIndex = 1;
  _currentMsg[0] = 0B1010; 
  _currentMsg[1] = 255;
  return true; 
  
  if (!_currentMsg[1])
    _currentMsg[1]++;
  else 
    _currentMsg[1]<<= 1;
  return true;
}  


SenderApp::SenderApp(const uint8_t **peerAddressLis, uint8_t nbPeers):
  App(new ESPMgr(_currentMsg,peerAddressLis,nbPeers)){}

void SenderApp::loop(){
  if(_updateOutgoing()) {
    _espMgr->sendByteVec(_currentMsg,_msgLength);
    Serial.print(_espMgr->name + " TX : ");
    showByteVec(_currentMsg,_msgLength);
    delay(1000);
  }
}

////////////////  RECEIVER APP Methods /////////////////////////////

ReceiverApp::ReceiverApp():
  App(new ESPMgr(_currentMsg)){}

void ReceiverApp::loop(){
  if(_updateIncoming()) {
    Serial.print(_espMgr->name + " RX : ");
    showByteVec(_currentMsg,_msgLength);
  }
}
