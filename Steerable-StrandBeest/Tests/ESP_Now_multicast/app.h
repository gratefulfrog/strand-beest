#ifndef APP_H
#define APP_H

#include "espMgr.h"
#include "outils.h"

class App{
  protected:
    static const uint8_t _msgLength = 2;
    uint8_t              _lastMsg[_msgLength]    = {0,0},
                         _currentMsg[_msgLength] = {0,0};
    virtual bool _updateIncoming();  // return true if something new
    virtual bool _updateOutgoing() = 0;  // return true if something new
    ESPMgr *_espMgr;
  public:
    App(ESPMgr *espMgr);
    virtual void loop() = 0;
};

class SenderApp: public App{
  protected:
    virtual bool _updateOutgoing();
  public:
    SenderApp(const uint8_t **peerAddressLis, uint8_t nbPeers);
    void loop();
};

class ReceiverApp: public App{
  protected:
    virtual bool _updateOutgoing(){return false;}
  public:
    ReceiverApp();
    void loop();
};

#endif;
