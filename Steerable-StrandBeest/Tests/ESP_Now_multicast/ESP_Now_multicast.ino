#include "app.h"

#define SENDER

const uint8_t *receivers[] = {ESPMgr::ESP32_B_Address,
                              ESPMgr::ESP32_G_Address},
              nbReceivers = 2;

App *app;

void setup() {
#ifdef SENDER
  app = new SenderApp(receivers,nbReceivers);
#else
  app = new ReceiverApp();
#endif
}

void loop() {
  app->loop(); 
}
