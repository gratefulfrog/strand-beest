#include "outils.h"

bool macEquals(const uint8_t macL[],const uint8_t macR[]){
  for (int i=0;i<6;i++){
    if (macL[i] != macR[i]){
      return false;
    }
  }
  return true;
}

void printMac(const uint8_t mac[]){
  for (int i=0;i<5;i++){
    Serial.print(mac[i],HEX);
    Serial.print(":");
  }
  Serial.println(mac[5],HEX);
}

String byte2BinString(uint8_t b){
  String res = "";
  for (uint8_t i=0;i<8;i++){
    res+= (b>>(7-i))&0b1;
  }
  return res;
}

void showByteVec(const uint8_t *vec, uint8_t len, bool carReturn){
  for(uint8_t i=0;i<len;i++){
    Serial.print(byte2BinString(vec[i]) + "  ");
  }
  if(carReturn) Serial.println();
}

bool equal(uint8_t l, uint8_t u){ return l==u;}
bool notEqual(uint8_t l, uint8_t u){ return l!=u;}

bool any(const uint8_t *v0, const uint8_t *v1, uint8_t nb, byte2byte func){
  for(uint8_t i=0;i<nb;i++){
    if ((*func)(v0[i],v1[i])) return true;
  }
  return false;
}
bool every(const uint8_t *v0, const uint8_t *v1, uint8_t nb, byte2byte func){
  for(uint8_t i=0;i<nb;i++){
    if (!(*func)(v0[i],v1[i])) return false;
  }
  return true;
}
bool anyEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb){
  return any(v0,v1, nb, equal);
}
bool anyNotEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb){
  return any(v0,v1, nb, notEqual);
}
bool everyNotEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb){
  return every(v0,v1, nb, notEqual);
}
bool everyEqual(const uint8_t *v0, const uint8_t *v1, uint8_t nb){
  return every(v0,v1, nb, equal);
}
void testAny(){  
  const uint8_t v[] = {0,1,2,3,4},
                s[] = {0,0,0,0,0};
  Serial.println(anyEqual(v,s,5));
  Serial.println(anyEqual(v,v,5));
  Serial.println(everyEqual(v,s,5));
  Serial.println(everyEqual(v,v,5));
  
  Serial.println(anyNotEqual(v,s,5));
  Serial.println(anyNotEqual(v,v,5));
  Serial.println(everyNotEqual(v,s,5));
  Serial.println(everyNotEqual(v,v,5));  
}
