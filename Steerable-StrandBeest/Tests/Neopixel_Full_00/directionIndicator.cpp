#include "directionIndicator.h"

DirectionIndicator::DirectionIndicator(PixelSegment &ps):
  _ps(ps),
  _colorVec({stopColor, reverseColor, forwardColor}){
}
                           
void DirectionIndicator::update(uint8_t rawDirectionValue){ // where it is 0 for stopped, 1 for reverse, 2 for forward;
  _ps.setColor(_colorVec[rawDirectionValue]);
}

DirectionIndicatorMgr::DirectionIndicatorMgr(PixelSegment *psVec[nbDirectionIndicators]){
  for (uint8_t i = 0; i< nbDirectionIndicators;i++){
    _diVec[i] = new DirectionIndicator(*psVec[i]);
  }
}
void DirectionIndicatorMgr::update(uint8_t rawDirectionByte){
  for (uint8_t i = 0; i< nbDirectionIndicators;i++){
    _diVec[i]->update((rawDirectionByte>>(i*2))&0b11);
  }
}
