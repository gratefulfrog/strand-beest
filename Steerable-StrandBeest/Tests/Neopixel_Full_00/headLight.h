#ifndef HEADLIGHT_H
#define HEADLIGHT_H

#include <Arduino.h>
#include "pixelMgr.h"

class HeadLight{
  public:
    static const uint32_t  stopColor    = PixelMgr::off,
                           forwardColor = PixelMgr::brightWhite,
                           reverseColor = PixelMgr::brightRed;
    static const uint8_t  nbColors = 3;
  protected:
    const uint32_t _colorVec[nbColors];
    PixelSegment &_ps;

  public:
    HeadLight(PixelSegment &ps,bool isFront);
    void update(uint8_t rawDirectionValue); // where it is 0 for stopped, 1 for reverse, 2 for forward;
};

class HeadLightMgr{
  public:
    static const uint8_t nbHeadLights = 4;
    
  protected:
    HeadLight *_hlVec[nbHeadLights];

  public:
    HeadLightMgr(PixelSegment *psVec[nbHeadLights]);
    void update(uint8_t rawDirectionByte); //0bJJKK, where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
};

#endif
