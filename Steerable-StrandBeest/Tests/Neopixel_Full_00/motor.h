 #ifndef BMOTOR_H
 #define BMOTOR_H

#include <Arduino.h>

// abstract motor class
class DCMotor{
  protected:
    static const uint8_t _pwmFrequency   =  10, 
                         _pwmResolution  =   8;

    static uint8_t _nextPWMChannelIndex;
    static uint8_t _getPWMChannelIndex();
    const uint8_t _pinVec[2];

    uint8_t _direction  = 0, // 2 positive speed, (1) negative speed, 0 stopped
            _speedValue = 0; // this is the pwm duty cycle

  public:
    virtual void runSpeed() const = 0;
    void setDirection(uint8_t dir); // 0,1,2 = stopped reverse forward
    void setSpeed(uint8_t newSpeed);
    DCMotor(uint8_t iPin0, uint8_t iPin1);
};

// DC motor concrete class
class DCMotor3Pin : public DCMotor{
 public:
    static const uint8_t nbPins = 3;
    
 protected:
    const uint8_t  _pwmIndex;
    const uint8_t  _enablePin;
    
  public:
    virtual void runSpeed() const;
    DCMotor3Pin(uint8_t ePin,uint8_t i1Pin, uint8_t i2Pin);
};

// DC motor concrete class
class DCMotor2Pin : public DCMotor{
 public:
    static const uint8_t  _nbPWMChannels = 2;
    static const int nbPins = 2;

 protected:   
    const uint8_t  _pwmIndex[_nbPWMChannels];
       
  public:
    virtual void runSpeed() const;             
    DCMotor2Pin(int i1Pin, int i2Pin);
};

class MotorMgr{
  public:
    static const uint8_t  nbMotors = 2;

  protected:
    DCMotor *_motorVec[nbMotors];
  public:
    MotorMgr(DCMotor *rightMotor, DCMotor *leftMotor);
    MotorMgr(const uint8_t pinVec[], uint8_t nbPins);  //R1,R2,L1,L2, or //RE, R1, R2, LE, L1,L2
    MotorMgr(uint8_t pinR1,uint8_t pinR2,uint8_t pinL1,uint8_t pinL2);   // 2 pin motor
    MotorMgr(uint8_t pinRE, uint8_t pinR1,uint8_t pinR2,uint8_t pinLE,uint8_t pinL1,uint8_t pinL2);   // 3 pin motor
    
    void setSpeed(uint8_t speed);
    void setDirections(uint8_t directionByte); // 0bLLRR where LL and RR are 00, 01, 10 for stopped reverse forward 
    void runSpeed(); 
    void update(uint8_t *dirSpeedVec); //{0bJJKK, speedByte} where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
};

#endif
