#include "speedControl.h"

SpeedControl::SpeedControl(PotMgr &potMgr, UpdateableBlinkerPixelSegment &ubps):
  _potMgr(potMgr),
  _ubps(ubps){
}
void SpeedControl::update(){  // does not call show()!
  unsigned long newPeriod = map(_potMgr.getReading(),_potMgr.maxOutputVal,0,_ubps.minUpdatePeriod,_ubps.maxUpdatePeriod);
  _ubps.setPeriod(newPeriod);
  _ubps.setNbOn(map(newPeriod,_ubps.maxUpdatePeriod,_ubps.minUpdatePeriod,1,_ubps.getNbPixels()));
}
