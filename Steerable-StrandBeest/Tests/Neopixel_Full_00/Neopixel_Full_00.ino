#include "pixelMgr.h"
#include "potMgr.h"
#include "rfButtonMgr.h"
#include "speedControl.h"
#include "headLight.h"
#include "directionIndicator.h"
#include "motor.h"
#include "flasherRunner.h"


MotorMgr              *mm;
HeadLightMgr          *hlm;                   
DirectionIndicatorMgr *dim;
ButtonMgr             *bm;
SpeedControl          *sc;
FRMgr                 *frm;
PotMgr                *potM;

PixelMgr *pm;

PixelSegment  *hlsVec[4];
PixelSegment  *disVec[2];
FlasherRunner *frVec[2];

const uint8_t buttonPinVec[] = {32,35,/*12,14,*/25,33}; // note that external pull up resisters are required on pins 34,35,36,39
const uint8_t motorPinVec[] = {27,14, 13,12} ; 
const uint8_t potPin      = 34;
const uint8_t pixelPin    = 26;
const uint8_t nbPixels    = 28;


void setup(){
  Serial.begin(115200);
  Serial.println("starting up...");
  mm    = new MotorMgr(motorPinVec,2);
  bm = new ButtonMgr(buttonPinVec);  
  potM = new PotMgr(potPin);
  
  pm = new PixelMgr(pixelPin,nbPixels);

  // indicators
  for (uint8_t i=0;i<DirectionIndicatorMgr::nbDirectionIndicators;i++){
    disVec[i] = new PixelSegment(*pm,20+i,1);
  }
  dim     = new DirectionIndicatorMgr(disVec);
  
  UpdateableBlinkerPixelSegment *upsT1 = new UpdateableBlinkerPixelSegment(*pm,22,6,500,PixelMgr::yellow,1);
  sc = new SpeedControl(*potM,*upsT1);
  for (uint8_t i=0;i<HeadLightMgr::nbHeadLights;i++){
    hlsVec[i] = new PixelSegment(*pm,19-i,1);
  }
  hlm = new HeadLightMgr(hlsVec);
  
  for (uint8_t i=0;i<FRMgr::nbFRs;i++){
    UpdateableTheaterFlasherPixelSegment *fps = new UpdateableTheaterFlasherPixelSegment(*pm,
                                                                                         0+8*i,
                                                                                         8,
                                                                                         500,
                                                                                         1,
                                                                                         1,
                                                                                         PixelMgr::blue);
    UpdateableTheaterRunnerPixelSegment *rps = new UpdateableTheaterRunnerPixelSegment(*pm,
                                                                                       0+8*i,8,500,3,
                                                                                       i? PixelMgr::green : PixelMgr::green,
                                                                                       i? 1 : -1);
    frVec[i] = new FlasherRunner(*fps,*rps);
  }
  frm = new FRMgr(frVec);
 }

void loop(){
   uint8_t buttonState = bm->getByteValue(),
           potReading  = potM->getReading();
  Serial.print(buttonState,BIN);
  Serial.println(" : " + String(potReading));
  
  sc->update();
  hlm->update(buttonState);
  dim->update(buttonState);
  uint8_t dirspeedVec[] = {buttonState,potReading};
  frm->update(dirspeedVec);
  mm->update(dirspeedVec);
  mm->runSpeed();  
  Updateable::updateAll();
  pm->show();
}
  
