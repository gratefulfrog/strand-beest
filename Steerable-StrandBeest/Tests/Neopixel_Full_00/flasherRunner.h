#ifndef FLASHER_RUNNER_H
#define FLASHER_RUNNER_H

#include <Arduino.h>
#include "pixelMgr.h"

class FlasherRunner{
  public:
    static const uint32_t  stopColor    = PixelMgr::blue,
                           forwardColor = PixelMgr::green,
                           reverseColor = PixelMgr::red;
    static const uint8_t  nbColors = 3;
  protected:
    const uint32_t _colorVec[nbColors];
    UpdateableTheaterFlasherPixelSegment &_utf;
    UpdateableTheaterRunnerPixelSegment  &_utr;

  public:
    void setSpeed(uint8_t speed);
    void setDrection(uint8_t val,uint8_t index);
    FlasherRunner(UpdateableTheaterFlasherPixelSegment &fps,UpdateableTheaterRunnerPixelSegment &rps);
    void update(uint8_t rawDirectionValue, // where it is 0 for stopped, 1 for reverse, 2 for forward;
                uint8_t speedByte);        //     
};  

class FRMgr{
  public:
    static const uint8_t nbFRs = 2;
    
  protected:
    FlasherRunner *_frVec[nbFRs];

  public:
    FRMgr(FlasherRunner *frVec[nbFRs]);  // 
    void update(uint8_t *dirSpeedVec); //{0bJJKK, speedByte} where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
};

#endif
