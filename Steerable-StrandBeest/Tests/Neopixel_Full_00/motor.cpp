 #include "motor.h"

/////////////////////////////////////////////////////////////////////
///////////////////     DCMotor           ////////////////////////////
/////////////////////////////////////////////////////////////////////
 // abstract motor class
 void DCMotor::setDirection(uint8_t dir){
  _direction = dir;
}

void DCMotor::setSpeed(uint8_t newSpeed){
  _speedValue = newSpeed;
}
DCMotor::DCMotor(uint8_t iPin0, uint8_t iPin1):
  _pinVec({iPin0,iPin1}),
  _speedValue(0){
}

uint8_t DCMotor::_nextPWMChannelIndex = 0;

uint8_t DCMotor::_getPWMChannelIndex(){
  return DCMotor::_nextPWMChannelIndex++;
}

/////////////////////////////////////////////////////////////////////
///////////////////     DCMotor3Pin      ////////////////////////////
/////////////////////////////////////////////////////////////////////
void DCMotor3Pin::runSpeed() const{
  digitalWrite(_pinVec[0],LOW);
  digitalWrite(_pinVec[1],LOW);
  if(_direction){
    digitalWrite(_pinVec[(_direction == 1 ? 1 : 0)],HIGH);
    ledcWrite(_pwmIndex, _speedValue);
  }
}

DCMotor3Pin::DCMotor3Pin(uint8_t ePin, uint8_t i1Pin, uint8_t i2Pin):
  DCMotor(i1Pin,i2Pin),
  _enablePin(ePin),
  _pwmIndex(DCMotor::_getPWMChannelIndex()){
  pinMode(_enablePin, OUTPUT);
  pinMode(_pinVec[0], OUTPUT);
  pinMode(_pinVec[1], OUTPUT);
  
  ledcSetup(_pwmIndex, _pwmFrequency, _pwmResolution);
  ledcAttachPin(_enablePin, _pwmIndex);
}


/////////////////////////////////////////////////////////////////////
///////////////////     DCMotor2Pin      ////////////////////////////
/////////////////////////////////////////////////////////////////////
void DCMotor2Pin::runSpeed() const{
  //if (!_direction) return;
  int index = (_direction == 1 ?  1 : (_direction == 2 ? 0 : -1));  
  for (int i=0;i<_nbPWMChannels;i++){
    ledcWrite(_pwmIndex[i],(i==index ? _speedValue : 0));
  }
}

DCMotor2Pin::DCMotor2Pin(int i1Pin, int i2Pin):
  DCMotor(i1Pin,i2Pin),
  _pwmIndex({DCMotor::_getPWMChannelIndex(),DCMotor::_getPWMChannelIndex()}){
    _speedValue = 0;
    pinMode(_pinVec[0], OUTPUT);
    pinMode(_pinVec[1], OUTPUT);
    for (int i =0;i<_nbPWMChannels;i++){
      ledcSetup(_pwmIndex[i], _pwmFrequency, _pwmResolution);
      ledcAttachPin(_pinVec[i], _pwmIndex[i]);
    }
}


/////////////////////////////////////////////////////////////////////
///////////////////     MotorMgr         ////////////////////////////
/////////////////////////////////////////////////////////////////////

MotorMgr::MotorMgr(DCMotor *rightMotor, DCMotor *leftMotor):
  _motorVec({rightMotor, leftMotor}){
}
MotorMgr::MotorMgr(const uint8_t pinVec[], uint8_t nbPins){
  for (uint8_t i = 0; i < nbMotors;i++){
    if(nbPins == 2){
      _motorVec[i] = new DCMotor2Pin(pinVec[nbPins*i],pinVec[nbPins*i+1]);
    }
    else{ // 3pin
      _motorVec[i] = new DCMotor3Pin(pinVec[nbPins*i],pinVec[nbPins*i+1],pinVec[nbPins*i+2]);
    }
  }
}
MotorMgr::MotorMgr(uint8_t pinR1,uint8_t pinR2,uint8_t pinL1,uint8_t pinL2){
  _motorVec[0] = new DCMotor2Pin(pinR1,pinR2);
  _motorVec[1] = new DCMotor2Pin(pinL1,pinL2);
}
MotorMgr::MotorMgr(uint8_t pinRE, uint8_t pinR1,uint8_t pinR2,uint8_t pinLE,uint8_t pinL1,uint8_t pinL2){
  _motorVec[0] = new DCMotor3Pin(pinRE,pinR1,pinR2);
  _motorVec[1] = new DCMotor3Pin(pinLE,pinL1,pinL2);
}
     
void MotorMgr::setSpeed(uint8_t speed){
  for (uint8_t i = 0; i < nbMotors;i++){
    _motorVec[i]->setSpeed(speed);
  }
}

void MotorMgr::setDirections(uint8_t directionByte){
  for (uint8_t i = 0; i < nbMotors;i++){
    _motorVec[i]->setDirection(((directionByte>>(i*2))&0b11));
  }
}

void MotorMgr::runSpeed(){
  for (uint8_t i = 0; i < nbMotors;i++){
    _motorVec[i]->runSpeed();
  }
}

void MotorMgr::update(uint8_t *dirSpeedVec){ //{0bJJKK, speedByte} where JJ is left, and KK is right, and each are 0 ,01 10, for stopped, reverse, forward
  setDirections(dirSpeedVec[0]);
  setSpeed(dirSpeedVec[1]);
}
