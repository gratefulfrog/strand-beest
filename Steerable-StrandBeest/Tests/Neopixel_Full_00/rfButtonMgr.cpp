#include "rfButtonMgr.h"

/////////////////////////////////////////////////////////////////////
///////////////////     RFButtonMgr      ////////////////////////////
/////////////////////////////////////////////////////////////////////
const int RFButtonMgr::_byteIntVec[] = {0,-1,1};
const uint8_t RFButtonMgr::_nbPins = 2;

int RFButtonMgr::toInt(uint8_t rfVal){
  return _byteIntVec[rfVal];
}

void RFButtonMgr::_setupPins() const{
  for(uint8_t i=0;i<_nbPins;i++){
    pinMode(_pinVec[i],INPUT_PULLUP);
  }
}

RFButtonMgr::RFButtonMgr(uint8_t rPin, uint8_t fPin):
  _pinVec({rPin,fPin}){
    _setupPins();
}

RFButtonMgr::RFButtonMgr(uint8_t *pinVec):
  _pinVec({pinVec[0],pinVec[1]}){
    _setupPins();    
}

uint8_t RFButtonMgr::getRFByte() const{ // returns 0bfr where f and r are 1 if button is down
  uint8_t res = 0;
  for(uint8_t i=0;i<_nbPins;i++){
    res |= (!digitalRead(_pinVec[i]))<<i;
  }
  return res;
}

/////////////////////////////////////////////////////////////////////
///////////////////     ButtonMgr      ////////////////////////////
/////////////////////////////////////////////////////////////////////

int ButtonMgr::toInt(uint8_t valByte, bool getLeft){
  return RFButtonMgr::toInt(valByte >> (getLeft*2)&0b11);
}

void ButtonMgr::_setupMgrs(uint8_t pinVec[][2]){
  for (uint8_t i = 0; i<_nbButtonMgrs;i++){
    _bmVec[i] = new RFButtonMgr(pinVec[i]);
  }
}

ButtonMgr::ButtonMgr(uint8_t rrPin,uint8_t  rfPin,uint8_t  lrPin,uint8_t  lfPin){
  uint8_t pVec[][2] = {{rrPin, rfPin}, {lrPin, lfPin}};
  _setupMgrs(pVec);
}
ButtonMgr::ButtonMgr(const uint8_t *pinVec){
  uint8_t pVec[][2] = {{pinVec[0], pinVec[1]}, {pinVec[2], pinVec[3]}};
  _setupMgrs(pVec);
}

uint8_t ButtonMgr::getByteValue() const{
  uint8_t res = 0;
  for (uint8_t i = 0; i<_nbButtonMgrs;i++){
    res |= _bmVec[i]->getRFByte() <<(2*i);
  }
  return res;
}
