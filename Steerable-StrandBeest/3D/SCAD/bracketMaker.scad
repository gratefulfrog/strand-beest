$fn=100;

DXFFileName                 = "../DXF/strandBeestStructure.dxf";
mainBracketLayerName        = "Bracket";
stepperBracketLayerName     = "StepperBracket";
dcMotorBracketLayerName     = "DC Motor Bracket";
dcMotorHolesLayerName       = "DC Motor Holes";
sideHolderLayerName         = "SideHolders";
sideHolderUpperLayerName    = "SideHoldersUpper";
mouseEarsLayerName          = "Mouse Ears";
connectorLayerName          = "Connector";
connectorLowCutLayerName    = "ConnectorLowCut";
connectorHighCutLayerName   = "ConnectorHighCut";
connectorHighCutDCLayerName = "ConnectorHighCutDC";

mainBracketZ       = 6; //8.5;
stepperBracketZ    = 12.5 /*10*/ + mainBracketZ;
dcMotorBracketZ    = 10.5 + mainBracketZ;
sideHoldersUpperZ  =  4;
mouseEarsZ         = 0.25 + 3*0.2;

connectorZ        = stepperBracketZ - 1; // test with minus 1??
connectorLowCutZ  = 6+2;
connectorHighCutZ = 10;

spacerZ             = 24;
spacerSmallInnerDia = 5.5;
spacerBigInnerDia   = 6.1;
spacerOuterDia      = 16.5;
spacerSmallOuterDia = 10;
spacerBarAngle      = 55; //56.5;  // was 58
spacerBarDia        = 4;
spacerZOffset       = 1;

triEpsilon= 2;
traingleLegLength        = 55;
triangleTopAngle         = spacerBarAngle;
triangleLegDia           = spacerBarDia;
triangleRingDia          = 11;
triangleRingthickness    = 2*spacerZOffset;
triangleRingInnerDia     = spacerSmallInnerDia;
triangleRingColumnLength = 20;
triangleSeparation       = spacerZ + triangleRingthickness/2.;// + triEpsilon;

lowerPlateauThickness   = 2;
lowerPlateauZ           = triangleSeparation+triEpsilon;//- triangleLegDia;
lowerPlateauX           = 35 + 5;
lowerPlateauCutaway     = 15;

uppperPlateauThickness = lowerPlateauThickness;
uppperPlateauZ         = triangleSeparation; // 64;
uppperPlateauY         = 0.6*58; //58;

elasticHoleDia         = 3;

pcbX =  96;
pcbY =  84;
pcbZ =   1.8;
mountingHoleY = 13;

///////  USER FUNCTIONS //////////////
// build the top plate and connector with supporting structure 
// and pcb mounting holes
//projection()
upperPlateWithPCB();

// upper plate and PCB demo
//upperPlateWithPCB(true);

// upper plate with no holes
//upperPlateUpright();

// build the lower plateau that clips to the triangle legs
//lowerPlateauUpright();

// build the spacer tube (2 required)
//spacerTube();

// build the 28BYJ-48 mounting bracket
//bracket2BYJ(); // with mouse ears
//dcBracket();   // with mouse ears

// build the connector linkage that links the motor drive shaft
// to the Strandbeest drive shaft
//connector28YJ();
//connectorDC();

////////////////// end of user functions ////////////////////////

module upperPlateWithPCB(withPCB=false){ 
  difference(){
    union(){
      if(withPCB){
      pcb();  
      }
      upperPlateUpright();
    }
    pcbHoleCutter();
  }
}
// upperPlateWithPCB()

module pcb(){
  translate([0,0,pcbZ/2. -10])
    roundedPlateau(pcbX,pcbY,pcbZ,true);
}
//pcb();
//pcbHoleCutter();

module pcbHoleCutter(){
  for (yFactor=[-1,1])
    translate([0,yFactor*mountingHoleY,0])
      cylinder(h=40,d=2.8,center=true);
}

module upperPlateauRaw(){ 
    translate([-cos(0.5*triangleTopAngle)*(triangleRingDia/2.),
               -sin(0.5*triangleTopAngle)*(triangleRingDia/2.),
               0])
    rotate([0,0, 0.5*triangleTopAngle])
      translate([-uppperPlateauThickness+0.2,0,0])
        rotate([0,90,0])
        roundedPlateau(uppperPlateauZ,
                       uppperPlateauY,
                       uppperPlateauThickness,
                       true);
}

module upperPlateauWithSpacer(rot_it = false){
  r1 = rot_it ? -90 : 0;
  r2 = rot_it ? -0.5*triangleTopAngle : 0;
  rotate([0,r1,0])
    rotate([0,0,r2])
      union(){
        upperPlateauRaw();
        spacySpacer();
      }
}
//upperPlateauWithSpacer();

module upperPlate(){
  upperPlateauWithSpacer();
  translate([0,0,-spacerZ/2.])
    legsX2();
}
//upperPlate();

module halfDisk(d,h,c=false){
  r= c ? 90 :0;
  rotate([0,0,r])
    difference(){
      cylinder(d=d,h=h,center=c);
      translate([0,-d/2.,0])
        cube([d,d,h],center=c);
    }
}

module legSupportsHelper(thicknessFactor = 1.){ //1.25){
  for (angle = [0:spacerBarAngle/10:spacerBarAngle])
  //rotate([0,0,spacerBarAngle])
  rotate([0,0,angle])
    rotate([180,-90,0])
      halfDisk(thicknessFactor*triangleLegDia,traingleLegLength/2.3); // 2.0
  //rotate([180,-90,0])
    // halfDisk(thicknessFactor*triangleLegDia,traingleLegLength/2.3); // 2.0
}
/*
difference(){
  upperPlateauWithSpacer();
  translate([0,0,spacerZ/2.+0.1])
    legSupportsHelper();
}
*/
module legSupportsHull(){
  hull(){
    linear_extrude(1)
      projection()
        rotate([0,-90,0])
          rotate([0,0,-0.5*spacerBarAngle])
            legSupportsHelper();
        rotate([0,-90,0])
          rotate([0,0,-0.5*spacerBarAngle])
           legSupportsHelper();
    }
}
//legSupportsHull();
//legSupportsHelper();

module legSupportsHullWithMirror(){
  zz = 5;
  difference(){
    union(){
      legSupportsHull();
      //translate([1,0,0])
      mirror([1,0,0])
        legSupportsHull();
    }
  translate([0,0,-zz/2.])
    cube([5.5,40,zz],center=true);
  }
}
//legSupportsHullWithMirror();

module legSupportsPositive(){
  translate([0,0,spacerZ/2.-0.32])
    rotate([0,0,0.5*spacerBarAngle])
      rotate([0,90,0])
        translate([0,0,-0.5*spacerOuterDia+uppperPlateauThickness])
          //legSupportsHull();
          legSupportsHullWithMirror();
}
//legSupportsPositive();

module legSupports(){
  difference(){
    legSupportsPositive();
    //translate([0,0,-spacerZ/2.])
      //legsX2();
    //cylinder(h=spacerZ+5,d=spacerInnerDia,center=true);
    translate([0,0,spacerZ/2.+1])
      trianglePieCut();//triangle();
      //legSupportsHelper();
  }
}
//legSupports();
//translate([0,0,spacerZ/2.+1])
  //triangle();

module upperPlateauWithSupports(){
  upperPlateauWithSpacer();
  legSupports();
  mirror([0,0,1])
    legSupports();
}

  //upperPlateauWithSpacer();
//upperPlateauWithSupports();

module elasticHole(dia=elasticHoleDia, dOffset = 1){ // 0){
  d = (triangleRingDia + dia)/2.+ dOffset;
  translate([d*cos(spacerBarAngle/2.),
             d*sin(spacerBarAngle/2.),
             0])
    cylinder(h=uppperPlateauZ+2+2, d=dia, center=true);
}

module upperPlateauCutout(){
  difference(){
    upperPlateauWithSupports();
    elasticHole();
    elasticHole(13.5 ,5.5); //11, 9 );
  }
}
//upperPlateauCutout();
//  elasticHole();
//  elasticHole(11 ,7 );
  
module upperPlateUpright(){
  rotate([0,-90,0])
    rotate([0,0,-spacerBarAngle/2.])
      upperPlateauCutout();
}
//upperPlateUpright();

module lpCorner(y,z){
  translate([0,y,z])
    rotate([0,90,0])
      cylinder(h=lowerPlateauThickness,d=1.8*triangleLegDia);
}
//lpCorner();

module lowerPlateauRaw(){
  lpY = 2* lowerPlateauX * sin(triangleTopAngle/2.);
  translate([lowerPlateauX,0,0])
    rotate([0,0,triangleTopAngle/2.])
      union(){
        basePlate(lpY);
        for (y=[0,lpY])
          for (z=[0,lowerPlateauZ-triEpsilon])
            lpCorner(y,z);
      }
}
//lowerPlateauRaw();
//legsX2();

module basePlate(lpY){
  difference(){
    cube([lowerPlateauThickness, 
          lpY,
          lowerPlateauZ]);
    translate([0.5*lowerPlateauThickness,lpY*0.5,0.5*lowerPlateauThickness])
      cube([lowerPlateauThickness,
            lowerPlateauCutaway, 
            lowerPlateauThickness],
           center=true);
    translate([0.5*lowerPlateauThickness,
               lpY*0.5,
               lowerPlateauZ-triEpsilon-0.5*lowerPlateauThickness])
      cube([lowerPlateauThickness,
            lowerPlateauCutaway, 
            lowerPlateauThickness],
           center=true);
  }
}
//basePlate(2* lowerPlateauX * sin(triangleTopAngle/2.));

module lowerPlateau(){
  difference(){
    lowerPlateauRaw();
    legsX2();
    triSlicer(true);
    triSlicer(false);
  }
}
//lowerPlateau();

module lowerPlateauUpright(){
  rotate([0,-90,])
  rotate([0,0,-triangleTopAngle/2.])
    lowerPlateau();
}
//projection()
//lowerPlateauUpright();

module triangleRing(){
  //if(ringIt)
    cylinder(h=triangleRingColumnLength,d=triangleRingInnerDia,center=true);
  cylinder(h=triangleRingthickness,d=triangleRingDia,center=true);
}
//triangleRing();

module triangleLeg(col=true){
  rotate([0,90,0])
    cylinder(h=traingleLegLength,d=triangleLegDia);
  if (col)
    translate([traingleLegLength,0,0])
      triangleRing();
}
//triangleLeg();
module triangle(col=true){
  if (col)
    triangleRing();
  triangleLeg();
  rotate([0,0,triangleTopAngle])
    triangleLeg();
}
//triangle();

module trianglePieCut(angleLimit = triangleTopAngle ){
  triangleRing();
  for(angle = [0:angleLimit/10.:angleLimit])
    rotate([0,0,angle])
      triangleLeg();
}
//translate([0,0,spacerZ/2.])
//trianglePieCut();

module legsX2(){
  triangle();
  translate([0,0,triangleSeparation/*-triEpsilon*/])
    triangle();
}
//legsX2();

module triSlicer(top = false){
  tz = top ? triangleSeparation/*-triEpsilon*/: -traingleLegLength;
  translate([0,-triangleRingDia,tz])
    cube(traingleLegLength);
}
//triSlicer(false);

module faceSlicer(left = false){
  rZ = left ? 0 : triangleTopAngle;
  tY = left ? -2*triangleLegDia : 0;
  rotate([0,0,rZ])
  translate([0,tY,-0.5*triangleRingDia])
  cube([traingleLegLength,
        2*triangleLegDia,
        1.5*triangleSeparation]);
}
//faceSlicer();
        
        
module cutterBar(down=true){
  z = down ? -spacerZOffset : spacerZ+spacerZOffset;
  translate([0,0,z]){
    rotate([0,90,0])
      cylinder(h=spacerZ,d=spacerBarDia);
    //rotate([0,90,spacerBarAngle])
      //cylinder(h=spacerZ,d=spacerBarDia);
  }
}

//cutterBar(true);
//cutterBar(false);

module spacer(outerD = spacerOuterDia, innerD = spacerSmallInnerDia, single = false) {
  translate([0,0,spacerZ])
    rotate([180,0,0])
        difference(){
          cylinder(h=spacerZ,d=outerD);
          union(){
            cylinder(h=spacerZ,d=innerD);
            if (single){
              cutterBar(true);
              cutterBar(false);
            }
            else{
             translate([0,0,spacerZ+spacerZOffset])
                trianglePieCut();
              translate([0,0,-spacerZOffset])
                trianglePieCut();
            } 
          }
        }
}
//spacer(outerD = spacerSmallOuterDia, single=true);
//spacer();

module spacySpacer(){
  translate([0,0,-spacerZ/2.])
    rotate([0,0,spacerBarAngle])
      spacer();
}
//spacySpacer();

module spacerTube(){
  spacer(outerD = spacerSmallOuterDia,
        innerD= spacerBigInnerDia,  
        single=true);
}
//spacerTube();

module mainBracket(){
  linear_extrude(mainBracketZ)
    import(DXFFileName,layer=mainBracketLayerName);
}
 
module stepperBracket(){
  linear_extrude(stepperBracketZ)
    import(DXFFileName,layer=stepperBracketLayerName);
}
module bracket2BYJ(){
  union(){
    mainBracket();
    linear_extrude(mouseEarsZ)
        import(DXFFileName,layer=mouseEarsLayerName);
    stepperBracket();
  }
}
//bracket2BYJ();

module dcMotorBracket(){
  linear_extrude(dcMotorBracketZ)
    import(DXFFileName,layer=dcMotorBracketLayerName);
}

module dcBracket(){
  difference(){
    union(){
      mainBracket();
      dcMotorBracket();
      linear_extrude(mouseEarsZ)
        import(DXFFileName,layer=mouseEarsLayerName);
      linear_extrude(dcMotorBracketZ)
        import(DXFFileName,layer=sideHolderLayerName);
      linear_extrude(dcMotorBracketZ+sideHoldersUpperZ)
        import(DXFFileName,layer=sideHolderUpperLayerName);
    }
    linear_extrude(dcMotorBracketZ)
      import(DXFFileName,layer=dcMotorHolesLayerName);
  }
}
//projection()
//dcBracket();
      
module connector28YJ(){
  translate([0,0,connectorZ])
    mirror([0,1,0])
      rotate([180,0,0])
        difference(){
          linear_extrude(connectorZ)
            import(DXFFileName,layer=connectorLayerName);
          union(){
            linear_extrude(connectorLowCutZ)
              import(DXFFileName,layer=connectorLowCutLayerName);
            linear_extrude(connectorZ)
              import(DXFFileName,layer=connectorHighCutLayerName);
            }
        }
}
//connector28YJ();


module connectorDC(){
  translate([0,0,connectorZ])
    mirror([0,1,0])
      rotate([180,0,0])
        difference(){
          linear_extrude(connectorZ)
            import(DXFFileName,layer=connectorLayerName);
          union(){
            linear_extrude(connectorLowCutZ)
              import(DXFFileName,layer=connectorLowCutLayerName);
            linear_extrude(connectorZ)
              import(DXFFileName,layer=connectorHighCutDCLayerName);
            }
        }
}
//connectorDC();

module roundedPlateau(x,y,z,center=false,r=3){
  px = x - 2*r*sin(45);
  py = y - 2*r*sin(45);
  tr = center ? [-x/2+r*sin(45),-y/2.+r*sin(45),-z/2.] : [r*sin(45),r*sin(45),0];
  translate(tr)
  hull()
  for (i = [0,px])
    for (j =[0,py])
      translate([i,j,0])
        cylinder(h=z,r=r);
}
/*
roundedPlateau(64,58,2);
translate([0,0,-2])
cube([64,58,2]);
*/