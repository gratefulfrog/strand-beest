EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:L7805 U5
U 1 1 60DCA90F
P 7430 3720
F 0 "U5" V 7383 3825 50  0000 L CNN
F 1 "L7805" V 7476 3825 50  0000 L CNN
F 2 "misc:TO-220-3_Vertical_big_pads" H 7455 3570 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 7430 3670 50  0001 C CNN
	1    7430 3720
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 60DCB0AA
P 7430 4170
F 0 "C4" H 7545 4217 50  0000 L CNN
F 1 "100nF" H 7545 4124 50  0000 L CNN
F 2 "misc:C_Radial_D5.0mm_H7.0mm_P2.00mm_bug_pads" H 7468 4020 50  0001 C CNN
F 3 "~" H 7430 4170 50  0001 C CNN
	1    7430 4170
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 60DCB83E
P 7430 3270
F 0 "C3" H 7545 3317 50  0000 L CNN
F 1 "330nF" H 7545 3224 50  0000 L CNN
F 2 "misc:C_Rect_L7.2mm_W4.5mm_P5.00mm_Big_Pads" H 7468 3120 50  0001 C CNN
F 3 "~" H 7430 3270 50  0001 C CNN
	1    7430 3270
	1    0    0    -1  
$EndComp
$Comp
L switch-tact:DTS-6 S2
U 1 1 60DCF16A
P 2110 3350
F 0 "S2" V 1765 3350 50  0000 C CNN
F 1 "Left_Forward" V 1858 3350 50  0000 C CNN
F 2 "misc:SW_PUSH_tact-12mm_big_pads" H 2110 3500 50  0001 C CNN
F 3 "" H 2110 3350 50  0001 C CNN
	1    2110 3350
	0    1    1    0   
$EndComp
$Comp
L switch-tact:DTS-6 S4
U 1 1 60DCFAC2
P 3400 3370
F 0 "S4" V 3055 3370 50  0000 C CNN
F 1 "Right_Forward" V 3148 3370 50  0000 C CNN
F 2 "misc:SW_PUSH_tact-12mm_big_pads" H 3400 3520 50  0001 C CNN
F 3 "" H 3400 3370 50  0001 C CNN
	1    3400 3370
	0    1    1    0   
$EndComp
$Comp
L switch-tact:DTS-6 S1
U 1 1 60DD04A3
P 2790 3360
F 0 "S1" V 2445 3360 50  0000 C CNN
F 1 "Left_Rverse" V 2538 3360 50  0000 C CNN
F 2 "misc:SW_PUSH_tact-12mm_big_pads" H 2790 3510 50  0001 C CNN
F 3 "" H 2790 3360 50  0001 C CNN
	1    2790 3360
	0    1    1    0   
$EndComp
$Comp
L switch-tact:DTS-6 S3
U 1 1 60DD0D42
P 4050 3370
F 0 "S3" V 3705 3370 50  0000 C CNN
F 1 "Right_Reverse" V 3798 3370 50  0000 C CNN
F 2 "misc:SW_PUSH_tact-12mm_big_pads" H 4050 3520 50  0001 C CNN
F 3 "" H 4050 3370 50  0001 C CNN
	1    4050 3370
	0    1    1    0   
$EndComp
$Comp
L power:+7.5V #PWR0101
U 1 1 60DF92B8
P 8390 3420
F 0 "#PWR0101" H 8390 3270 50  0001 C CNN
F 1 "+7.5V" V 8405 3550 50  0000 L CNN
F 2 "" H 8390 3420 50  0001 C CNN
F 3 "" H 8390 3420 50  0001 C CNN
	1    8390 3420
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 60DF937A
P 8590 3420
F 0 "J3" H 8618 3396 50  0000 L CNN
F 1 "Conn_01x02_Female" H 8618 3303 50  0000 L CNN
F 2 "misc:WireHoles_1x02_P5.08mm_big_pads" H 8590 3420 50  0001 C CNN
F 3 "~" H 8590 3420 50  0001 C CNN
	1    8590 3420
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60DF9384
P 8390 3420
F 0 "#FLG0101" H 8390 3495 50  0001 C CNN
F 1 "PWR_FLAG" H 8390 3596 50  0000 C CNN
F 2 "" H 8390 3420 50  0001 C CNN
F 3 "~" H 8390 3420 50  0001 C CNN
	1    8390 3420
	1    0    0    -1  
$EndComp
Connection ~ 8390 3420
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60DF938F
P 8390 3520
F 0 "#FLG0102" H 8390 3595 50  0001 C CNN
F 1 "PWR_FLAG" H 8390 3696 50  0000 C CNN
F 2 "" H 8390 3520 50  0001 C CNN
F 3 "~" H 8390 3520 50  0001 C CNN
	1    8390 3520
	-1   0    0    1   
$EndComp
Wire Wire Line
	7430 3420 8390 3420
Connection ~ 7430 3420
Text Label 7430 4020 0    50   ~ 0
+5v_Controller
Text Label 4900 4710 2    50   ~ 0
+5v_Controller
$Comp
L power:GND1 #PWR0107
U 1 1 60EE0500
P 8390 3520
F 0 "#PWR0107" H 8390 3270 50  0001 C CNN
F 1 "GND1" V 8395 3390 50  0000 R CNN
F 2 "" H 8390 3520 50  0001 C CNN
F 3 "" H 8390 3520 50  0001 C CNN
	1    8390 3520
	0    1    1    0   
$EndComp
Connection ~ 8390 3520
$Comp
L power:GND1 #PWR0108
U 1 1 60EE8796
P 7430 3120
F 0 "#PWR0108" H 7430 2870 50  0001 C CNN
F 1 "GND1" V 7435 2990 50  0000 R CNN
F 2 "" H 7430 3120 50  0001 C CNN
F 3 "" H 7430 3120 50  0001 C CNN
	1    7430 3120
	0    1    1    0   
$EndComp
$Comp
L power:GND1 #PWR0109
U 1 1 60EE9B99
P 7130 3720
F 0 "#PWR0109" H 7130 3470 50  0001 C CNN
F 1 "GND1" V 7135 3590 50  0000 R CNN
F 2 "" H 7130 3720 50  0001 C CNN
F 3 "" H 7130 3720 50  0001 C CNN
	1    7130 3720
	0    1    1    0   
$EndComp
$Comp
L power:GND1 #PWR0110
U 1 1 60EEA13D
P 7430 4320
F 0 "#PWR0110" H 7430 4070 50  0001 C CNN
F 1 "GND1" V 7435 4190 50  0000 R CNN
F 2 "" H 7430 4320 50  0001 C CNN
F 3 "" H 7430 4320 50  0001 C CNN
	1    7430 4320
	0    1    1    0   
$EndComp
$Comp
L power:GND1 #PWR0111
U 1 1 60EEA7ED
P 6500 3510
F 0 "#PWR0111" H 6500 3260 50  0001 C CNN
F 1 "GND1" V 6505 3380 50  0000 R CNN
F 2 "" H 6500 3510 50  0001 C CNN
F 3 "" H 6500 3510 50  0001 C CNN
	1    6500 3510
	0    -1   -1   0   
$EndComp
$Comp
L power:GND1 #PWR0112
U 1 1 60EEC57C
P 4900 4210
F 0 "#PWR0112" H 4900 3960 50  0001 C CNN
F 1 "GND1" V 4905 4080 50  0000 R CNN
F 2 "" H 4900 4210 50  0001 C CNN
F 3 "" H 4900 4210 50  0001 C CNN
	1    4900 4210
	0    1    1    0   
$EndComp
Text Label 2990 3360 0    50   ~ 0
LR
Text Label 1910 3450 2    50   ~ 0
LF
Text Label 3200 3370 2    50   ~ 0
RF
Text Label 4900 3810 2    50   ~ 0
LR
Text Label 4900 3910 2    50   ~ 0
LF
Text Label 4250 3470 0    50   ~ 0
RR
Text Label 6500 3810 0    50   ~ 0
RR
Text Label 6500 3910 0    50   ~ 0
RF
NoConn ~ 4900 2910
NoConn ~ 4900 3010
NoConn ~ 4900 3110
NoConn ~ 4900 3210
NoConn ~ 4900 3310
NoConn ~ 4900 3410
NoConn ~ 4900 3510
NoConn ~ 4900 3610
NoConn ~ 4900 3710
NoConn ~ 6500 3010
NoConn ~ 6500 3110
NoConn ~ 6500 3210
NoConn ~ 6500 3310
NoConn ~ 6500 3410
NoConn ~ 6500 3610
NoConn ~ 6500 4210
NoConn ~ 6500 4310
NoConn ~ 6500 4410
NoConn ~ 6500 4510
NoConn ~ 6500 4610
NoConn ~ 6500 4710
NoConn ~ 4900 4610
NoConn ~ 4900 4510
NoConn ~ 4900 4410
NoConn ~ 4900 4310
NoConn ~ 2590 3360
NoConn ~ 2990 3460
NoConn ~ 1910 3350
NoConn ~ 4250 3370
NoConn ~ 3200 3470
NoConn ~ 3600 3370
NoConn ~ 3850 3370
NoConn ~ 2310 3350
$Comp
L Device:LED_BGCR D2
U 1 1 60E88CD8
P 3590 2530
F 0 "D2" V 3543 2890 50  0000 L CNN
F 1 "LED_BGCR" V 3636 2890 50  0000 L CNN
F 2 "misc:LED_D5.0mm-4_RGB_P2.54_Wide_Pins" H 3590 2480 50  0001 C CNN
F 3 "~" H 3590 2480 50  0001 C CNN
	1    3590 2530
	0    1    1    0   
$EndComp
$Comp
L Device:LED_BGCR D1
U 1 1 60E89F05
P 2350 2550
F 0 "D1" V 2303 2910 50  0000 L CNN
F 1 "LED_BGCR" V 2396 2910 50  0000 L CNN
F 2 "misc:LED_D5.0mm-4_RGB_P2.54_Wide_Pins" H 2350 2500 50  0001 C CNN
F 3 "~" H 2350 2500 50  0001 C CNN
	1    2350 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60ECF331
P 2350 2200
F 0 "R2" H 2420 2247 50  0000 L CNN
F 1 "1K" H 2420 2154 50  0000 L CNN
F 2 "misc:Resistor_Horizontal_RM7mm_Big_Pads" V 2280 2200 50  0001 C CNN
F 3 "~" H 2350 2200 50  0001 C CNN
	1    2350 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 60ECFF10
P 3590 2180
F 0 "R5" H 3660 2227 50  0000 L CNN
F 1 "1K" H 3660 2134 50  0000 L CNN
F 2 "misc:Resistor_Horizontal_RM7mm_Big_Pads" V 3520 2180 50  0001 C CNN
F 3 "~" H 3590 2180 50  0001 C CNN
	1    3590 2180
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR02
U 1 1 60ED4461
P 2350 2050
F 0 "#PWR02" H 2350 1800 50  0001 C CNN
F 1 "GND1" V 2355 1920 50  0000 R CNN
F 2 "" H 2350 2050 50  0001 C CNN
F 3 "" H 2350 2050 50  0001 C CNN
	1    2350 2050
	-1   0    0    1   
$EndComp
$Comp
L power:GND1 #PWR05
U 1 1 60ED5650
P 3590 2030
F 0 "#PWR05" H 3590 1780 50  0001 C CNN
F 1 "GND1" V 3595 1900 50  0000 R CNN
F 2 "" H 3590 2030 50  0001 C CNN
F 3 "" H 3590 2030 50  0001 C CNN
	1    3590 2030
	-1   0    0    1   
$EndComp
$Comp
L power:GND1 #PWR01
U 1 1 60ED77EC
P 2310 3450
F 0 "#PWR01" H 2310 3200 50  0001 C CNN
F 1 "GND1" V 2315 3320 50  0000 R CNN
F 2 "" H 2310 3450 50  0001 C CNN
F 3 "" H 2310 3450 50  0001 C CNN
	1    2310 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR03
U 1 1 60ED980D
P 2590 3460
F 0 "#PWR03" H 2590 3210 50  0001 C CNN
F 1 "GND1" V 2595 3330 50  0000 R CNN
F 2 "" H 2590 3460 50  0001 C CNN
F 3 "" H 2590 3460 50  0001 C CNN
	1    2590 3460
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR04
U 1 1 60ED9CC3
P 3600 3470
F 0 "#PWR04" H 3600 3220 50  0001 C CNN
F 1 "GND1" V 3605 3340 50  0000 R CNN
F 2 "" H 3600 3470 50  0001 C CNN
F 3 "" H 3600 3470 50  0001 C CNN
	1    3600 3470
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR06
U 1 1 60EDA4E5
P 3850 3470
F 0 "#PWR06" H 3850 3220 50  0001 C CNN
F 1 "GND1" V 3855 3340 50  0000 R CNN
F 2 "" H 3850 3470 50  0001 C CNN
F 3 "" H 3850 3470 50  0001 C CNN
	1    3850 3470
	1    0    0    -1  
$EndComp
NoConn ~ 2150 2750
NoConn ~ 3390 2730
Text Label 2350 2750 3    50   ~ 0
LG_LED
Text Label 2550 2750 3    50   ~ 0
LR_LED
Text Label 3590 2730 3    50   ~ 0
RG_LED
Text Label 3790 2730 3    50   ~ 0
RR_LED
$Comp
L power:GND1 #PWR0102
U 1 1 60F873F1
P 6500 2910
F 0 "#PWR0102" H 6500 2660 50  0001 C CNN
F 1 "GND1" V 6505 2780 50  0000 R CNN
F 2 "" H 6500 2910 50  0001 C CNN
F 3 "" H 6500 2910 50  0001 C CNN
	1    6500 2910
	0    -1   -1   0   
$EndComp
$Comp
L ESP32-DEVKITC-32D:ESP32-DEVKITC-32D U4
U 1 1 60DD4718
P 5700 3810
F 0 "U4" H 5700 4980 50  0000 C CNN
F 1 "ESP32-Controller" H 5700 4887 50  0000 C CNN
F 2 "misc:MODULE_ESP32-DEVKITC-32D_big_pads" H 5700 3810 50  0001 L BNN
F 3 "" H 5700 3810 50  0001 L BNN
F 4 "4" H 5700 3810 50  0001 L BNN "PARTREV"
F 5 "Espressif Systems" H 5700 3810 50  0001 L BNN "MANUFACTURER"
	1    5700 3810
	1    0    0    -1  
$EndComp
Text Label 4900 4010 2    50   ~ 0
LG_LED
Text Label 4900 4110 2    50   ~ 0
LR_LED
Text Label 6500 4010 0    50   ~ 0
RG_LED
Text Label 6500 4110 0    50   ~ 0
RR_LED
NoConn ~ 6500 3710
$Comp
L Mechanical:MountingHole H3
U 1 1 60FE1B25
P 2580 4360
F 0 "H3" H 2680 4407 50  0000 L CNN
F 1 "MountingHole" H 2680 4314 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2580 4360 50  0001 C CNN
F 3 "~" H 2580 4360 50  0001 C CNN
	1    2580 4360
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60FE41CD
P 2580 4600
F 0 "H4" H 2680 4647 50  0000 L CNN
F 1 "MountingHole" H 2680 4554 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2580 4600 50  0001 C CNN
F 3 "~" H 2580 4600 50  0001 C CNN
	1    2580 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 60FE479A
P 2570 4830
F 0 "H1" H 2670 4877 50  0000 L CNN
F 1 "MountingHole" H 2670 4784 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2570 4830 50  0001 C CNN
F 3 "~" H 2570 4830 50  0001 C CNN
	1    2570 4830
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60FE4DD1
P 2570 5060
F 0 "H2" H 2670 5107 50  0000 L CNN
F 1 "MountingHole" H 2670 5014 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2570 5060 50  0001 C CNN
F 3 "~" H 2570 5060 50  0001 C CNN
	1    2570 5060
	1    0    0    -1  
$EndComp
$EndSCHEMATC
