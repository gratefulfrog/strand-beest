You can find color coded images of leg assembly and gear assembly.

My notes indicate conversions to metrics

Color coded list, legs and frame (number of copies):

OK : Triangles:
Red - Min_Tri metric.stl (16) OK
* big hole needs to be converted to m6
* small holes need to be converted to m3

Blue - Maj_Tri metric.stl (16) OK
* small holes need to be converted to m3

---
White - Minor_Frame.stl (4) OK
* end holes need to be converted to m6 6.5
* big hole needs to be really 22mm dia 22.5

Black - Main_Frame.stl(2) OK
* bigger upper holes need to be converted to m8 8.5
* smaller lower holes need to be converted to m6 6.5
* central hole should be ok to fit 22mm bearing 22.5

---

Cam round columns need to match ST parts large and medium arm big hole 8.4mm dia

Cams:
* base height = 3mm
* cam first :
**  column height = 30 + 3mm base
**  column dia 7.8mm
**  drive cube height = 3 mm + column height
**  drive cube is 5.3 x 5.3
**  hole diamer 7.7mm

* cam second :
**  column height = 12 + 3mm base
**  column dia 7.8mm
**  drive cube height = 3 mm + column height
**  drive cube is 5.3 x 5.3
**  square hole: 5.7x5.7mm

* cam third :
**  column height = 25 + 3mm base
**  column dia 7.8mm
**  drive cube height = 3 mm + column height
**  drive cube is 5.3 x 5.3


cams in version"C" seem good!

Light blue - Cam_First_Stage_2.stl(2)
* round hole needs to be converted to m8

Purple - Cam_Second_Stage.stl (4)

Dark green - Cam_Third_Stage_2.stl(2)


----
OK linkage arms: OK
St_Parts metric new rounders.stl(8) 2:28 print time 
Orange - Large arm OK
* small holes need to be converted to m3
* large hole is dia 8.4 to fit a 8mm dia cam

Pink - Medium arm OK
* small holes need to be converted to m3
* large hole is dia 8.4 to fit a 8mm dia cam

Yellow - Small arm OK
* big hole needs to be converted to m6
* small holes need to be converted to m3

Green - Mini arm OK
* small holes need to be converted to m3

----
Color coded list, gears (number of copies):
Pink - Gear_First_Stage_2.stl(1) OK !!
* hole needs to be converted to m22+eplsion

Red - Gear_Second_Stage.stl(1) OK
* small hole needs to be converted to m8 and uses the m6-to-m8 shim ok
* big hole m22+small epsilon ok
* fix all dimensions to metric: gear height = cone height = cylinder height = cube height = 5mm
* project down from whole object to get ger teeth, lower diameter of cone
* project down from base of cube to get upper diameter of cone
* cone slope is such that no supports are needed: Oustide dia slopes to cylinder outside dia
  inside dia slopes from m22 to m8 ok
* in file stl-gears.scad & "DXF/Gear_Second_Stage base v2.dxf",layer="0"


Light blue - Gear_Third_Stage.stl(1) OK
* big hole m22+small epsilon
* square 14mm + eps

Yellow - Gear_Fourth_Stage.stl(1)  OK
* small hole needs to be converted to m8, no shim

wheel drive gear:  OK
Blue - Small_Gear_Shaft_Double.stl(1) broken
used - Small_Gear_Shaft_Double_fixed netfab online.stl
* small hole needs to be converted to m8
* needs to be adapted to actual wheels used

Orange - Small_Gear_Flange.stl(1)
* needs to be adapted to actual wheels used


Bearing_Shaft_Reducer.stl  OK
* inner dia should fit m6 shaft so dia should be 6.5
* outer dia should fit m8 hole so dia should be 8.0
* curent height: 6.5 mm
