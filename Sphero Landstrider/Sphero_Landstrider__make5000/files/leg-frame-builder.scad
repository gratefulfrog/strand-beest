$fn=100;

/********** FRAMES **********/

module mainFrame(){
  fullHt = 7.0;
  mainHt = 5.0;
  file = "Main_Frame";
  linear_extrude(height=mainHt)
      import (str("DXF/",file,".dxf"), layer="0");
  linear_extrude(height=fullHt)
      import (str("DXF/",file,".dxf"), layer="ring");
}
//mainFrame();

module minorFrame(){
  ht = 7.0;
  file = "Minor_Frame";
  linear_extrude(height=ht)
      import (str("DXF/",file,".dxf"), layer="0");
  }
//minorFrame();


/********************************** LEG PARTS ********************/
files = ["St_Parts",   // 0
         "Min_Tri",    // 1
         "Maj_Tri",    // 2
         ];

module legParts(ind){
  ht = 3;
  file = files[ind];
  linear_extrude(height=ht)
      import (str("DXF/",file,".dxf"), layer="0");
}

module stlParts(){
  legParts(0);
}
//stlParts();

module minTriangle(){
  legParts(1);
}
//minTriangle();

module majTriangle(){
  legParts(2);
}
majTriangle();

