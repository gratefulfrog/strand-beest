$fn=100;

/*******************************************************************/
/*********************   First stage gear  *************************/
lowerGearHeight = 5;
upperGearHeight = 10;
gearEpsilon     = 0.1;

module lowerGear(){
  linear_extrude(lowerGearHeight)
    import("DXF/Gear_First_Stage big gear.dxf",layer="0");
}
module upperGear(){
  linear_extrude(upperGearHeight+gearEpsilon)
    import("DXF/Gear_First_Stage small gear.dxf",layer="0");
}

module gear1(){
  translate([0,0,lowerGearHeight-gearEpsilon])
    upperGear();
  lowerGear();
}
//gear1();

/**************************************************************/
/******  GEAR SECOND STAGE *****/

// second stage gear();
lowerGearHeightG2 = 5;
slopeHeight       = 5;
upperCylinderHeight = 5;
upperCubeHeight = 5;

/* This is used to create the basis for modifications to the original
 * STL 
 */
module get2D(h,profile){
  projection(true)
    translate([0,0,-h])
      if(profile) rotate([-90,0,0])
      import("STL/Gear_Second_Stage.stl");
}

module getCone(){
  file = "Gear_Second_Stage Cone cross section";
  rotate_extrude($fn=200)
    import(str("DXF/",file,".dxf"), layer = "new");
}
//getCone();

module getProfile(h){
    get2D(0,true);
}
//getProfile(0);

module gearBase(){
  linear_extrude(5)
  #import("DXF/Gear_Second_Stage base.dxf",layer="0");
}
//gearBase();

module disk(){
  translate([0,0,10])
      cylinder(h=5,r1=10,r2=10);
}

module sqr(){
  translate([0,0,15+1.5])
    cube([14,14,5],center=true);
}
module axis(){
  translate([0,0,-1]){
    cylinder(h=25,r=4.25);
    cylinder(h=8,r=11.15);
  }
}

module gear2(){
  difference(){
    union(){
      #gearBase();
      #getCone();
      #disk();
      #sqr();
    }
    axis();
  }
}
//gear2();

/******************************************************************/
/*************  Gear Third Stage ***********/
module get32D(h){
  projection(false)
    translate([0,0,-h])
      import("STL/Gear_Third_Stage.stl");
}
//get32D(0);

module gearBase3(){
  linear_extrude(10)
  #import("DXF/Gear_Third_Stage base.dxf",layer="0");
}

module sqr3(){
  translate([0,0,15-1])
    cube([14,14,30],center=true);
}
module disk3(){
  //eps = 0.5;
  translate([0,0,10]){
    difference(){
      cylinder(h=7,r=14.55);
      cylinder(h=7,r=11.15);
  }
}
}

//gearBase3();
//sqr3();
module gear3(){
  difference(){
    union(){
      #gearBase3();
      #disk3();
    }
    #sqr3();
  }
}
//gear3();

/********************************************************************/
/**********   GEAR FOURTH STAGE    ********/
module get42D(h){
  projection(false)
    translate([0,0,-h])
      import("STL/Gear_Fourth_Stage.stl");
}
//get42D(0);
module gear4(){
  linear_extrude(5)
  #import("DXF/Gear_Fourth_Stage base.dxf",layer="0");
}
//gear4();

/********************************************************************/
/**********   Bearing_Shaft_Reducer   ********/
/* Inner dia is hole to accept M6 rod
 * Outer dia is shaft to enter in m8 (diam 8.5) hole of rht bearing
 */

ReducerInnerD = 6.3;
ReducerOuterD = 8.2; //8.0;
ReducerHeight = 32; //6.5;  // update to fit the double gear

module outerCyl(){
    cylinder(h=ReducerHeight,r=ReducerOuterD/2.0);
}
module innerCyl(){
    cylinder(h=ReducerHeight*3,r=ReducerInnerD/2.0,center=true);
}

module Bearing_Shaft_Reducer(){
  difference(){
    outerCyl();
    innerCyl();
  }
}
//Bearing_Shaft_Reducer();

/********************************************************************/
/**********   Small_Gear_Shaft_Double   ********/

module getSG2D(h,rot){
 projection(true)
    if (rot) rotate([90,0,0])
    rotate([0,0,6])
      translate([0,0,-h])
      //import("STL/Small_Gear_Shaft_Double original (repaired).stl");
      import("STL/Small_Gear_Shaft_Double original.off");

}
//getSG2D(15,true);

module baseDisk(){
  ht = 2;
  file = "Small_Gear_Shaft_Double baseDisk";
  linear_extrude(ht)
    import(str("DXF/",file,".dxf"), layer = 0);
}

module cylStage(){
  file = "Small_Gear_Shaft_Double cyl cross section";
  rotate_extrude($fn=200)
    import(str("DXF/",file,".dxf"), layer = 0);
}

module gearStage(){
  ht = 8;
  file = "Small_Gear_Shaft_Double gear cross section";
  translate([0,0,11])
    linear_extrude(ht)
      import(str("DXF/",file,".dxf"), layer = 0);
}

module topCylStage(){
  translate([0,0,30])
    rotate([180,0,0])
      cylStage();
} 

module axleCutter(){
  rad = 8.5/2.0;
  cylinder(h=30*2,r=rad);
}

module topDisk(){
  ht = 2;
  file = "Small_Gear_Shaft_Double topDisk";
  linear_extrude(ht)
    import(str("DXF/",file,".dxf"), layer = 0);
}

module Small_Gear_Shaft_Double(){
  difference(){
    union(){
      baseDisk();
      cylStage();
      gearStage();
      topCylStage();
    }
    axleCutter();
  }
}
//Small_Gear_Shaft_Double();
//topDisk();

/*************************************************************/
/************ Bearing Spacer ********************/
SpacerInnerD = 8.3;
SpacerOuterD = 11.3; 
SpacerHeight = 1.6;  // slice at layer height = 0.2 

module outerSCyl(){
    cylinder(h=SpacerHeight,r=SpacerOuterD/2.0);
}
module innerSCyl(){
    cylinder(h=SpacerHeight*3,r=SpacerInnerD/2.0,center=true);
}

module Bearing_Spacer(){
  difference(){
    outerSCyl();
    innerSCyl();
  }
}
//Bearing_Spacer();
