$fn=100;

epsilon = 0.1;
baseHt  = 2;
ergotHt = 5;
colHt   = 60;
notchHt = 5;

DXFfileName = "DXF/workbench.dxf";

layVec = ["0",                     // 0
          "Base",                  // 1
          "ColumnSupportsInner",   // 2
          "ColumnSupportsOuter",   // 3
          "Ergot",                 // 4
          "TopNotch"];             // 5


module getDxfExtrude(file,lay,hh){
  linear_extrude(hh)
    import(file,layer=lay);
}

//getDxfExtrude(DXFfileName,layVec[5],5);

module socles(){
  difference(){
    ht = ergotHt;
    getDxfExtrude(DXFfileName,layVec[3],ht);
    translate([0,0,-epsilon/2.])
      getDxfExtrude(DXFfileName,layVec[2],ht+epsilon);
  }
}
//socles();

module base(){
  getDxfExtrude(DXFfileName,layVec[1],baseHt);
  translate([0,0,baseHt])
    socles();
}
#base();

module column(){  
  getDxfExtrude(DXFfileName,layVec[0],colHt);
  getDxfExtrude(DXFfileName,layVec[4],ergotHt);
  translate([0,0,colHt])    
    getDxfExtrude(DXFfileName,layVec[5],notchHt);  
}
translate([0,0,baseHt])
  #column();

/**************  Testing code ********************/

module testCutter(ht=15){
  //ht = 15;
  wt = 30;
  ln = 30;
  translate([0,0,ht/2.-epsilon])
    cube([wt,ln,ht],center=true);
}
//#testCutter();

module baseTester(){
  intersection(){
    base();
    testCutter();
  }
}
//baseTester();

module colTester(){
  intersection(){
    column();
    testCutter(colHt+notchHt);
  }
}
//colTester();