$fn=100;

baseHeight = 3;
collHeight =  [30,12,25];
cubeHeight = baseHeight;
function cubeTotalHeight(i) = cubeHeight+collTotalHeight(i);
function collTotalHeight(i) = baseHeight+collHeight[i-1];

module cam(id,h=-1){
  file = str("DXF/Cam",id,".dxf");
  collH = h > -1 ? baseHeight + h : collTotalHeight(id);
  cubeH =  collH + cubeHeight;
  translate([-(50+25*(id-1)),-40,0]){
    linear_extrude(cubeH,true)
    import(file,layer="squares");

    linear_extrude(collH,true)
      import(file,layer="columns");

    linear_extrude(baseHeight,true)
    difference(){
      import(file,layer="rounders");
      import(file,layer="holes");
    }
  }
}
cam(1);

