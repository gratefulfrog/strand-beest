$fn=100;

module s1(){
stage1FileName = "STL/Cam_First_Stage_2.stl";
//projection(false)
//translate([0,0,-5])

translate([40,0,0])
import(file=stage1FileName);
}
module s2(){
stage2FileName = "STL/Cam_Second_Stage_4.stl";
//projection(false)
//translate([0,0,-5])

translate([-40,0,0])
import(file=stage2FileName);
}

module s3(){
stage3FileName = "STL/Cam_Third_Stage_2.stl";
//projection(false)
//translate([0,0,-5])

translate([-40,0,0])
import(file=stage3FileName);
}

module s123(){
stageFileName = "STL/Cam_First_Second_Third_Stage.stl";
//projection(false)
//translate([0,0,-5])

c1x = -47;
c3x = -71;
c2x = -96;

translate([c2x,-70,0])
import(file=stageFileName);
}

  s123();
/*
frameFileName = "Main_Frame.stl";

translate([200,0,0])
projection()
import(file=frameFileName);
*/