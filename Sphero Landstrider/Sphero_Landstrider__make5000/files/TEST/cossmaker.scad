$fn=100;

module cross1(){
  linear_extrude(36)
  #import("DXF/cooling cross v1.dxf",layer="0");
}

module coll(hh){
  wall = 1.35;
  rad  = 5;
  difference(){
    cylinder(h=hh,r=rad+wall);
    cylinder(h=hh,r=rad);
  }
}
    
//cross();
coll(36);