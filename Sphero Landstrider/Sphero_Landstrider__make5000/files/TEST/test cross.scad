$fn=100;

crossHeight = 1.5;

module cross(ht){
  linear_extrude(ht)
    import("../DXF/TEST/test square 93_7x7 with holes and gearing hole.dxf", layer="0");
}
module smallify(){
  w=120;
  c= 93/2.0;
  difference(){
  cross(crossHeight);
  translate([c,c,0])
    cube([w/2.,w/2.,5],center=true);
  }
}
module crossTxt(txt=false){
  union() {
    cross(crossHeight);
    if (txt)
      linear_extrude(crossHeight+2){
        translate([4,80,-1])
          text("m3", size=4);
        translate([4,11,-1])
          text("m6", size=4);
        translate([85,80,-1])
          text("m5", size=4);
        translate([85,12,-1])
          text("m8", size=4);
        translate([56,35.5,-1])
          rotate([0,0,-45])
            text("m22", size=4);
        }
    }
  }
 crossTxt(true);