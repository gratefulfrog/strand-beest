$fn=100;

crossHeight = 1.0;

module cross(ht){
  linear_extrude(ht)
    import("../DXF/Main_Frame metric rounded TESTER v3.dxf", layer="0");
}
module cutter(){
  w=120;
  c= 93/2.0;
  difference(){
  cross(crossHeight);
  translate([c,c,0])
    cube([w/2.,w/2.,5],center=true);
  }
}
cross(7);